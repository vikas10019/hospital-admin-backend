<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ucwords($setting?->name)}}</title>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        table {
            border-spacing: 0;
            background-color: #fff;
        }

        .table {
            margin: 10px auto;
        }

        table tr td {
            padding: 10px;
        }
        .table img{
            object-fit: cover
        }
    </style>
</head>

<body>
    <table class="table" style="width: 90%;">
        <tr>
            <td style="border-right:0; border-bottom:2px solid #ddd;">
                <div class="logo">
                    <img src="{{asset('storage/'.$setting?->bill_logo)}}" style="height:auto; width:150px;object-fit: contain;" alt="logo">
                </div>
            </td>            
            <td style="text-align: center; border-left: 0;border-bottom:2px solid #ddd;">
                <h4 style="font-size: 30px; font-weight: 600;"><b>{{ucwords($setting?->name)}}</b></h4>
                <p>
                    {{$setting?->address}} <br>
                    {{$setting?->email}} <br>
                    {{$setting?->mobile}} <br>
                    Timings:{{$setting?->timings}}
                </p>
            </td>
            <td style="text-align: center; border-left: 0;border-bottom:2px solid #ddd;"></td>
        </tr>
        <tr>
            <td style="border-top: 0; padding-top: 8px; width: 33%;">
                <b>Name:</b> <span style="text-align: end;">{{ucwords($invoice?->patient?->user?->name)}}</span>
            </td>
            <td style="border-top: 0; padding-top: 8px; width: 33%;">
                <b>ID:</b> <span style="text-align: end;">{{$invoice?->patient?->patient_id}}</span>
            </td>
            <td style="border-top: 0; padding-top: 8px; width: 33%;">
                <b>Age/Sex:</b> <span style="text-align: end;">{$invoice?->patient?->age}} Years/
                                            {{$invoice?->patient?->user?->gender == 1 ? 'Male' : 'Female'}}</span>
            </td>
        </tr>
        <tr>
            <td style="border-top: 0; padding-top: 8px; width: 33%;">
                <b>Date:</b> <span style="text-align: end;">{{$invoice->bill_date}}</span>
            </td>
            <td style="border-top: 0; padding-top: 8px; width: 33%;">
                <b>Bill:</b> <span style="text-align: end;">{{$billId}}</span>
            </td>
            <td style="border-top: 0; padding-top: 8px; width: 33%;">
                <b>Doctor:</b> <span style="text-align: end;">Dr. {{ucwords($invoice?->doctor?->user?->name)}}</span>
            </td>
        </tr>    
    </table>
    <table class="table" style="width: 90%;">        
        <tr>
            <th colspan="3" style="border-top: 2px solid #ddd;border-bottom: 2px solid #ddd;padding:10px">Bill/Receipt
            </th>
        </tr>
        <tr>
            <th style="text-align:left; padding:10px; width: 10%;">S.No</th>
            <th style="text-align:left; padding:10px;width: 40%;">Service</th>
            <th style="text-align:left; padding:10px;width: 20%;">Charges (INR)</th>
            @if(!$invoice?->appointment)
            <th style="text-align:left; padding:10px;width: 10%;">Quantity</th>
            @endif
            <th style="text-align:left; padding:10px;width: 20%;">Sub Total (INR)</th>
        </tr>
        @php $subtotal = 0; $discount = 0; $tax = 0; $total = 0; @endphp
        @if($invoice?->appointment)
        <tr>
            <td style="text-align:left; padding:10px;">
                01.
            </td>
            <td style="text-align:left; padding:10px;">Consultation Fees</td>
            <td style="text-align:left; padding:10px;">{{$invoice?->doctor?->price}}</td>
<!--            <td style="text-align:left; padding:10px;">{{ \Carbon\Carbon::createFromTimeString($invoice?->doctor?->user?->schedule()->first()->per_patient_time)->diffForHumans(Carbon\Carbon::today(), Carbon\CarbonInterface::DIFF_ABSOLUTE, true, 3)}}</td>-->
            <td style="text-align:left; padding:10px;">{{$invoice?->doctor?->price}}</td>
             @php    $subtotal = $invoice?->doctor?->price; @endphp
        </tr>
        @endif
        @foreach($invoice?->services as $key => $service)
        <tr>
            <td style="text-align:left; padding:10px;">
                <p>{{$key + 1}}</p>
            </td>
            <td style="text-align:left; padding:10px;">
                <p>{{$service?->name}}</p>
            </td>
            <td style="text-align:left; padding:10px;">
                <p>{{$service?->amount}}</p>
            </td>
            <td style="text-align:left; padding:10px;">
                <p>{{$service?->pivot?->quantity}}</p>
            </td>
            <td style="text-align:left; padding:10px;">
                <p>{{$service->amount * $service?->pivot?->quantity}}</p>
            </td>
            @php
                $subtotal += $service->amount * $service?->pivot?->quantity;
            @endphp
        </tr>
        @endforeach 
        @php
             $discount = $subtotal*($invoice?->discount/100);
             $tax = $subtotal*($invoice?->tax/100);
             $total = $subtotal-$discount+$tax;
        @endphp
        @if(!$invoice?->appointment)
        <tr>
            <td colspan="4" style="border-top: 2px solid #ddd; text-align: right;">Sub Total</td>
            <td style=" text-align: left;border-top:2px solid #ddd;">{{$subtotal}} INR</td>
        </tr>
        <tr>
            <td colspan="4" style=" text-align: right;">Discount ({{$invoice?->discount ??0}}%)</td>
            <td style=" text-align: left;">{{$discount}} INR</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: right;">Tax ({{$invoice?->tax ??0}}%)</td>
            <td style=" text-align: left;">{{$tax}} INR</td>
        </tr>
        @endif
        <tr>
            <td colspan="4" style="border-top: 2px solid #ddd; text-align: right;"><b>Total Amount</b></td>
            <td style="border-top: 2px solid #ddd; text-align: left;">{{$total}} INR</td>
        </tr>
        <tr>
            <td colspan="5" style="border-top: 2px solid #ddd; text-align:  right; padding-top:60px;">
                <p style="margin-bottom: 0px;padding-bottom:5px"><b>Authorized Signatory</b></p>                
                <span>Dr. {{ucwords($invoice?->doctor?->user?->name)}}</span>
                <br>
                <span>{{ucwords($invoice?->doctor?->specialization?->name)}}</span>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="border-top: 2px solid #ddd; text-align:  center;">
                <p style="margin-bottom: 0px;padding-bottom:5px">{{$setting?->notes ?? "Thank you for choosing us. We are committed to providing you with the best care."}}</p>                               
            </td>
        </tr>
    </table>
</body>

</html>