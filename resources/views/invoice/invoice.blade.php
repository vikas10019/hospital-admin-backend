<!DOCTYPE html>
<html>
<head>
    <title>Hospital Doccure</title>
</head>
<style type="text/css">
    body{
        font-family: 'Roboto Condensed', sans-serif;
    }
    .m-0{
        margin: 0px;
    }
    .p-0{
        padding: 0px;
    }
    .pt-5{
        padding-top:5px;
    }
    .mt-10{
        margin-top:10px;
    }
    .text-center{
        text-align:center !important;
    }
    .w-100{
        width: 100%;
    }
    .w-50{
        width:50%;   
    }
    .w-85{
        width:85%;   
    }
    .w-15{
        width:15%;   
    }
    .logo img{
        width:200px;
        height:60px;        
    }
    .gray-color{
        color:#5D5D5D;
    }
    .text-bold{
        font-weight: bold;
    }
    .border{
        border:1px solid black;
    }
    table tr,th,td{
        border: 1px solid #d2d2d2;
        border-collapse:collapse;
        padding:7px 8px;
    }
    table tr th{
        background: #F4F4F4;
        font-size:15px;
    }
    table tr td{
        font-size:13px;
    }
    table{
        border-collapse:collapse;
    }
    .box-text p{
        line-height:10px;
    }
    .float-left{
        float:left;
    }
    .total-part{
        font-size:16px;
        line-height:12px;
    }
    .total-right p{
        padding-right:20px;
    }
</style>
<body>
<div class="head-title">
    <h1 class="text-center m-0 p-0">Invoice</h1>
</div>
<div class="add-detail mt-10">
    <div class="w-50 float-left mt-10">
        <p class="m-0 pt-5 text-bold w-100">Invoice Details :</p>
        <p class="m-0 pt-5 text-bold w-100">Invoice Id - <span class="gray-color">{{$billId}}</span></p>
        <p class="m-0 pt-5 text-bold w-100">Invoice Date - <span class="gray-color">{{$invoice->bill_date}}</span></p>
    </div>
<!--    <div class="w-50 float-left mt-10">
        <p class="m-0 pt-5 text-bold w-100">Bill To :</p>
        <p class="m-0 pt-5 text-bold w-100">{{$invoice?->patient?->user?->name}}</p>
        <p class="m-0 pt-5 text-bold w-100">{{$invoice?->patient?->user?->address}}</span</p>
    </div>-->
    <div style="clear: both;"></div>
</div>
<div class="table-section bill-tbl w-100 mt-10">
    <table class="table w-100 mt-10">
        <tr>
            <th class="w-50">Doctor Details</th>
            <th class="w-50">Patient Details</th>
        </tr>
        <tr>
            <td>
                <div class="box-text">
                    <p>{{$invoice?->doctor?->title}} {{$invoice?->doctor?->user?->name}},</p>
                    <p>{{$invoice?->doctor?->designation}} ({{$invoice?->doctor?->specialization?->name}}),</p>
                    <p>{{$invoice?->doctor?->user?->address}},</p>                    
                    <p>Contact: {{$invoice?->doctor?->user?->mobile}}</p>
                </div>
            </td>
            <td>
                <div class="box-text">
                    <p>{{$invoice?->patient?->user?->name}},</p>
                    <p>{{$invoice?->patient?->user?->address}},</p>                  
                    <p>Contact: {{$invoice?->patient?->user?->mobile}}</p>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="table-section bill-tbl w-100 mt-10">
    <table class="table w-100 mt-10">
        <tr>
            <th class="">Payment Method</th>
<!--            <th class="w-50">Shipping Method</th>-->
        </tr>
        <tr>
            <td>{{$invoice->payment_method}}</td>
<!--            <td>Free Shipping - Free Shipping</td>-->
        </tr>
    </table>
</div>
<div class="table-section bill-tbl w-100 mt-10">
    <table class="table w-100 mt-10">
        <tr>
<!--            <th class="w-50">S.no.</th>-->
            <th class="w-50">Service Name</th>
            <th class="w-50">Price</th>
            <th class="w-50">Qty / Hrs</th>
            <th class="w-50">Subtotal</th>
        </tr>
         @php $subtotal = 0; @endphp
        @if($invoice?->appointment)
        <tr align="center">
<!--            <td>1</td>-->
            <td>Consultation Fee</td>
            <td>${{$invoice?->doctor?->price}}</td>
            <td>{{ Carbon\Carbon::createFromTimeString($invoice?->doctor?->user?->schedule()->first()->per_patient_time)->diffForHumans(Carbon\Carbon::today(), Carbon\CarbonInterface::DIFF_ABSOLUTE, true, 3)}}</td>
            <td>${{$invoice?->doctor?->price}}</td>
           @php $subtotal = $invoice?->doctor?->price; @endphp
        </tr>
        @endif
        @foreach($invoice?->services as $service)
        <tr align="center">
<!--            <td>M102</td>-->
            <td>{{$service?->name}}</td>
            <td>${{$service?->amount}}</td>
            <td>{{$service?->pivot?->quantity}}</td>
            <td>${{$service->amount*$service?->pivot?->quantity}}</td>
           @php
           $subtotal += $service->amount*$service?->pivot?->quantity;
           @endphp
        </tr>
        @endforeach
        
        <tr>
            <td colspan="7">
                <div class="total-part">
                    <div class="total-left w-85 float-left" align="right">
                        <p>Sub Total</p>
<!--                        <p>IGST (18%)</p>-->
                        <p>Total Payable</p>
                    </div>
                    <div class="total-right w-15 float-left text-bold" align="right">
                        <p>${{$subtotal}}</p>
<!--                        <p>$18</p>-->
                        <p>${{$subtotal}}</p>
                    </div>
                    <div style="clear: both;"></div>
                </div> 
            </td>
        </tr>
    </table>
</div>
</html>