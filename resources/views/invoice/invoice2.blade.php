<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hospital Doccure</title>
    <style>
        body {
            font-family: "Poppins", sans-serif;
        }

        .container {
            width: 700px;
            margin: auto;
        }

        table {
            border-color: #eee;
            margin: 20px auto;
            border: 1px solid #000;
        }

        table.info {
            border-color: #333;
        }

        table tr th {
            font-size: 18px;
            font-weight: 600;
        }

        .logo_bar img {
            margin-bottom: 2px;
        }
        hr {
            margin: 0;
            border-color: #000;
            border-width: 1px;
        }

        h1 {
            margin: 0;
            padding-bottom: 10px;
            border-bottom: 1px solid #000;

        }

        .detail table {
            border: none;
        }

        .detail table thead tr td {
            width: 50%;
        }
        .main{
            border-bottom: 1px solid #000;
        }
    </style>
</head>

<body>
    <section>
        <div class="page-wrapper">
            <div class="container">
                <div class="panel-body" id="printMe">
                    <div class="table-header" style="width: 700px;">
                        <div class="col-xs-12 col-md-6 logo_bar">
                            <img src="{{asset('storage/'.$setting?->bill_logo)}}"
                                class="img-responsive" alt="">

                            <br>
                            <p style="line-height: 22px;">Phone No: +91123465798<br>
                            Email: admin@admin.com<br></p>
                        </div>
                        <div class="col-xs-12 col-md-6 main">
                            <h1><strong>{{$invoice?->doctor?->title}} {{$invoice?->doctor?->user?->name}}</strong></h1>

                            <div class="detail">
                                <table style="margin-left: 0;" width="500" border="0" cellpadding="0" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <td style="width: 40%; vertical-align: top; line-height: 26px;">
                                                {{$invoice?->doctor?->specialization?->name}}<br>
                                                {{$invoice?->doctor?->designation}}<br>
                                                {{$invoice?->doctor?->user?->address}}<br>
                                                Contact: {{$invoice?->doctor?->user?->mobile}}

                                            </td>
                                            <td style="text-align: right; width: 60%; vertical-align: top;">Timing:
                                                09:00 AM - 02:00 PM | Closed: Thursday</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Patient Info -->
                    <div class="row patient_info">
                        <div class="table-responsive" style="border: none;">
                            <table class="info" cellpadding="10" cellspacing="0" width="500" border="1">
                                <tbody>
                                    <tr>
                                        <td>Patient ID:</td>
                                        <td>{{$invoice?->patient?->patient_id}}</td>
                                        <td>Bill Date:</td>
                                        <td>{{$invoice->bill_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>Patient Name:</td>
                                        <td>{{$invoice?->patient?->user?->name}}</td>
                                        <td>Receipt No:</td>
                                        <td>{{$billId}}</td>
                                    </tr>
                                    <tr>
                                        <td>Age/Sex:</td>
                                        <td>{{$invoice?->patient?->age}} Years/
                                            {{$invoice?->patient?->user?->gender == 1 ? 'Male' : 'Female'}}
                                        </td>
                                        <td>Address:</td>
                                        <td>{{$invoice?->patient?->user?->address}}, Contact:
                                            {{$invoice?->patient?->user?->mobile}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Patient Package -->
                    <!-- Patient Charge -->
                    <div class="patient_charge">
                        <table class="charge" cellpadding="10" cellspacing="0" width="500" border="1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Service Name</th>
                                    <th>Price (INR)</th>
                                    <th>Quantity</th>
                                    <th>Sub Total (INR)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $subtotal = 0; @endphp
                                @if($invoice?->appointment)
                                    <tr align="center">
                                        <td>#</td>
                                        <td>Consultation Fee</td>
                                        <td>{{$invoice?->doctor?->price}}</td>
                                        <td>{{ Carbon\Carbon::createFromTimeString($invoice?->doctor?->user?->schedule()->first()->per_patient_time)->diffForHumans(Carbon\Carbon::today(), Carbon\CarbonInterface::DIFF_ABSOLUTE, true, 3)}}
                                        </td>
                                        <td>{{$invoice?->doctor?->price}}</td>
                                        @php    $subtotal = $invoice?->doctor?->price; @endphp
                                    </tr>
                                @endif
                                @foreach($invoice?->services as $key => $service)
                                                                <tr align="center">
                                                                    <td class="description">
                                                                        <p>{{$key + 1}}</p>
                                                                    </td>
                                                                    <td class="description">
                                                                        <p>{{$service?->name}}</p>
                                                                    </td>
                                                                    <td class="charge">
                                                                        <p>{{$service?->amount}}</p>
                                                                    </td>
                                                                    <td class="discount">
                                                                        <p>{{$service?->pivot?->quantity}}</p>
                                                                    </td>
                                                                    <td class="ballance">
                                                                        <p>{{$service->amount * $service?->pivot?->quantity}}</p>
                                                                    </td>
                                                                    @php
                                                                        $subtotal += $service->amount * $service?->pivot?->quantity;
                                                                       @endphp
                                                                </tr>
                                @endforeach                                     
                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6 ">
                            <table class="payment" cellpadding="10" cellspacing="0" width="500" border="1">
                                <thead>
                                    <tr>
                                        <th>Payment Method</th>
                                        <th>{{$invoice->payment_method}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-xs-12 col-md-6 ">
                            <table style="text-align: center;" class="payment" cellpadding="10" cellspacing="0"
                                width="500" border="1">
                                <thead>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td>{{$subtotal}} INR</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Discount (0%)</td>
                                        <td>0 INR</td>
                                    </tr>
                                    <tr>
                                        <td>Tax (0%)</td>
                                        <td>0 INR</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th>Total Amount</th>
                                        <th>{{$subtotal}} INR</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="my_sign pull-right">
                        <span>___________________________</span>
                        <p>Signature</p>
                    </div>
                    <div class="clear"></div>
                    <div class="row bill-address">
                        <address>
                            <h3>Address</h3>
                            <strong>119, Panchsheel Enclave Durgapura, Jaipur</strong>
                            <p>Time: Tuesday-Saturday, Evening 5-8PM<br>
                                (On Sunday/Monday by Appointment. Mob:9460926075)<br>
                                *Not for Medico Legal Purpose
                            </p>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>

</html>