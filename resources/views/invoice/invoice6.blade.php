<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if($invoice?->doctor?->clinic_name)
    <title>{{ucwords($invoice?->doctor?->clinic_name)}}</title>
    @else
    <title>{{ucwords($setting?->name)}}</title>
    @endif
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        body {
            margin: 0;
            padding: 0;
            font-family:Arial, Helvetica, sans-serif;
        }
        table {
            width: 100%;
            border: 1px solid #ddd;            
            width: 90%;
            margin:0;
            border-collapse: collapse;
        }
        td, th {
            padding: 6px;
            border: 1px solid #ddd;
            text-align: left;
            font-size: 10px;
            color:#333;
            font-weight: 400;
        }
        p {
            margin: 0;
            line-height: 1.5;
        }
        .center-text {
            text-align: center;
        }
        .right-text {
            text-align: right;
        }
        .strong {
            font-weight: bold;
            margin:0;
        }      
        .logo{
            position:absolute;
            right:10px;
            top:30px;
        } 
        .paidtext{
            position: absolute;
            top: 6px;
            left: 5%;
            margin: auto;
            display: block;
            transform: rotate(345deg);
            font-size: 18px;
            font-weight: 500;
            color:#000;
            text-transform: uppercase;
            border: 1px dashed #fff;
            padding: 2px 5px;
            line-height:22px;
            border-radius: 5px;
            margin: 0;
        }
    </style>
</head>
<body>
    <div style="padding-top:40px">
    <table style="margin:0 auto;">    
        <tr>
            <td style="text-align: center; padding: 4px 10px 0; border: 0; font-size: 18px; position:relative" colspan="4"><strong>{{$invoice?->doctor?->clinic_name ? ucwords($invoice?->doctor?->clinic_name) : ucwords($setting?->name)}} </strong>
            <img class="logo" src="https://www.jaipurjoints.com/wp-content/uploads/2019/01/jj-log12.png" alt="{{$invoice?->doctor?->clinic_name ? ucwords($invoice?->doctor?->clinic_name) : ucwords($setting?->name)}}" style="width: 150px;" />
        </td>                       
        </tr>       
        <tr>
            <td style="text-align: center; padding:0 10px 0; border: 0;" colspan="4"><p style="max-width:350px;margin:0 auto 0;">{{$invoice?->doctor?->clinic_address ? $invoice?->doctor?->clinic_address : $setting?->address}}</p></td>
        </tr>
        <tr>
            <td style="text-align: center; padding:0 10px 0; border: 0;" colspan="4"><strong>Phone:</strong> {{$invoice?->doctor?->phone ? $invoice?->doctor?->phone : $setting?->mobile}} | <strong>Email:</strong> {{$invoice?->doctor?->user?->email ? $invoice?->doctor?->user?->email : $setting?->email}}</td>            
        </tr> 
        <tr>
            <td style="text-align: center; padding-top: 4px; padding-bottom: 10px; border: 0; border-bottom: 1px solid #ddd;" colspan="4"><p style="max-width:350px;margin:0 auto 0;"><strong>Timings:</strong> {{$invoice?->doctor?->clinic_timings ? $invoice?->doctor?->clinic_timings : $setting?->timings}}</p></td>
        </tr>
        <tr>
            <td style="width:20%; border:0;padding:4px 10px"><strong>Name:</strong></td> 
            <td style="border:0; width:30%;padding:4px 10px">{{ucwords($invoice?->patient?->user?->name)}} </td>                                 
            <td style="border:0; width:30%;padding:4px 10px"><strong>Doctor:</strong></td>   
            <td style="border:0; width:20%;padding:4px 10px">Dr. {{ucwords($invoice?->doctor?->user?->name)}}</td>  
            
        </tr>
        <tr>   
            <td style="border:0; width:20%;padding:4px 10px"><strong>Age / gender:</strong></td>            
            <td style="border:0; width:30%;padding:4px 10px">{{$invoice?->patient?->age}} Years / {{$invoice?->patient?->user?->gender == 1 ? 'Male' : 'Female'}}</td>
            <td style="border:0; width:30%;padding:4px 10px"><strong>Speciality:</strong></td>
            <td style="border:0; width:20%;padding:4px 10px">{{ucwords($invoice?->doctor?->specialization?->name)}}</td>
        </tr>  
        <tr>    
            <td style="border:0; width:20%;padding:4px 10px"><strong>ID:</strong></td>        
            <td style="border:0; width:30%;padding:4px 10px">{{$invoice?->patient?->patient_id}}</td>
            <td style="border:0; width:30%;padding:4px 10px"><strong>Bill Date & Time:</strong></td>
            <td style="border:0; width:20%;padding:4px 10px">{{\Carbon\Carbon::parse($invoice?->created_at)->format('d-m-Y h:i A') }}</td>
        </tr>  
         <tr>    
            <td style="border:0; width:20%;padding:4px 10px"><strong>Contact No.:</strong></td>        
            <td style="border:0; width:30%;padding:4px 10px">{{ucwords($invoice?->patient?->user?->mobile)}}</td>
            <td style="border:0; width:30%;padding:4px 10px"><strong>Bill ID:</strong></td>
            <td style="border:0; width:20%;padding:4px 10px">{{$invoice?->bill_id}}</td>
        </tr> 
         @if($invoice?->appointment)
        <tr>    
            <td style="border:0; width:30%;padding:4px 10px"><strong>Appointment Date & Time:</strong></td>
            <td style="border:0; width:20%;padding:4px 10px">{{\Carbon\Carbon::parse($invoice?->appointment->meeting_at)->format('d-m-Y h:i A') }}</td>
            <td style="border:0; width:20%;padding:4px 10px"><strong></strong></td>
            <td style="border:0; width:30%;padding:4px 10px"></td>
        </tr>  
         @endif
    </table>
    <table style="margin:0 auto; border-top:0;">
        <tr style="border-bottom:1px solid #ddd">
            <td style="width:2%; border:0;"><strong>S.No</strong></td>
            <td style="width:38%; border:0;"><strong>Service</strong></td>
            <td style="width: 20%; border:0;"><strong>Charges (INR)</strong></td>
            @if(!$invoice?->appointment)
            <td style="width: 20%; border: 0;border-right:0"><strong>Quantity</strong></td>
            @endif
            <td style="width: 20%; border:0;text-align:center;"><strong>Sub Total (INR)</strong></td>
        </tr>
        @php $subtotal = 0; $discount = 0; $tax = 0; $total = 0; @endphp
        @if($invoice?->appointment)
        <tr>
            <td style="border:0; width: 10%">
                01.
            </td>
            <td style="border:0; width: 40%">Consultation Fees</td>
            <td style="border:0; width: 20%">{{$invoice?->doctor?->price}}</td>
<!--            <td style="text-align:left; padding:10px;">{{ \Carbon\Carbon::createFromTimeString($invoice?->doctor?->user?->schedule()->first()->per_patient_time)->diffForHumans(Carbon\Carbon::today(), Carbon\CarbonInterface::DIFF_ABSOLUTE, true, 3)}}</td>-->
            <td style="border:0;text-align:center; width: 30%">{{$invoice?->doctor?->price}}</td>
             @php    $subtotal = $invoice?->doctor?->price; @endphp
        </tr>
        @endif
        @foreach($invoice?->services as $key => $service)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$service?->name}}</td>
            <td>{{$service?->amount}}</td>
            <td>{{$service?->pivot?->quantity}}</td>
            <td style="text-align:center;">{{$service->amount * $service?->pivot?->quantity}}</td>
             @php
                $subtotal += $service->amount * $service?->pivot?->quantity;
            @endphp
        </tr>
        @endforeach 
         @php
             $discount = $subtotal*($invoice?->discount/100);
             $tax = $subtotal*($invoice?->tax/100);
             $total = $subtotal-$discount+$tax;
             $numberFormater = new \NumberFormatter("en",  \NumberFormatter::SPELLOUT);
             $wordAmount = $numberFormater->format($total);
        @endphp
        <tr style="border-top:0;">
            <td colspan="@if($invoice?->appointment)2 @else 3 @endif" style="border:0; border-right:1px solid #ddd; border-top:1px solid #ddd"><strong>Payment Mode:</strong> Cash</td>
            <td style="border:0; border-top:1px solid #ddd;padding:4px 10px"><strong>Sub Total</strong></td>
            <td style="border:0; border-top:1px solid #ddd;text-align:center;padding:4px 10px">{{$subtotal}} INR</td>
        </tr>
        <tr>
            <td colspan="@if($invoice?->appointment)2 @else 3 @endif" rowspan="3" style="border:0;vertical-align:bottom; border-right:1px solid #ddd;position:relative">
                @if($invoice?->status)
                <span class="paidtext"  style="color:#2ca87f; border-color:#2ca87f">{{ $invoice?->status ? "PAID" :""}}</span>
                @else
                <span class="paidtext"  style="color:#dc2626; border-color:#dc2626">{{ $invoice?->status ? "" :"UNPAID "}}</span>
                @endif
                <strong>{{ucwords($wordAmount)}} INR Only</strong></td>
            <td style="border:0;padding:4px 10px"><strong>Discount ({{$invoice?->discount ??0}}%)</strong></td>
            <td style="border:0;text-align:center;padding:4px 10px"><strong>{{$discount}} INR</strong></td>
        </tr>
        <tr>
            <td style="border:0;padding:4px 10px"><strong>Tax ({{$invoice?->tax ??0}}%)</strong></td>
            <td style="border:0;text-align:center;padding:4px 10px"><strong>{{$tax}} INR</strong></td>
        </tr>
        <tr>
            <td style="border-right:0"><strong>Total Amount</strong></td>
            <td style="text-align:center;border-left:0"><strong>{{$total}} INR</strong></td>
        </tr>
    </table>
    <table style="border-top: 0; margin:0 auto;">
        <tr>
            <td colspan="4" class="right-text" style="padding-top: 40px; border-top: 0;">
                <strong>Authorized Signatory</strong>
                <p>Jaipur Joints</p>
<!--                <p>Dr. {{ucwords($invoice?->doctor?->user?->name)}} @if($invoice?->doctor?->designation)<br>{{ucwords($invoice?->doctor?->designation)}} @else <br>{{ucwords($invoice?->doctor?->specialization?->name)}} @endif</p>-->
            </td>
        </tr>
        <!-- <tr>
            <td colspan="4" style="text-align: center;"><strong>Payment Received</strong></td>
        </tr>
         @if(!$invoice?->appointment)
        <tr>
            <td><strong>Date</strong></td>
            <td><strong>Reference#</strong></td>
            <td><strong>Payment Mode</strong></td>
            <td><strong>Amount</strong></td>
        </tr>
        <tr>
            <td>{{\Carbon\Carbon::parse($invoice?->created_at)->format('Y-m-d') }}</td>
            <td>{{$invoice?->card_check_no ?? "N/A"}}</td>
            <td>{{$invoice?->payment_method ?? "N/A"}}</td>
            <td>{{$total}}</td>
        </tr>
         @endif
         <tr>
            <td colspan="4" style="border-top: 2px solid #ddd; text-align:  center;">
                <p style="margin-bottom: 0px;padding-bottom:5px">{{$setting?->notes ?? "Thank you for choosing us. We are committed to providing you with the best care."}}</p>                               
            </td>
        </tr> -->
    </table>
    </div>
</body>
</html>
