@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => 'http://localhost:3000/reset-password/'.$token])
Click Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent