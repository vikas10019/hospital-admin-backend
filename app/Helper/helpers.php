<?php
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\User;

if (!function_exists('smtpConnect')) {

    function smtpConnect() {

        try {
            $mail = config()->get('mail.mailers.smtp');
//            dd($mail);
            $transport = new \Swift_SmtpTransport($mail['host'], $mail['port'], $mail['encryption']);
            $transport->setUsername($mail['username']);
            $transport->setPassword($mail['password']);
            $mailer = new \Swift_Mailer($transport);
            $mailer->getTransport()->start();
            $smtp = true;
        } catch (\Swift_TransportException $e) {
            $smtp = false;
        }
        return $smtp;
    }

}
if (!function_exists('paginate')) {

    function paginate($items, $perPage = 1, $page = null, $options = []) {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);
           // Convert JSON data to associative array
                $arrayData = json_decode($items->forPage($page, $perPage), true);

                // Convert the associative array into the desired format
                $formattedArray = array_values($arrayData);

                // Output the formatted array as JSON
                $item_decode = json_encode($formattedArray, JSON_PRETTY_PRINT);

        return new LengthAwarePaginator(json_decode($item_decode), $items->count(), $perPage, $page, $options);
    }
}

if (!function_exists('store_activity_log')) {

    function store_activity_log($causer, $performed_on, $custom_properties, $log_name, $description)
    {
        $activity = activity($log_name)
           ->causedBy($causer)
           ->performedOn($performed_on)
           ->withProperties($custom_properties)
           ->log($description);

        return $activity;
    }   

}

