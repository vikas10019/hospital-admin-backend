<?php

namespace App\Imports;

use App\Models\Patient;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Http\Controllers\AppointmentController;
use App\Models\Schedule;
use App\Models\Appointment;
use DB;

class PatientsImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        // Skip the first row (worksheet name)
//        $rows->shift();
        foreach ($rows as $row) {
//        dd($row);
            // Check if the patient already exists by mobile number
//            $existingPatient = Patient::whereHas('user', function ($query) use ($row) {
//            $query->where('mobile', $row['mobile']);
//            })->first();
               // Check and convert the appointment date format 
            try { 
                $appointmentDate = is_numeric($row['appointment_date']) ? Carbon::instance(Date::excelToDateTimeObject($row['appointment_date']))->format('Y-m-d') : Carbon::createFromFormat('d/m/Y', $row['appointment_date'])->format('Y-m-d');      
                $appointmentDay = Carbon::createFromFormat('Y-m-d', $appointmentDate)->format('l');
            } catch (\Exception $e) { 
                $appointmentDate = Carbon::now()->format('Y-m-d');    
                $appointmentDay = Carbon::now()->format('l');
            } 
            $schedule = Schedule::where('doctor_id', 4)->where('available_days',$appointmentDay)->first();
            $obj = new AppointmentController();
            $slots = $obj->getSlot($schedule, $appointmentDay, $appointmentDate);
//            dd($slots);
            // Find the first available time slot 
            $firstAvailableSlot = null; 
            foreach ($slots as &$slot) { 
                if ($slot['booked'] === 0) { 
                    $firstAvailableSlot = $slot['time']; 
                    $slot['booked'] = 1; // Mark this slot as booked 
                    break; 
                    }                    
                }
//                dd( Carbon::parse($firstAvailableSlot)->format('H:i'));
            $existingPatient = User::where('mobile', $row['mobile'])->first();

            if (!$existingPatient) {
               
            $gen = explode('/', $row['gendage']);
            $user = new User();
            $user->name = $row['patnm'];
            $user->mobile = $row['mobile'];
            $user->address = $row['address'];
            $user->created_at = Carbon::instance(Date::excelToDateTimeObject($row['registrationdate']))->format('Y-m-d');
            $user->gender = ($gen[0] == 'M')? 1 : 0;
            $user->status = 1;
            $user->save();

            $user->assignRole(4);

            $patient = new Patient();
            $patient->user_id = $user->id;
            $patient->age = $gen[1];
            $patient->mrno = $row['mrno'];
            $patient->created_at = Carbon::instance(Date::excelToDateTimeObject($row['registrationdate']))->format('Y-m-d');
            $patient->save();

            $nextId = $patient ? $patient->id : '1';
            $patientId = "H-PA-00$nextId";
            $patient->patient_id = $patientId;
            $patient->save();
            $existingPatient = $user;
            }
           // Check if an appointment already exists for the same day 
           $existingAppointment = Appointment::where('patient_id', $existingPatient->id)->whereDate('date', $appointmentDate)->first(); 
           if (!$existingAppointment) { 
           // If no appointment exists, create a new one 
               if(!is_null($firstAvailableSlot)){
            $appointment = new Appointment();
            $appointment->patient_id = $existingPatient->id; 
            $appointment->doctor_id = 4; 
            $appointment->date = $appointmentDate; 
            $appointment->time = Carbon::parse($firstAvailableSlot)->format('H:i'); 
            $appointment->meeting_at = Carbon::parse($appointmentDate)->format('Y-m-d') . " " . Carbon::parse($firstAvailableSlot)->format('H:i');
            $appointment->description = $row['panel']; 
            $appointment->come_from = 'BMCHRC hospital'; 
            $appointment->status = 0; 
            $appointment->save(); 
            $nextId = $appointment ? $appointment->id : '1';
            $appointment_id = "H-APP-00$nextId";
            $appointment->appointment_id = $appointment_id;
            $appointment->save();
               }
           }
        }
            return true;
    }
}

