<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PrMedicalHistory;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps = true;
    protected $table="patient";
    protected $fillable=[
      'patient_id',
      'blood_group',
        'user_id',
        'visit_count',
        'last_visit_date'
      //'gender'
    ];
    public function appointment() {
      return $this->hasMany('App\Models\Appointment');
    }
    public function document() {
      return $this->hasMany(Document::class);
    }
    
    public function medicalHistory()
    {
        return $this->hasOne(PrMedicalHistory::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
