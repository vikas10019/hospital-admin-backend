<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrExercise extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps = true;
    protected $table="pr_exercise";
    protected $fillable=[
      'prescription_id','name','turns','notes'
    ];
}
