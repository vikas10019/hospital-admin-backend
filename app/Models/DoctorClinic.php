<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorClinic extends Model {

    use HasFactory,
        SoftDeletes;

    protected $table = "doctor_clinic";

    public function doctor() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
