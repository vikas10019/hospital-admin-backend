<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Source;
use App\Models\Lead;

class FollowUp extends Model {

    use HasFactory;

    protected $fillable = ['date', 'lead_id', 'source_id', 'comments', 'next_follow_up_date', 'follower_id'];

    // follower user
    public function follower() {
        return $this->belongsTo(User::class, 'follower_id', 'id');
    }
    // source
    public function source() {
        return $this->belongsTo(Source::class);
    }

    public function lead() {
        return $this->belongsTo(Lead::class);
    }

}
