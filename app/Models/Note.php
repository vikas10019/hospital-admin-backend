<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Note extends Model
{
    use HasFactory;
     // User
    public function user() {
        return $this->belongsTo(User::class);
    }
    // lead
    public function lead() {
        return $this->belongsTo(Lead::class);
    }
}
