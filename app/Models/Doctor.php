<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Schedule;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model {
    use HasFactory, SoftDeletes;
    public $timestamps = true;
    protected $table="doctor";
    protected $fillable=[
      'title',
      'phone',
      'designation',
      'specialization_id',
      'dob',
      'short_biography',
      'blood_group',
      'degree'
    ];
    public function document()
    {
      return $this->hasmany(Document::class);
    }
    public function schedule() {
      return $this->hasMany('App\Models\Schedule');
    }
      public function specialization() {
        return $this->belongsTo('App\Models\Speciality', 'specialization_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function generateApiKey()
    {
        $this->api_key = Str::random(60);
        $this->save();
    }
  
}
