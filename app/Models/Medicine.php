<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table="medicine";
    protected $fillable=[
      'name',
      'drug_type_id',
      'description',
      'quantity',
      'unit',
    ];
    
    public function drugType() {
        return $this->belongsTo(DrugType::class,'drug_type_id','id');
    }
    
     public function prescibeMedicine()
    {
        return $this->hasMany(PrescribeMedicine::class, 'medicine_id');
    }
}
