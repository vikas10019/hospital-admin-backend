<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
 {
    use HasFactory, SoftDeletes;
    protected $tables = "documents";
    protected $fillable = [
        'patient_id',
        'mobile',
        'doctor_id',
        'description',
        'patient_document',
        'upload_by'
    ];
    
    
    public function patient(){
        return $this->belongsTo('App\Models\User', 'patient_id', 'id');
    }
    
    public function doctor(){
        return $this->belongsTo('App\Models\User', 'doctor_id', 'id');
    }
}
              