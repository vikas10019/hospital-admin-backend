<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Appointment extends Model {

    use HasFactory, SoftDeletes;

    public $timestamps = true;
    protected $table = "appointment";
    protected $fillable = [
        'appointment_id',
        'patient_id',
        'patient_name',
        'department_id',
        'doctor_id',
        'schedule_id',
        'serial_no',
        'date',
        'time',
        'duration',
        'problems',
        'appointment_status',
        'status',
        'cancellation_reason'
    ];

    public function doctor() {
        return $this->belongsTo('App\Models\User', 'doctor_id', 'id');
    }

    public function patient() {
        return $this->belongsTo('App\Models\User', 'patient_id', 'id');
    }
    // if appointment has bill
    public function bill() {
        return $this->hasOne(Bill::Class);
    }

    public function scopeToday($query) {
        $date = Carbon::today();
    return $query->whereDate('meeting_at', $date);
    }
    
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }
   // appointment prescription
   public function prescription()
    {
        return $this->hasOne(Prescription::class);
    }
}
