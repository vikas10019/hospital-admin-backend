<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BedAssign extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table="bedassign";
    protected $fillable=[
      'serial',
      'patient_id',
      'mobile',
      'bed_id',
      'description',
      'assign_date',
      'discharge_date',
      'assign_by',
      'status'
    ];
    
    public function bed() {
        return $this->belongsTo(Bed::class, 'bed_id', 'id');
    }
    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id', 'id');
    }
}
