<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bill extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps=true;
    protected $table="bill";
    protected $fillable=[
        'bill_id',
        'bill_date',
        'appointment_id',
        'mobile',
        'discount',
        'tax',
        'date',
        'total',
        'payment_method',
        'card_check_no',
        'receipt_no',
        'notes',
        'status'
    ];
    
    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('quantity')->using(BillService::class)
                    ->withTimestamps();
    }
     public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}
