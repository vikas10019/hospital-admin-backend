<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DrugType extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table="drug_type";
    protected $fillable=[
      'name ',
    ];
}
