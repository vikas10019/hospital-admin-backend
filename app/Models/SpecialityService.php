<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialityService extends Model
{
    use HasFactory;
    protected $table="speciality_services";
    protected $fillable=[
      'speciality_id',
      'doctor_service_id',
    ];
    
    public function speciality() {
        return $this->belongsTo(Speciality::class, 'speciality_id', 'id');
    }
    public function doctorService() {
        return $this->belongsTo(DoctorService::class, 'doctor_service_id', 'id');
    }
}
