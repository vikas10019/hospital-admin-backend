<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrMedicalHistory extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps = true;
    protected $table="pr_medical_history";
    protected $fillable=[
      'patient_id','height','weight','bp_high','bp_low','genetic_history','pulse_rate'
    ];
     public function patient() {
        return $this->belongsTo('App\Models\User', 'patient_id', 'id');
    }
}
