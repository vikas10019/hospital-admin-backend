<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrescribeMedicine extends Model
{
    use HasFactory, SoftDeletes;
    public $timestamps = true;
    protected $table="prescribe_medicine";
    protected $fillable=[
      'prescription_id','drug_type_id','medicine_id','note','days','frequency'
    ];
     public function medicine()
    {
        return $this->belongsTo(Medicine::class);
    }
     public function drugType()
    {
        return $this->belongsTo(DrugType::class);
    }
}
