<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorHospital extends Model {

    use HasFactory,
        SoftDeletes;

    protected $table = "doctor_hospital";

    public function doctor() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
