<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investigation extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table="investigation";
    protected $fillable=[
      'name'
    ];
}
