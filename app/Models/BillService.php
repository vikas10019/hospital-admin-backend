<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillService extends Pivot
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = "bill_service";
}
