<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
 
    use HasFactory, SoftDeletes;
    
    protected $table="schedule";
    protected $fillable=[
      'doctor_id',
      'start_time',
      'end_time',
      'available_days',
      'per_patient_time',
      'serial_visibility_type',
      'status'
    ];
    public function doctor() {
        return $this->belongsTo(User::class,'doctor_id');
    }
    
}
