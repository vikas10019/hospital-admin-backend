<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\LeadSource;
use App\Models\LeadStatus;
use App\Models\ServiceType;
use App\Models\Industry;
use App\Models\Note;
use App\Models\Attachment;
use App\Models\FollowUp;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Country;
use App\Models\State;
use App\Models\City;

class Lead extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['email','phone','mobile','lead_status_id','company','lead_title','whatsapp','website','skype_id','secondary_email','twitter','lead_source_id','industry_id','assigned_id','description','zip_code','street1','street2','currency','service_type_id','is_public','campaign_name','lead_owner_id','title','first_name','last_name','is_convert'];
    protected $appends = ['full_name'];
     // Lead owner
    public function leadOwner() {
        return $this->belongsTo(User::class,'lead_owner_id','id');
    }
     // Lead Source
    public function leadSource() {
        return $this->belongsTo(LeadSource::class);
    }
     // Lead Status
    public function leadStatus() {
        return $this->belongsTo(LeadStatus::class);
    }
     // Industry
    public function industry() {
        return $this->belongsTo(Industry::class);
    }
     // assigned user
    public function assigned() {
        return $this->belongsTo(User::class,'assigned_id','id');
    }
    // notes
    public function notes() {
        return $this->hasMany(Note::class);
    }
    // Attachment
    public function attachments() {
        return $this->hasMany(Attachment::class);
    }
    // FollowUp
    public function followUps() {
        return $this->hasMany(FollowUp::class);
    }
    // full name
    public function getFullNameAttribute()
    {
        return $this?->title . ' ' .$this?->first_name . ' ' . $this?->last_name;
    }
     // country
    public function country() {
        return $this->belongsTo(Country::class);
    }
    // state
    public function state() {
        return $this->belongsTo(State::class);
    }
    public function city() {
        return $this->belongsTo(City::class);
    }
        public function latestFollowUp()
{
    return $this->hasOne(FollowUp::class)->latestOfMany('next_follow_up_date');
}
 // Lead Service type
    public function serviceType() {
        return $this->belongsTo(ServiceType::class);
    }
    // leads form
    public function formLead() {
        return $this->hasOne(FormLead::Class);
    }
}
