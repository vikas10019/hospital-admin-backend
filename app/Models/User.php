<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Document;
use App\Models\City;
use Illuminate\Support\Facades\Cache;
use App\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use HasFactory,
        Notifiable,
        HasApiTokens,
        HasRoles, 
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'mobile',
        'address',
        'picture',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function doctor() {
        return $this->belongsTo('App\Models\Doctor', 'id', 'user_id');
    }

    public function patient() {
        return $this->hasOne('App\Models\Patient', 'user_id');
    }

    public function schedule() {
        return $this->hasMany(Schedule::class, 'doctor_id', 'id');
    }

    public function appointment() {
        return $this->hasMany('App\Models\Appointment', 'doctor_id', 'id');
    }

    public function userAppointment() {
        return $this->hasMany('App\Models\Appointment', 'patient_id', 'id');
    }
    public function userDocuments() {
        return $this->hasMany('App\Models\Document', 'doctor_id', 'id');
    }
    public function document() {
        return $this->hasMany('App\Models\Document', 'patient_id', 'id');
    }
    
    
    public function scopeUser($query, $role) {
        $seconds = 24*60*60;
           $doctor =  $query->with('doctor.specialization')->role('Doctor')->orderBy('created_at', 'DESC')->get();
            Cache::add('doctor', $doctor, $seconds);
            return $doctor;
    }
     public function city() {
        return $this->belongsTo(City::class, 'city');
    }
    
     public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
    
    public function layout() {
        return $this->hasOne(Layout::class, 'doctor_id', 'id');
    }
    
    public function doctorSpecialityServices(){
        return $this->hasMany(DoctorSpecialityService::class, 'doctor_id', 'id');
    }
    public function doctorEducation(){
        return $this->hasMany(DoctorEducation::class, 'user_id', 'id');
    }
    public function doctorExperience(){
        return $this->hasMany(DoctorExperience::class, 'user_id', 'id');
    }
    public function prescription() {
        return $this->hasMany(Prescription::class,'patient_id','id');  
    }
    public function doctorClinic(){
        return $this->hasMany(DoctorClinic::class, 'user_id', 'id');
    }
    public function doctorHospital(){
        return $this->hasMany(DoctorHospital::class, 'user_id', 'id');
    }

}
