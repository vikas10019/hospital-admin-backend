<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Appointment;
use App\Models\Medicine;
use App\Models\DrugType;
use App\Models\PrExercise;
use App\Models\PrescribeMedicine;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prescription extends Model
{
    use HasFactory, SoftDeletes;
    protected $table    = 'prescription';
    protected $dates    = ['created_at','updated_at'];
    protected $fillable = ['patient_id','doctor_id','appointment_id','symptoms','clinic_diagnosis','test_prescribed','diet_advice '];
     public function doctor() {
        return $this->belongsTo('App\Models\User', 'doctor_id', 'id');
    }

    public function patient() {
        return $this->belongsTo('App\Models\User', 'patient_id', 'id');
    }
     public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
    
    
     public function exercise()
    {
        return $this->hasMany(PrExercise::class, 'prescription_id');
    }
    
    public function prescibeMedicine()
    {
        return $this->hasMany(PrescribeMedicine::class, 'prescription_id');
    }
    
    
}
