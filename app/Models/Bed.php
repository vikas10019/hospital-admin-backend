<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bed extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table="bed";
    protected $fillable=[
      'type',
      'description',
      'limit',
      'charge',
      'status'
    ];
    
    public function bedAssign() {
      return $this->hasMany(BedAssign::class);
    }
}
