<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormLead extends Model {

    use HasFactory;

    public function lead() {
        return $this->belongsTo(Lead::class);
    }

}
