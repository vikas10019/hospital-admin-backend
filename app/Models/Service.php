<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table="service";
    protected $fillable=[
      'name',
      'description',
      'quantity',
      'amount',
      'status'
    ];
    
    public function bills()
    {
        return $this->belongsToMany(Bill::class)->using(BillService::class)
                    ->withTimestamps();
    }
}
