<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorSpecialityService extends Model
{
    use HasFactory, SoftDeletes;
    protected $table="doctor_speciality_services";
    protected $fillable = [
        'deleted_at','speciality_service_id','doctor_id'
    ];
    
    public function specialityServices(){
        return $this->belongsTo(SpecialityService::class, 'speciality_service_id', 'id');
    }
}
