<?php

namespace App\Http\Controllers;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
 {
    public function addDepartment( Request $request )
 {
    $rules = [
        'name' => 'required',
        'description' => 'required',
    ];
    
    $validator = Validator::make( $request->all(), $rules);
    if ( $validator->fails() ) {
        return response()->json( array(
            'success' => false,
            'message' => 'There are incorect values in the form!',
            'errors' => $validator->getMessageBag()->toArray()
        ), 422 );
    }
    try{
        $department = new Department();
        $department->name = $request->input( 'name' );
        $department->description = $request->input( 'description' );
        $department->status = $request->input( 'status' );
        $department->save();
        $data = [
            'status' => 'success',
            'response' => $department,
        ];
    } catch (Exception $e) {
    $data = [
        'status' => 'error',
        'message' => [
            'error' => $e->getMessage(),
            'errorLine' => $e->getLine(),
            'errorFile' => $e->getFile()
        ],
        'response' => null
    ];
}
return response($data);
}
    public function ListingDepartment() {
        return Department::all();
//        return Department::paginate(10);
    }

    function deleteDepartment( $id ) {
        $result = Department::where( 'id', $id )->delete();
        if ( $result ) {
            return ['result'=> 'Department has been deleted'];
        }
    }

    public function EditDepartment( $id ) {
        return Department::find( $id );
    }

    public function updateDepartment( Request $request, $id )
 {
    $rules = [
        'name' => 'required',
        'description' => 'required',
    ];
    
    $validator = Validator::make( $request->all(), $rules);
    if ( $validator->fails() ) {
        return response()->json( array(
            'success' => false,
            'message' => 'There are incorect values in the form!',
            'errors' => $validator->getMessageBag()->toArray()
        ), 422 );
    }
    try{
        $department = Department::find( $id );
        $department->name = $request->input( 'name' );
        $department->description = $request->input( 'description' );
        $department->status = $request->input( 'status' );
        $department->save();
        $data = [
            'status' => 'success',
            'response' => $department,
        ];
    } catch (Exception $e) {
    $data = [
        'status' => 'error',
        'message' => [
            'error' => $e->getMessage(),
            'errorLine' => $e->getLine(),
            'errorFile' => $e->getFile()
        ],
        'response' => null
    ];
}
return response($data);
}
}
