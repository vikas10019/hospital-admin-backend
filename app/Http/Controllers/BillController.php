<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\File;
use Carbon\CarbonInterface;
use App\Models\Appointment;
use App\Models\Layout;
use App\Models\Setting;

class BillController extends Controller {

    /**
     * add bill
     * @param Request $request
     * @return type
     */
    public function addBill(Request $request) {
        $rules = [
            'bill_date' => 'required|date',
            'mobile' => 'required|numeric|digits:10',
            'discount' => 'nullable|numeric',
            'tax' => 'nullable|numeric',
            'total' => 'required|numeric',
            'payment_method' => 'required_if:status,1',
            'patient_id' => 'required',
            'doctor_id' => 'required',
            'services.*.id' => 'required',
            'services.*.quantity' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules, [
                    'patient_id.required' => 'The Patient field is required',
                    'doctor_id.required' => 'The Doctor field is required',
                    'services.*.id.required' => 'The Service name field is required',
                    'services.*.quantity.required' => 'The Service quantity field is required',
        ]);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $bill = new Bill();
            $bill->mobile = $request->input('mobile');
            $bill->appointment_id = $request->input('appointment_id');
            $bill->patient_id = $request->patient_id;
            $bill->doctor_id = $request->doctor_id;
            $bill->discount = $request->input('discount');
            $bill->tax = $request->input('tax');
            $bill->bill_date = Carbon::parse($request->input('bill_date'))->format('Y-m-d');
            $bill->total = $request->input('total');
            $bill->payment_method = $request->input('payment_method');
            $bill->card_check_no = $request->input('card_check_no');
            $bill->receipt_no = $request->input('receipt_no');
            $bill->notes = $request->input('notes');
            $bill->status = $request->input('status');
            if ($bill->save()) {
                    $series = "B.ID-000000";
                    $expNum = explode('-', $series);
                    $numbers = str_pad($expNum[1] + (int)$bill->id, 6, "0", STR_PAD_LEFT);
                    $bill->bill_id = $expNum[0] . '-' . $numbers;
                    $bill->save();
                if (!empty($request->services)) {
                    $services = $request->services;
//                    $quantity = $request->quantity;
                    foreach ($services as $key => $service) {
                        $bill->services()->attach($service['id'], ['quantity' => $service['quantity']]);
                    }
                }
                $data = array(
                    'status' => true,
                    'message' => 'Bill created successfully.',
                    "response" => $bill,
                );
                // Activity logs
             $causer = auth()->user();
             $log_name = 'bill';
             $operation = 'bill_add';
             $description = 'Bill of Dr. '.$bill?->doctor?->user?->name.' added for patient ' . $bill?->patient?->user?->name;
             $custom_properties = ['application' => config('app.name'),
                 'operation' => $operation,
                 'causer_name' => $causer->name,
             ];
             store_activity_log($causer, $bill, $custom_properties, $log_name, $description);
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * list bill
     * @param Request $request
     * @return type
     */
    public function listBill(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Bill::query()->with('doctor.user','doctor.specialization', 'patient.user', 'appointment');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('doctor_id', auth()->user()->doctor->id);
        }
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                        $query1->where('bill_id', 'like', '%' . $request->search . '%');
                    })
                    ->orWhereHas('patient.user', function ($query2) use ($request) {
                        $query2->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('patient.patient_id', 'like', '%' . $request->search . '%');
                    });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('bill_date', '>=', $start)->whereDate('bill_date', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') != 'patient.id' && request('field') != 'patient.user.name') {
                $query->orderBy(request('field'), request('sortOrder'));
            }
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') == 'patient.user.name' || request('field') == 'patient.id') {
                if (strtoupper(request('sortOrder')) == "DESC") {
                    $bill = $query->get()->sortByDesc(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                } else {
                    $bill = $query->get()->sortBy(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                }
                // Convert JSON data to associative array
                $arrayData = json_decode($bill, true);

                // Convert the associative array into the desired format
                $formattedArray = array_values($arrayData);

                // Output the formatted array as JSON
                $bills1 = json_encode($formattedArray, JSON_PRETTY_PRINT);
                $bills = paginate(json_decode($bills1, true), $length, null, ['path' => $request->url(), 'query' => $request->query()]);
            }
        } else {
            $bills = $query->paginate($length);
        }
        if (isset($bills)) {
            return $bills;
        } else {
            return $bills = $query->paginate($length);
        }
    }

    /**
     * delete bill 
     * @param type $id
     * @return type
     */
    public function deleteBill($id) {
        try {
            $bill = Bill::doesntHave('appointment')->find($id);

            if (!empty($bill)) {
                $serviceIds = $bill->services()->pluck('id');

                foreach ($serviceIds as $serviceId) {
                    $bill->services()->updateExistingPivot($serviceId, ['deleted_at' => now()]);
                }

                $bill->delete();
                $data = [
                    'status' => 'success',
                    'message' => 'Bill deleted successfully.'
                ];
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'The bill is associated with an appointment and cannot be deleted.',
                    "response" => '',
                );
            }
            return response($data);
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 422);
        }
    }

    /**
     * Edit bill
     * @param type $id
     * @return type
     */
    public function editBill($id) {
        return Bill::with('doctor.user', 'patient.user', 'services', 'appointment')->find($id);
    }

    /**
     * update bill
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateBill(Request $request, $id) {
        $rules = [
            'bill_date' => 'required|date',
            'mobile' => 'required|numeric|digits:10',
            'discount' => 'nullable|numeric',
            'tax' => 'nullable|numeric',
            'total' => 'required|numeric',
            'payment_method' => 'required_if:status,1',
            'patient_id' => 'required',
            'doctor_id' => 'required',
            'services.*.id' => 'required',
            'services.*.quantity' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules, [
                    'patient_id.required' => 'The Patient field is required',
                    'doctor_id.required' => 'The Doctor field is required',
                    'services.*.id.required' => 'The Service name field is required',
                    'services.*.quantity.required' => 'The Service quantity field is required',
        ]);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $bill = Bill::find($id);
            $bill->mobile = $request->input('mobile');
            $bill->discount = $request->input('discount');
            $bill->tax = $request->input('tax');
            $bill->bill_date = Carbon::parse($request->input('bill_date'))->format('Y-m-d');
            $bill->total = $request->input('total');
            $bill->payment_method = $request->input('payment_method');
            $bill->card_check_no = $request->input('card_check_no');
            $bill->receipt_no = $request->input('receipt_no');
            $bill->notes = $request->input('notes');
            $bill->status = $request->input('status');
            if ($bill->save()) {
                $bill->services()->detach();
                if (!empty($request->services)) {
                    $services = $request->services;
//                    $quantity = $request->quantity;
                    foreach ($services as $key => $service) {
                        $bill->services()->attach($service['id'], ['quantity' => $service['quantity']]);
                    }
                }
                $data = array(
                    'status' => true,
                    'message' => 'Bill created successfully.',
                    "response" => $bill,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * download Invoice
     * @param Request $request
     * @return type
     */
    public function downloadInvoice(Request $request) {
        if (isset($request->bill_id)) {
            $invoice = Bill::with('services', 'doctor.user', 'doctor.specialization', 'patient.user', 'doctor.user.schedule', 'appointment')->find($request->bill_id);
        } else {
            $appointment = Appointment::with('doctor.doctor', 'patient.patient')->find($request->appointment_id);
            $invoice = new Bill();
            $invoice->mobile = $appointment->patient->mobile;
            $invoice->appointment_id = $appointment->id;
            $invoice->patient_id = $appointment->patient->patient->id;
            $invoice->doctor_id = $appointment->doctor->doctor->id;
            $invoice->bill_date = now()->format('Y-m-d');
            $invoice->total = $appointment->doctor->doctor->price;
            $invoice->payment_method = "Cash";
            $invoice->status = 1;
            $invoice->save();
            $series = "B.ID-000000";
            $expNum = explode('-', $series);
            $numbers = str_pad($expNum[1] + (int)$invoice->id, 6, "0", STR_PAD_LEFT);
            $invoice->bill_id = $expNum[0] . '-' . $numbers;
            $invoice->save();
        }


        $numbers = str_pad($invoice->id, 4, "0", STR_PAD_LEFT);
        $billId = 'INV-' . $numbers;
        $layout = Layout::where('doctor_id',$invoice->doctor->user_id)->first();
        $setting = Setting::first();
        if($layout){
            switch($layout->bill_layout){
               case 1: $bill_layout = 'invoice.invoice3';
                   break;
               case 2: $bill_layout = 'invoice.invoice4';
                   break;
               case 3: $bill_layout = 'invoice.invoice5';
                   break;
               default: $bill_layout = 'invoice.invoice3';
            }
        }else{
            $bill_layout = 'invoice.invoice3';
        }
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('invoice.invoice6', compact('invoice', 'billId','setting'));
        $headers = [
            'Content-Type' => 'application/pdf',
//                'Access-Control-Allow-Origin' => '*',
        ];
//        return $pdf->download('invoice.pdf',$headers);
//        $output = $pdf->output();
        $day = date('dmY');
        $path = storage_path('app/public/pdf/' . $day);
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        $fileName = time() . '.pdf';
        $pdf->save($path . '/' . $fileName);
//        $output = $pdf->output();

        $file = public_path("storage/pdf/" . $day . '/' . $fileName);
//    $file = public_path("storage/pdf/02042024/1712056628.pdf");
//    
//    file_put_contents($name,$output);
        return response()->download($file, $fileName, $headers);
    }
    /**
     * Bill Details
     * @param type $id
     * @return type
     */
    public function billDetails($id) {
         try {
        $bill = Bill::with('doctor.user.layout', 'doctor.specialization', 'patient.user', 'services', 'appointment')->find($id); 
        $setting = Setting::first();
        if($bill){
            return response()->json([
                        'success' => true,
                        'message' => 'Bill Details fetch successfully.',
                        'response' => ['bill'=>$bill,'setting'=>$setting]
                            ], 200);
        }else{
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'response' => ''
                            ), 500);
        }
         } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }

}
