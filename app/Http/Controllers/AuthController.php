<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller {

    public function register(Request $request) {

        return User::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => Hash::make($request->input('password'))
                ]);
    }

    public function login(Request $request) {
        if (!$this->validateEmail($request->email)) {
            // this is validate to fail send mail or true
            return response()->json([
                        "status" =>false,
                        'message' => 'Email does\'nt found on our database'
                            ], 400);
        }
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response([
                "status" =>false,
                'message' => 'Invalid credentials!'
                    ], 400);
        }
        $user = Auth::user();
        if (!$user->hasVerifiedEmail()) {
            return response()->json([
                "status" =>false,
                'message' => 'Email not verified'], 400);
        }
        $token = $user->createToken('token')->plainTextToken;
        $cookie = cookie('jwt', $token, 60 * 24);
        $permissions = Auth::user()->getAllPermissions();
        if (count($permissions)) {
            $permissions = array_column($permissions->toArray(), 'name');
        }
        $roles = Auth::user()->roles()->get();
        if (count($roles)) {
            $roles = array_column($roles->toArray(), 'name');
        }
        return response([
                    'status' => true,
                    'token' => $token,
                    'message' => $token,
                    'roles' => $roles,
                    'permissions' => $permissions
                ],200)->withCookie($cookie);
    }

    public function validateEmail($email) {
        return !!User::where('email', $email)->first();
    }

    public function user() {
        return Auth::user();
    }

    public function logout() {
        $cookie = Cookie::forget('jwt');

        return response([
                    'message' => 'Success'
                ])->withCookie($cookie);
    }

}
