<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Schedule;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ScheduleController extends Controller {

    public function __construct() {
        $this->middleware('permission:schedule_list', ['only' => ['scheduleListing']]);
        $this->middleware('permission:schedule_create', ['only' => ['addschedule']]);
//        $this->middleware('permission:schedule_edit', ['only' => ['editSchedule', 'updateSchedule']]);
        $this->middleware('permission:schedule_delete', ['only' => ['deleteSchedule']]);
    }

    /**
     * doctor schedule add
     * 
     * @param Request $request
     * @return type
     */
    public function addschedule(Request $request) {
        $rules = [
            'common.*' => 'required|array',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        $saved = false;
        try {
            foreach ($request->common as $data) {
                $doctor_id = $data['doctor_id'];
                $per_patient_time = $data['per_patient_time'];
                $status = $data['status'];
            }
            if (Schedule::exists($doctor_id)) {
//                Schedule::where('doctor_id', $doctor_id)->delete();
                 return response()->json(array(
                        'status' => false,
                        'message' => 'Doctor already has schedule!'
                            ), 400);
            }
            foreach ($request->days as $key => $user) {
                if (isset($user['checked']) && $user['checked'] == true && !empty($user['startTime']) && !empty($user['endTime'])) {
                    $schedule = new Schedule();
                    $schedule->available_days = $user['available_days'];
                    $schedule->doctor_id = $doctor_id;
                    $schedule->start_time = $user['startTime'];
                    $schedule->end_time = $user['endTime'];
                    $schedule->per_patient_time = $per_patient_time;
                    $schedule->status = $status;
                    $saved = $schedule->save();
                }
            }
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'schedule';
            $operation = 'schedule_add';
            $description = 'Schedule added for Dr. '.$schedule?->doctor?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $schedule, $custom_properties, $log_name, $description);
        } catch (Exception $e) {
            $saved = false;
        }
        return response()->json($saved);
    }

    /**
     * all doctor schedule listing show
     * @param Request $request
     * @return type
     */
    public function scheduleListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = User::query()->has('schedule')->with('schedule', 'doctor.specialization')->role('doctor');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('id', auth()->user()->id);
        }
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%')
                        ->orWhereHas('schedule', function ($query2) use ($request) {
                            $query2->where('available_days', 'like', '%' . $request->search . '%');
                        });
            });
//            $query->with(['schedule' => fn($q) => $q->where('available_days', 'like', '%' . $request->search . '%')]);
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        $schedule = $query->paginate($length);
        return response()->json($schedule);
    }

    /**
     * doctor schedule delete by doctor id
     * 
     * @param type $doctor_id
     * @return type
     */
   public function deleteSchedule($doctor_id) {
    $schedule = Schedule::with('doctor')->where('doctor_id', $doctor_id)->get();

    // Activity logs
    $causer = auth()->user();
    $roleName = $causer->roles()->pluck('name')->first();
    $log_name = 'schedule';
    $operation = 'schedule_delete';
    $description = 'Schedule deleted for Dr. '.$schedule->first()?->doctor?->name;
    $custom_properties = ['application' => config('app.name'),
        'operation' => $operation,
        'causer_name' => $causer->name,
        'role_name' =>$roleName
    ];
    store_activity_log($causer, $schedule->first(), $custom_properties, $log_name, $description);

    // Delete each schedule item
    $deleted = true;
    foreach ($schedule as $item) {
        if (!$item->delete()) {
            $deleted = false;
        }
    }

    if ($deleted) {
        return response()->json(array(
            'status' => true,
            'message' => 'The Schedule has been deleted.',
            'response' => '',
        ), 200);
    } else {
        return response()->json(array(
            'status' => false,
            'message' => 'Something went wrong, Please try again later!',
            'response' => '',
        ), 400);
    }
}


    /**
     * get doctor schedule detail by id
     * 
     * @param type $doctor_id
     * @return type
     */
    public function editSchedule($doctor_id) {
        $schedule = Schedule::where('doctor_id', $doctor_id)->get();
        return response()->json($schedule);
    }

    /**
     * update doctor detail by id
     * 
     * @param Request $request
     * @param type $doctor_id
     * @return type
     */
    public function updateSchedule(Request $request) {
        $rules = [
            'common.*' => 'required|array',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        $saved = "";
        try {
            foreach ($request->common as $data) {
                $doctor_id = $data['doctor_id'];
                $per_patient_time = $data['per_patient_time'];
                $status = $data['status'];
            }
            $scheduleIds = array();
            foreach ($request->days as $data) {
                if (isset($data['checked']) && $data['checked'] == true && isset($data['id'])) {
                    $scheduleIds[] = $data['id'];
                }
            }

            Schedule::where('doctor_id', $doctor_id)->whereNotIn('id', $scheduleIds)->delete();
            foreach ($request->days as $key => $data) {
                if (isset($data['checked']) && $data['checked'] == true && $data['startTime'] != null && $data['endTime'] != null) {
                    if (isset($data['checked']) && $data['checked'] == true && isset($data['id']) && $data['startTime'] != null && $data['endTime'] != null) {
                        $schedule = Schedule::where('id', $data['id'])->first();
                    } else {
                        $schedule = new Schedule;
                    }
                    $schedule->available_days = $data['available_days'];
                    $schedule->doctor_id = $doctor_id;
                    $schedule->start_time = $data['startTime'];
                    $schedule->end_time = $data['endTime'];
                    $schedule->per_patient_time = $per_patient_time;
                    $schedule->status = $status;
                    $saved = $schedule->save();
                }
            }
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'schedule';
            $operation = 'schedule_update';
            $description = 'Schedule updated for Dr. '.$schedule?->doctor?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $schedule, $custom_properties, $log_name, $description);
        } catch (Exception $e) {
            $saved = false;
        }

        return response()->json($saved);
    }

}
