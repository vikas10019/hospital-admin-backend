<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ServiceController extends Controller {

    /**
     * Add Service
     * @param Request $request
     * @return type
     */
    public function addService(Request $request) {
        $rules = [
            'name' => 'required|regex: /^[a-zA-Z0-9\s\.\-]{2,40}$/|max:255|unique:service,name',
            'description' => 'nullable|max:255',
            'quantity' => 'required|numeric',
            'amount' => 'required|numeric',
            'status' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $service = new Service();
            $service->name = $request->input('name');
            $service->description = $request->input('description');
            $service->quantity = $request->input('quantity');
            $service->amount = $request->input('amount');
            $service->status = $request->input('status');
            if ($service->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'The Service created successfully.',
                    "response" => $service,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $e->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * List Service
     * @param Request $request
     * @return type
     */
    function listService(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Service::query();
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('amount', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        $services = $query->paginate($length);
        return $services;
    }

    /**
     * Service delete by id
     * 
     * @param type $id
     * @return type
     */
    function deleteService($id) {
        try {
            $service = Service::with('bills')->find($id);
            if($service){
                if(!$service->bills->isEmpty()){
                      return response()->json(array(
                                        'status' => false,
                                        'message' => 'The Service has attached with bill and can not be deleted.',
                                        "response" => '',
                                            ), 400);
                  }else{
                      $service->delete();
                      return response()->json(array(
                                      'status' => true,
                                      'message' => 'The Service has been deleted.',
                                      "response" => '',
                                          ), 200);
                  }
            }else{
                return response()->json(array(
                        'status' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        "response" => '',
                    ), 400);
            }
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 422);
        }     
    }

    /**
     * Edit Service
     * @param type $id
     * @return type
     */
    public function editService($id) {
        return Service::find($id);
    }

    /**
     * Update Service
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateService(Request $request, $id) {
        $rules = [
            'name' => 'required|regex: /^[a-zA-Z0-9\s\.\-]{2,40}$/|max:255|unique:service,name,' . $id,
            'description' => 'nullable|max:255',
            'quantity' => 'required|numeric',
            'amount' => 'required|numeric',
            'status' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $service = Service::find($id);

            $service->name = $request->input('name');
            $service->description = $request->input('description');
            $service->quantity = $request->input('quantity');
            $service->amount = $request->input('amount');
            $service->status = $request->input('status');
            if ($service->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'The Service created successfully.',
                    "response" => $service,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
        return response($data);
    }

    /**
     * common service list
     * @return type
     */
    public function serviceList() {
        return Service::all();
    }

}
