<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Models\User;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

   public function verify($user_id, Request $request) {
    if (!$request->hasValidSignature()) {
        return response()->json([
            "status" =>false,
            "message" => "Invalid/Expired url provided."], 401);
    }

    $user = User::findOrFail($user_id);

    if (!$user->hasVerifiedEmail()) {
        $user->markEmailAsVerified();
        $user->status = 1;
        $user->save();
    }else{
       return response()->json([
            "status" =>false,
            "message" => "Email already verified."], 400); 
    }

    return response()->json([
        "status" =>true,
        'message' => 'Email verified successfully.'], 200);
}

public function resend($user_id) {
    $user = User::findOrFail($user_id);
    if ($user->hasVerifiedEmail()) {
        return response()->json([
            "status" =>false,
            "message" => "Email already verified."], 400);
    }

    $user->sendEmailVerificationNotification();

    return response()->json([
        "status" =>true,
        "message" => "Email verification link sent on your email id"],200);
}
}
