<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\hasfile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Exception;
use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;
use Mail;
use App\Mail\Register;
use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notifiable;
use App\Notifications\VerifyEmail;
use App\Models\Layout;
use App\Models\Appointment;
use App\Jobs\EmailVerificationJob;
use App\Models\SpecialityService;
use App\Models\DoctorService;
use App\Models\DoctorSpecialityService;
use App\Models\DoctorEducation;
use App\Models\DoctorExperience;
use App\Models\DoctorClinic;
use App\Models\DoctorHospital;

class AddDoctorController extends Controller {

    function __construct() {
        $this->middleware('permission:doctor_list', ['only' => ['doctorListing']]);
        $this->middleware('permission:doctor_create', ['only' => ['addDoctor']]);
        $this->middleware('permission:doctor_edit', ['only' => ['doctorEdit', 'updateDoctor']]);
        $this->middleware('permission:doctor_delete', ['only' => ['deleteDoctor']]);
        $this->middleware('permission:layout_list', ['only' => ['layoutStore']]);
    }

    /**
     * New Doctor create
     * 
     * @param Request $request
     * @return type
     */
    public function addDoctor(Request $request) {

        $rules = [
            'name' => 'required|regex: /^[a-z\s\.]+$/i|min:2|max:40',
            'password' => 'required|min:8',
            'email' => 'required|email|unique:users,email|email:rfc,dns',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'degree' => 'nullable|mimes:pdf,jpg,png,jpeg,gif,webp|max:5120',
            'phone' => 'nullable|numeric|digits:10|unique:doctor,phone',
            'address' => 'nullable|max:255',
            'specialization' => 'required',
            'gender' => 'required',
            'password' => 'required|min:8',
            'dob' => 'nullable|before:today',
            'price' => 'required|numeric',
            'clinic_logo' => 'nullable|mimes:pdf,jpg,png,jpeg,gif,svg,webp|max:10',
            'clinic_tagline' => 'nullable|max:100',
            'clinic_website' => 'nullable|max:100',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        try {
            if ($request->hasFile('picture')) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'users'");
                $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();

                $file_name = time() . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $pathPicture = 'doctor' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'doctor');
            } else {
                $pathPicture = NULL;
            }

            if ($request->hasFile('degree')) {
                if (empty($nextId)) {
                    $statement = DB::select("SHOW TABLE STATUS LIKE 'users'");
                    $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();
                }
                $file_name = time() . 'degree_' . $nextId;
                $fileName = $file_name . '.' . $request->file('degree')->getClientOriginalExtension();
                $pathDegree = 'doctor' . '/' . $request->file('degree')->storeAs($nextId, $fileName, 'doctor');
            } else {
                $pathDegree = NULL;
            }
            if ($request->hasFile('clinic_logo')) {
                if (empty($nextId)) {
                    $statement = DB::select("SHOW TABLE STATUS LIKE 'users'");
                    $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();
                }
                $file_name = time() . 'clinic_logo' . $nextId;
                $fileName = $file_name . '.' . $request->file('clinic_logo')->getClientOriginalExtension();
                $clinicLogo = 'doctor' . '/' . $request->file('clinic_logo')->storeAs($nextId, $fileName, 'doctor');
            } else {
                $clinicLogo = NULL;
            }
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->mobile = $request->input('mobile');
            $user->address = $request->input('address');
            $user->picture = $pathPicture;
            $user->gender = $request->input('gender');
            $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
            $user->status = $request->input('status') ?? 0;
            $user->save();

            $user->assignRole('Doctor');
            $doctor = new Doctor();
            $doctor->title = $request->input('title');
            $doctor->user_id = $user->id;
            $doctor->designation = $request->input('designation');
            $doctor->specialization_id = $request->input('specialization');
            $doctor->short_biography = $request->input('short_biography');
            $doctor->blood_group = $request->input('blood_group');
            $doctor->degree = $pathDegree;
            $doctor->phone = $request->input('phone');
            $doctor->price = $request->price;
            $doctor->clinic_name = $request->clinic_name;
            $doctor->clinic_timings = $request->clinic_timings;
            $doctor->clinic_address = $request->clinic_address;
            $doctor->clinic_logo = $clinicLogo;
            $doctor->clinic_tagline = $request->clinic_tagline;
            $doctor->clinic_website = $request->clinic_website;
//            $doctor->webhook_url = $request->webhook_url;
            $layoutData = new Layout();
            $layoutData->doctor_id = $user->id;
            $layoutData->bill_layout = 1;
            $layoutData->prescription_layout = 1;
            $layoutData->is_default = 1;
            $layoutData->save();
            if (!empty($request->speciality_services)) {
                $specialityServices = json_decode($request->speciality_services, true);
                foreach ($specialityServices as $key => $speciality_services) {
                    if (!isset($speciality_services['id'])) {
                        $serviceNew = DoctorService::firstOrCreate(['name' => $speciality_services['value']]);
                        if ($serviceNew) {
                            $specialityService = SpecialityService::firstOrCreate(['speciality_id' => $request->specialization, 'doctor_service_id' => $serviceNew->id]);
                        }
                    } else {
                        $specialityService = SpecialityService::find($speciality_services['id']);
                    }
                    $doctorSpecialityService = new DoctorSpecialityService();
                    $doctorSpecialityService->doctor_id = $user->id;
                    $doctorSpecialityService->speciality_service_id = $specialityService->id;
                    $doctorSpecialityService->save();
                }
            }
            if ($doctor->save()) {
                $doctor->generateApiKey();
                // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_add';
            $description = 'doctor ' . $user?->name .' added';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                Cache::forget('doctor');
                User::user('doctor');
//                if (smtpConnect() == true) {
//                    $doctorMail = array(
//                        'email' => $request->email,
//                        'password' => $request->password,
//                    );
//                    Mail::to($user->email)->send(new Register($doctorMail));
//                }
                dispatch(new EmailVerificationJob($user->id));
//                $user->sendEmailVerificationNotification();
                $data = array(
                    'status' => true,
                    'message' => 'Doctor details stored successfully.A fresh verification link has been sent to your email address.',
                    "response" => $doctor,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * All doctor listing get
     * 
     * @return type
     */
    public function doctorListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
//        if (Cache::has('doctor') && (!isset($request->search) || !isset($request->field)) ) {
//            $doctor = Cache::get('doctor');
//        } else {
//            Cache::forget('doctor');
        $query = User::query()->with('doctor.specialization', 'layout', 'doctorSpecialityServices.specialityServices.speciality', 'doctorSpecialityServices.specialityServices.doctorService')->role('Doctor');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('id', auth()->user()->id);
        }
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('email', 'like', '%' . $request->search . '%')
                        ->orWhere('mobile', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        $doctor = $query->paginate($length);
//            $seconds = 24*60*60;
//            Cache::add('doctor', $doctor, $seconds);
//
//        }
        return $doctor;
    }

    /**
     * All doctor listing get
     * 
     * @return type
     */
    public function getDoctor() {
        $query = User::query()->with('doctor.specialization', 'schedule', 'doctorSpecialityServices.specialityServices.doctorService')->role('Doctor');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('id', auth()->user()->id);
        }
        $doctor = $query->where('status', 1)->get();
        return response()->json($doctor);
    }

    /**
     * delete doctor by id
     * 
     * @param type $id
     * @return type
     */
    public function deleteDoctor($id) {
        try {
            $user = User::doesntHave('appointment')->doesntHave('userDocuments')->with('doctor', 'doctorSpecialityServices')->role('Doctor')->find($id);
            if (!empty($user)) {
                if ($user->id == auth()->user()->id) {
                    return response()->json(array(
                                'success' => false,
                                'message' => 'You cannot delete yourself!',
                                    ), 500);
                }
//                $user->removeRole('Doctor');

                $serviceIds = $user->doctorSpecialityServices;
                foreach ($serviceIds as $serviceId) { 
                    $serviceId->where('speciality_service_id', $serviceId->speciality_service_id)->update(['deleted_at' => now()]);           
                }
                $user->doctor()->delete();
                
//                $user->doctorSpecialityServices()->delete();
                $user->schedule()->delete();
                $user->doctorExperience()->delete();
                $user->doctorEducation()->delete();
                if ($user->delete()) {
                    Cache::forget('doctor');
                    User::user('doctor');
                    $data = array(
                        'status' => true,
                        'message' => 'Doctor has been successfully deleted.',
                        "response" => null,
                    );
                } else {
                    $data = array(
                        'status' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        "response" => '',
                    );
                }
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'The doctor is attached with an appointment or user documents and cannot be deleted.',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * get doctor detail by id
     * 
     * @param type $id
     * @return type
     */
    public function doctorEdit($id) {
        return User::with('doctor', 'doctorSpecialityServices.specialityServices.doctorService')->role('Doctor')->find($id);
    }

    /**
     * update doctor detail by id
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateDoctor(Request $request, $id) {
        $rules = [
            'name' => 'required|regex: /^[a-z\s\.]+$/i|min:2|max:40',
            'email' => 'nullable|email|email:rfc,dns|unique:users,email,' . $id,
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,' . $id,
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'degree' => 'nullable|mimes:pdf,jpg,png,jpeg,gif,webp|max:5120',
            'phone' => 'nullable|numeric|digits:10|unique:doctor,phone,' . Doctor::where('user_id', $id)->value('id'),
            'address' => 'nullable|max:255',
            'specialization' => 'required',
            'gender' => 'required',
            'password' => 'nullable|min:8',
            'dob' => 'nullable|before:today',
            'price' => 'required|numeric',
            'clinic_logo' => 'nullable|mimes:pdf,jpg,png,jpeg,gif,svg,webp|max:10',
            'clinic_tagline' => 'nullable|max:100',
            'clinic_website' => 'nullable|max:100',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $user = User::with('doctor')->role('Doctor')->find($id);
            if ($request->hasFile('picture')) {
                $nextId = $id;
                $file_name = time() . '_' . $id;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $pathPicture = 'doctor' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'doctor');
                if (File::exists($user->picture)) {
                    unlink($user->picture);
                }
                $user->picture = $pathPicture;
            }
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if (!empty($request->get('password'))) {
                $user->password = Hash::make($request->input('password'));
            }
            $user->mobile = $request->input('mobile');
            $user->address = $request->input('address');
            $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
            $user->gender = $request->input('gender');
            $user->status = $request->input('status') ?? 0;
            $user->save();
            if (!empty($request->speciality_services)) {
                DoctorSpecialityService::where('doctor_id', $user->id)->delete();
                $specialityServices = json_decode($request->speciality_services, true);
                foreach ($specialityServices as $key => $speciality_services) {
                    if (!isset($speciality_services['id'])) {
                        $serviceNew = DoctorService::firstOrCreate(['name' => $speciality_services['value']]);
                        if ($serviceNew) {
                            $specialityService = SpecialityService::firstOrCreate(['speciality_id' => $request->specialization, 'doctor_service_id' => $serviceNew->id]);
                        }
                    } else {
                        $specialityService = SpecialityService::find($speciality_services['id']);
                    }
                    $doctorSpecialityService = new DoctorSpecialityService();
                    $doctorSpecialityService->doctor_id = $user->id;
                    $doctorSpecialityService->speciality_service_id = $specialityService->id;
                    $doctorSpecialityService->save();
                }
            } else {
                DoctorSpecialityService::where('doctor_id', $user->id)->delete();
            }
            $doctor = Doctor::where('user_id', $user->id)->first();
            $doctor->title = $request->input('title');
            $doctor->user_id = $user->id;
            $doctor->designation = $request->input('designation');
            $doctor->specialization_id = $request->input('specialization');
            $doctor->short_biography = $request->input('short_biography');
            $doctor->blood_group = $request->input('blood_group');
//            $doctor->webhook_url = $request->webhook_url;
            if ($request->hasFile('degree')) {
                $nextId = $id;
                $file_name = time() . 'degree_' . $id;
                $fileName = $file_name . '.' . $request->file('degree')->getClientOriginalExtension();
                $pathDegree = 'doctor' . '/' . $request->file('degree')->storeAs($nextId, $fileName, 'doctor');
                if (File::exists($doctor->degree)) {
                    unlink($doctor->degree);
                }
                $doctor->degree = $pathDegree;
            }
            if ($request->hasFile('clinic_logo')) {
                $nextId = $id;
                $file_name = time() . 'clinic_logo_' . $id;
                $fileName = $file_name . '.' . $request->file('clinic_logo')->getClientOriginalExtension();
                $clinicLogo = 'doctor' . '/' . $request->file('clinic_logo')->storeAs($nextId, $fileName, 'doctor');
                if (File::exists($doctor->clinic_logo)) {
                    unlink($doctor->clinic_logo);
                }
                $doctor->clinic_logo = $clinicLogo;
            }
            $doctor->phone = $request->input('phone');
            $doctor->price = $request->price;
            $doctor->clinic_name = $request->clinic_name;
            $doctor->clinic_timings = $request->clinic_timings;
            $doctor->clinic_address = $request->clinic_address;
            $doctor->clinic_tagline = $request->clinic_tagline;
            $doctor->clinic_website = $request->clinic_website;
            if ($doctor->save()) {
//                $doctor->generateApiKey();
                    // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_modification';
            $description = 'modification in Dr. ' . $user?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                Cache::forget('doctor');
                $data = array(
                    'status' => true,
                    'message' => 'Doctor details updated successfully.',
                    "response" => $doctor,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * get doctor detail by id
     * 
     * @param type $id
     * @return type
     */
    public function doctorDetails(Request $request, $id) {
        $doctorDetail = User::with('doctor.specialization', 'schedule', 'doctorSpecialityServices.specialityServices.doctorService')->where('id', $id)->role('Doctor')->first();
        $appointments = Appointment::with('doctor.doctor', 'patient.patient', 'bill')->where('meeting_at', '>=', now()->format('Y-m-d H:i:s'))->where('doctor_id', $id)->latest()->paginate(10);
        return response(["doctorDetail" => $doctorDetail, "appointments" => $appointments]);
    }

    /**
     * doctor detail update
     * 
     * @param Request $request
     * @return type
     */
    public function updateDoctorDetails(Request $request) {
        $rules = [
            'name' => 'required|regex:/^[a-z\s\.]+$/i|min:2|max:40',
            'password' => 'nullable|min:8',
            'email' => 'required|email|email:rfc,dns|unique:users,email,' . $request->input('id'),
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,' . $request->input('id'),
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'degree' => 'nullable|mimes:pdf,jpg,png,jpeg,gif,webp|max:5120',
            'phone' => 'nullable|numeric|digits:10|unique:doctor,phone,' . Doctor::where('user_id', $request->id)->value('id'),
            'address' => 'nullable|max:255',
            'specialization' => 'required',
            'gender' => 'required',
            'dob' => 'nullable|before:today',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $user = User::with('doctor')->where('id', $request->input('id'))->role('Doctor')->first();
            if ($request->hasFile('picture')) {
                $nextId = $file_name = time() . '_' . $request->input('id');
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $pathPicture = 'doctor' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'doctor');
                if (File::exists($user->picture)) {
                    unlink($user->picture);
                }
                $user->picture = $pathPicture;
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->mobile = $request->input('mobile');
            $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
            $user->address = $request->input('address');
            $user->gender = $request->input('gender');
            $user->status = $request->input('status');
            if ($user->save()) {
                $doctor = $user->doctor;
                if ($request->hasFile('degree')) {
                    $statement = DB::select("SHOW TABLE STATUS LIKE 'doctor'");
                    $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();
                    $file_name = time() . '_' . $nextId;
                    $fileName = $file_name . '.' . $request->file('degree')->getClientOriginalExtension();
                    $pathDegree = 'doctor' . '/' . $request->file('degree')->storeAs($nextId, $fileName, 'doctor');
                    if (File::exists($doctor->degree)) {
                        unlink($doctor->degree);
                    }
                    $doctor->degree = $pathDegree;
                }
                $doctor->user_id = $user->id;
                $doctor->title = $request->input('title');
                $doctor->specialization_id = $request->input('specialization');
                $doctor->designation = $request->input('designation');
                $doctor->short_biography = $request->input('short_biography');
                $doctor->blood_group = $request->input('blood_group');
                $doctor->phone = $request->input('phone');
                $doctor->clinic_name = $request->clinic_name;
                $doctor->clinic_timings = $request->clinic_timings;
                $doctor->clinic_address = $request->clinic_address;
//                $doctor->webhook_url = $request->webhook_url;
                if ($doctor->save()) {
                     // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_modification';
            $description = 'modification in Dr. ' . $user?->name.'from details';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                    Cache::forget('doctor');
                    $user = User::with('doctor.specialization')->where('id', $request->input('id'))->first();
                    $data = array(
                        'status' => true,
                        'message' => 'Doctor details updated successfully.',
                        "response" => $user,
                    );
                } else {
                    $data = array(
                        'status' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        "response" => '',
                    );
                }
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Doctor Password update by id
     * 
     * @param Request $request
     * @return type
     */
    public function updateDoctorpassword(Request $request) {
        $user = User::where('id', $request->input('id'))->first();
        $validator = Validator::make($request->all(), [
                    'old_password' => [
                        'required', function ($attribute, $value, $fail) use ($user) {
                            if (!Hash::check($value, $user->password)) {
                                $fail('Old Password didn\'t match');
                            }
                        },
                    ],
                    'password' => ['required', 'string', 'min:8', 'different:old_password', 'confirmed'],
        ]);

        //checking validation
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {
            $user = User::where('id', $request->input('id'))->update(array('password' => Hash::make(strip_tags($request->password))));
            if ($user) {
                 // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_password_update';
            $description = 'Password update in Dr. ' . $user?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                $data = array(
                    'status' => true,
                    'message' => 'Password updated successfully.',
                    "response" => '',
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * ckeditor image uploads
     * 
     * @param Request $request
     * @return type
     */
    public function uploadImages(Request $request) {

        if ($request->hasFile('files')) {
            $originName = $request->file('files')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('files')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;
            $request->file('files')->move(public_path('/storage/ckeditor'), $fileName);
            $url = asset('storage/ckeditor/' . $fileName);
            $funcNum = $request->input('CKEditorFuncNum');
            $msg = 'Image uploaded successfully';
            $response = '<script>window.parent.CKEDITOR.tools.callFunction(1,' . $url . ', ' . $msg . ')</script>';
            @header('Content-type: text/html; charset=utf-8');
            return response()->json(['url' => $url, 'function' => $funcNum, 'message' => $msg]);
        }
    }

    public function profileUpdate(Request $request, $id) {
        $rules = [
            'name' => 'required|regex: /^[a-z\s\.]+$/i|min:2|max:40',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,' . $id,
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'address' => 'nullable|max:255',
            'gender' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {

            $user = User::find($id);
            if ($request->hasFile('picture')) {
                $nextId = $file_name = time() . '_' . $id;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                if ($user->hasRole('Doctor')) {
                    $path = 'doctor' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'doctor');
                } elseif ($user->hasRole('Staff')) {
                    $path = 'staff' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'staff');
                } elseif ($user->hasRole('Admin')) {
                    $path = 'admin' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'admin');
                }
                if (File::exists($user->picture)) {
                    unlink($user->picture);
                }
                $user->picture = $path;
            }
            $user->name = $request->name;
            $user->mobile = $request->mobile;
            $user->address = $request->address;
            $user->gender = $request->gender;
            if ($user->save()) {
                 // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'Profile Update';
            $operation = 'profile-update';
            $description = $user?->name .' profile Updated';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                $data = array(
                    'status' => true,
                    'message' => 'User details updated successfully.',
                    "response" => $user,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * layout store and update
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function layoutStore(Request $request) {
        try {
            if ($request->is_default) {
                $doctors = User::role('Doctor')->where('status', 1)->get();
                if (isset($doctors)) {
                    foreach ($doctors as $doctor) {
                        $layoutData = Layout::where('doctor_id', $doctor->id)->first();
                        if (empty($layoutData)) {
                            $layoutData = new Layout();
                        }
                        $layoutData->doctor_id = $doctor->id;
                        $layoutData->bill_layout = 1;
                        $layoutData->prescription_layout = 1;
                        $layoutData->is_default = 1;
                        $save = $layoutData->save();
                    }
                }
            } else {
                $doctors = User::role('Doctor')->where('status', 1)->get();
                if (isset($doctors)) {
                    foreach ($doctors as $doctor) {
                        $layout = Layout::where('doctor_id', $doctor->id)->first();
                        if (!empty($layout)) {
                            $layout->is_default = $request->is_default;
                            $layout->save();
                        }
                    }
                }
                if (isset($request->layout_id)) {
                    $layoutData = Layout::findOrfail($request->layout_id);
                } else {
                    $layoutData = new Layout();
                }
                $layoutData->doctor_id = $request->doctor_id;
                $layoutData->bill_layout = $request->bill_layout;
                $layoutData->prescription_layout = $request->prescription_layout;
                $layoutData->is_default = $request->is_default;
                $save = $layoutData->save();
            }
            if ($save) {
                $data = array(
                    'status' => true,
                    'message' => 'Layout has been updated successfully.',
                    "response" => '',
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (\Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function doctorSetting(Request $request, $id) {
        $rules = [
            'webhook_url' => 'nullable|url',
            'status' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $doctor = Doctor::where('user_id', $id)->first();
            $doctor->webhook_url = $request->webhook_url;
            $save = $doctor->save();
            $user = User::find($id);
            $user->status = $request->status;
            $user->save();
            if ($request->api_key) {
                $doctor->generateApiKey();
            }
            if ($save) {
                $data = array(
                    'status' => true,
                    'message' => 'Doctor setting updated successfully.',
                    "response" => $doctor,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Speciality Services
     * @param type $id
     * @return type
     */
    public function specialityServices($id) {
        try {
            $specialityServices = SpecialityService::with('doctorService:id,name')
                    ->where('speciality_id', $id)
                    ->select('id', 'doctor_service_id')  // Select necessary fields from SpecialityService
                    ->get()
                    ->map(function ($service) {
                return [
            'id' => $service->id,
            'name' => $service->doctorService->name, // Access the 'name' attribute of the related doctorService
                ];
            });

            if ($specialityServices) {
                $data = array(
                    'status' => true,
                    'message' => 'Speciality services fetch successfully.',
                    "response" => $specialityServices,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => [],
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }
/**
     * Doctor Education List
     * @param type $id
     * @return type
     */
    public function educationList($id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $educations = DoctorEducation::where('user_id', $user->id)->latest()->get(['id', 'degree',  'completed_in', 'university_name']);
                return response()->json([
                            'status' => true,
                            'response' => $educations,
                            'message' => "Doctor educations fetch successfully."
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Education store
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function educationStore(Request $request, $id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $validator = Validator::make($request->all(), [
                            'university_name' => 'required|max:255',
                            'degree' => 'required|max:255',
                            'completed_in' => 'required|numeric',
                                ], [
                            'completed_in.required' => 'The Year field is required',
                            'completed_in.numeric' => 'The Year field is numeric',
                            'degree.required' => 'The Degree field is required',
                            'degree.max' => 'The Degree field should not exceed to 255 characters',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                $education = new DoctorEducation;
                $education->user_id = $user->id;
                $education->degree = $request->degree;        
                $education->university_name = $request->university_name;
                $education->completed_in = $request->completed_in;
                $save = $education->save();
                if ($save) {
                     // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_education_add';
            $description = 'Dr. ' . $user?->name .' education added';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $education, $custom_properties, $log_name, $description);
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor Education added successfully.',
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function educationEdit($id) {
        try {
            $education = DoctorEducation::find($id);
            if ($education) {
                return response()->json([
                            'status' => true,
                            'response' => $education,
                            'message' => "Doctor educations fetch successfully."
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor educations not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Education update
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function educationUpdate(Request $request, $id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $validator = Validator::make($request->all(), [
                            'university_name' => 'required|max:255',
                            'degree' => 'required|max:255',
                            'completed_in' => 'required|numeric',
                                ], [
                            'completed_in.required' => 'The Year field is required',
                            'completed_in.numeric' => 'The Year field is numeric',
                            'degree.required' => 'The Degree field is required',
                            'degree.max' => 'The Degree field should not exceed to 255 characters',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                $education = DoctorEducation::find($request->education_id);
                $education->user_id = $user->id;
                $education->degree = $request->degree;
                $education->university_name = $request->university_name;
                $education->completed_in = $request->completed_in;
                $save = $education->save();
                if ($save) {
                              // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_education_modification';
            $description = 'Dr. ' . $user?->name .' education updated';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $education, $custom_properties, $log_name, $description);
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor Education updated successfully.',
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Education delete
     * @param type $id
     * @return type
     */
    public function educationDelete($id) {
        try {
            $education = DoctorEducation::find($id);
            if ($education->delete()) {
                return response()->json([
                            'status' => true,
                            'message' => 'Doctor Education deleted successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * Doctor Experiences list
     * @param type $id
     * @return type
     */
    public function experienceList($id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $experiences = DoctorExperience::where('user_id', $user->id)->latest()->get();
                return response()->json([
                            'status' => true,
                            'response' => $experiences,
                            'message' => 'Doctor Experiences fetch successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                            'response' => ''
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Experiences store
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function experienceStore(Request $request, $id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $validator = Validator::make($request->all(), [
                            'title_name' => 'required|max:255',
                            'organization_name' => 'required|max:255',
                            'start_date' => 'required|date',
                            'end_date' => 'nullable|required_if:is_currently_working,false|date|after:start_date',
                ],[
                    'title_name.required' =>"Certification title field is required",
                    'title_name.max' =>"Certification title field should be 255 charater",
                ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                $experience = new DoctorExperience;
                $experience->user_id = $user->id;
                $experience->title_name = $request->title_name;
                $experience->organization_name = $request->organization_name;
                $experience->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
                 if($request->is_currently_working != 'true'){
                    $experience->end_date = Carbon::parse($request->end_date)->format('Y-m-d');
                }
                $experience->is_currently_working = $request->is_currently_working == 'true' ? "true" : "false";
                $save = $experience->save();

                if ($save) {
                              // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'doctor';
            $operation = 'doctor_experience_add';
            $description = 'Dr. ' . $user?->name .' experience added';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $experience, $custom_properties, $log_name, $description);
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor Experience added successfully.',
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Experience edit
     * @param type $id
     * @return type
     */
    public function experienceEdit($id) {
        try {
            $experience = DoctorExperience::find($id);
            if ($experience) {
                return response()->json([
                            'status' => true,
                            'response' => $experience,
                            'message' => 'Doctor Experience fetch successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Experiences Update
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function experienceUpdate(Request $request, $id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $validator = Validator::make($request->all(), [
                            'title_name' => 'required|max:255',
                            'organization_name' => 'required|max:255',
                            'start_date' => 'required|date',
                            'end_date' => 'nullable|required_if:is_currently_working,false|date|after:start_date',
                ],[
                    'title_name.required' =>"Certification title field is required",
                    'title_name.max' =>"Certification title field should be 255 charater",
                ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                $experience = DoctorExperience::find($request->id);
                $experience->user_id = $user->id;
                $experience->title_name = $request->title_name;
                $experience->organization_name = $request->organization_name;
                $experience->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
                if($request->is_currently_working != 'true'){
                    $experience->end_date = Carbon::parse($request->end_date)->format('Y-m-d');
                }else{
                    $experience->end_date = NULL;
                }
                $experience->is_currently_working = $request->is_currently_working == 'true' ? "true" : "false";
                $save = $experience->save();

                if ($save) {
                // Activity logs
                $causer = auth()->user();
                $roleName = $causer->roles()->pluck('name')->first();
                $log_name = 'doctor';
                $operation = 'doctor_experience_modification';
                $description = 'Dr. ' . $user?->name .' experience updated';
                $custom_properties = ['application' => config('app.name'),
                    'operation' => $operation,
                    'causer_name' => $causer->name,
                    'role_name' =>$roleName
                ];
                store_activity_log($causer, $experience, $custom_properties, $log_name, $description);
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor Experience updated successfully.',
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Doctor Experience delete
     * @param type $id
     * @return type
     */
    public function experienceDelete($id) {
        try {
            $experience = DoctorExperience::find($id);
            if ($experience->delete()) {
                return response()->json([
                            'status' => true,
                            'message' => 'Doctor Experience deleted successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * Patient Appointment
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function doctorAppointment(Request $request, $id) {
        try {
       $appointments = Appointment::with('doctor.doctor', 'patient.patient', 'bill')->where('meeting_at', '>=', now()->format('Y-m-d H:i:s'))->where('doctor_id', $id)->latest()->paginate(10); 
       return response()->json([
                            'status' => true,
                            'response' =>$appointments,
                            'message' => 'Doctor appointments fetched successfully.',
                                ], 200);
       
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * Clinic List
     * @param type $id
     * @return type
     */
    public function clinicList($id) {
           try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $doctorClinics = DoctorClinic::where('user_id', $user->id)->latest()->get();
                return response()->json([
                            'status' => true,
                            'response' => $doctorClinics,
                            'message' => 'Doctor Clinic fetch successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                            'response' => ''
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * Clinic Store
     * @param Request $request
     * @param type $id
     * @return type
     */
     public function clinicStore(Request $request, $id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $validator = Validator::make($request->all(), [
                                'clinic_name'    => 'required|max:255',
                                'clinic_timings' => 'required|max:255',
                                'clinic_address' => 'required|max:255',
                                'clinic_tagline' => 'nullable|max:255',
                                'clinic_website' => 'nullable|max:255',
                                'clinic_logo'    => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:1024',
                                'clinic_phone'   => 'required|numeric|digits:10',
                                'clinic_email'   => 'nullable|email|email:rfc,dns',
                             ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                if ($request->hasFile('clinic_logo')) {         
                    $statement = DB::select("SHOW TABLE STATUS LIKE 'doctor_clinic'");
                    $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();
                    $file_name = time() . 'clinic_logo' . $nextId;
                    $fileName = $file_name . '.' . $request->file('clinic_logo')->getClientOriginalExtension();
                    $clinicLogo = 'doctor' . '/' . $request->file('clinic_logo')->storeAs($nextId, $fileName, 'doctor');
                } else {
                    $clinicLogo = NULL;
                }
                $doctorClinic = new DoctorClinic;
                $doctorClinic->user_id        = $user->id;
                $doctorClinic->clinic_name    = $request->clinic_name;
                $doctorClinic->clinic_timings = $request->clinic_timings;
                $doctorClinic->clinic_address = $request->clinic_address;
                $doctorClinic->clinic_tagline = $request->clinic_tagline;
                $doctorClinic->clinic_website = $request->clinic_website;
                $doctorClinic->clinic_logo    = $clinicLogo;
                $doctorClinic->clinic_phone   = $request->clinic_phone;
                $doctorClinic->clinic_email   = $request->clinic_email;
                $save = $doctorClinic->save();

                if ($save) {
                 // Activity logs
                $causer = auth()->user();
                $roleName = $causer->roles()->pluck('name')->first();
                $log_name = 'doctor';
                $operation = 'doctor_clinic_add';
                $description = 'Dr. ' . $user?->name .' clinic added';
                $custom_properties = ['application' => config('app.name'),
                    'operation' => $operation,
                    'causer_name' => $causer->name,
                    'role_name' =>$roleName
                ];
                store_activity_log($causer, $doctorClinic, $custom_properties, $log_name, $description);
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor Clinic added successfully.',
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
      /**
     * Doctor Clinic edit
     * @param type $id
     * @return type
     */
    public function clinicEdit($id) {
        try {
            $doctorClinic = DoctorClinic::find($id);
            if ($doctorClinic) {
                return response()->json([
                            'status' => true,
                            'response' => $doctorClinic,
                            'message' => 'Doctor Clinic fetch successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * doctor clinic update
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function clinicUpdate(Request $request, $id) {
        try {
            
                $validator = Validator::make($request->all(), [
                            'clinic_name'    => 'required|max:255',
                            'clinic_timings' => 'required|max:255',
                            'clinic_address' => 'required|max:255',
                            'clinic_tagline' => 'nullable|max:255',
                            'clinic_website' => 'nullable|max:255',
                            'clinic_logo'    => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:1024',
                            'clinic_phone'   => 'required|numeric|digits:10',
                            'clinic_email'   => 'nullable|email|email:rfc,dns',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                $doctorClinic = DoctorClinic::find($id);
                if ($request->hasFile('clinic_logo')) {
                $nextId = $id;
                $file_name = time() . 'clinic_logo_' . $id;
                $fileName = $file_name . '.' . $request->file('clinic_logo')->getClientOriginalExtension();
                $clinicLogo = 'doctor' . '/' . $request->file('clinic_logo')->storeAs($nextId, $fileName, 'doctor');
                if (File::exists($doctorClinic->clinic_logo)) {
                    unlink($doctorClinic->clinic_logo);
                }
                $doctorClinic->clinic_logo = $clinicLogo;
                }
                $doctorClinic->clinic_name = $request->clinic_name;
                $doctorClinic->clinic_timings = $request->clinic_timings;
                $doctorClinic->clinic_address = $request->clinic_address;
                $doctorClinic->clinic_tagline = $request->clinic_tagline;
                $doctorClinic->clinic_website = $request->clinic_website;
                $doctorClinic->clinic_phone   = $request->clinic_phone;
                $doctorClinic->clinic_email   = $request->clinic_email;

                if ($doctorClinic->save()) {
                    $user = User::find($doctorClinic->user_id);
                    // Activity logs
                    $causer = auth()->user();
                    $roleName = $causer->roles()->pluck('name')->first();
                    $log_name = 'doctor';
                    $operation = 'doctor_clinic_modification';
                    $description = 'Dr. ' . $user?->name .' clinic updated';
                    $custom_properties = ['application' => config('app.name'),
                        'operation' => $operation,
                        'causer_name' => $causer->name,
                        'role_name' =>$roleName
                    ];
                    store_activity_log($causer, $doctorClinic, $custom_properties, $log_name, $description);
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor Clinic updated successfully.',
                                    ], 200);
                }
            
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
     /**
     * Doctor Clinic delete
     * @param type $id
     * @return type
     */
    public function clinicDelete($id) {
        try {
            $doctorClinic = DoctorClinic::find($id);
            if ($doctorClinic->delete()) {
                return response()->json([
                            'status' => true,
                            'message' => 'Doctor Clinic deleted successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * hospital List
     * @param type $id
     * @return type
     */
    public function hospitalList($id) {
           try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $doctorHospital = DoctorHospital::where('user_id', $user->id)->latest()->get();
                return response()->json([
                            'status' => true,
                            'response' => $doctorHospital,
                            'message' => 'Doctor hospital fetch successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                            'response' => ''
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * hospital Store
     * @param Request $request
     * @param type $id
     * @return type
     */
     public function hospitalStore(Request $request, $id) {
        try {
            $user = User::find($id);
            if ($user != null && $user->hasRole('Doctor')) {
                $validator = Validator::make($request->all(), [
                                'hospital_name'    => 'required|max:255',
                                'hospital_timings' => 'required|max:255',
                                'hospital_address' => 'required|max:255',
                                'hospital_tagline' => 'nullable|max:255',
                                'hospital_website' => 'nullable|max:255',
                                'hospital_logo'    => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:1024',
                                'hospital_phone'   => 'required|numeric|digits:10',
                                'hospital_email'   => 'nullable|email|email:rfc,dns',
                             ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                if ($request->hasFile('hospital_logo')) {         
                    $statement = DB::select("SHOW TABLE STATUS LIKE 'doctor_hospital'");
                    $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();
                    $file_name = time() . 'hospital_logo' . $nextId;
                    $fileName = $file_name . '.' . $request->file('hospital_logo')->getClientOriginalExtension();
                    $hospitalLogo = 'doctor' . '/' . $request->file('hospital_logo')->storeAs($nextId, $fileName, 'doctor');
                } else {
                    $hospitalLogo = NULL;
                }
                $doctorHospital = new DoctorHospital;
                $doctorHospital->user_id        = $user->id;
                $doctorHospital->hospital_name    = $request->hospital_name;
                $doctorHospital->hospital_timings = $request->hospital_timings;
                $doctorHospital->hospital_address = $request->hospital_address;
                $doctorHospital->hospital_tagline = $request->hospital_tagline;
                $doctorHospital->hospital_website = $request->hospital_website;
                $doctorHospital->hospital_logo    = $hospitalLogo;
                $doctorHospital->hospital_phone   = $request->hospital_phone;
                $doctorHospital->hospital_email   = $request->hospital_email;
                $save = $doctorHospital->save();

                if ($save) {
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor hospital added successfully.',
                                    ], 200);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Doctor not found in records.',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
      /**
     * Doctor hospital edit
     * @param type $id
     * @return type
     */
    public function hospitalEdit($id) {
        try {
            $doctorHospital = DoctorHospital::find($id);
            if ($doctorHospital) {
                return response()->json([
                            'status' => true,
                            'response' => $doctorHospital,
                            'message' => 'Doctor hospital fetch successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    /**
     * doctor hospital update
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function hospitalUpdate(Request $request, $id) {
        try {
            
                $validator = Validator::make($request->all(), [
                             'hospital_name'    => 'required|max:255',
                                'hospital_timings' => 'required|max:255',
                                'hospital_address' => 'required|max:255',
                                'hospital_tagline' => 'nullable|max:255',
                                'hospital_website' => 'nullable|max:255',
                                'hospital_logo'    => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:1024',
                                'hospital_phone'   => 'required|numeric|digits:10',
                                'hospital_email'   => 'nullable|email|email:rfc,dns',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                                'status' => false,
                                'message' => 'Validation error',
                                'errors' => $validator->errors(),
                                    ], 422);
                }
                $doctorHospital = DoctorHospital::find($id);
                if ($request->hasFile('hospital_logo')) {
                $nextId = $id;
                $file_name = time() . 'hospital_logo_' . $id;
                $fileName = $file_name . '.' . $request->file('hospital_logo')->getClientOriginalExtension();
                $hospitalLogo = 'doctor' . '/' . $request->file('hospital_logo')->storeAs($nextId, $fileName, 'doctor');
                if (File::exists($doctorHospital->hospital_logo)) {
                    unlink($doctorHospital->hospital_logo);
                }
                $doctorHospital->hospital_logo = $hospitalLogo;
                }
                $doctorHospital->hospital_name    = $request->hospital_name;
                $doctorHospital->hospital_timings = $request->hospital_timings;
                $doctorHospital->hospital_address = $request->hospital_address;
                $doctorHospital->hospital_tagline = $request->hospital_tagline;
                $doctorHospital->hospital_website = $request->hospital_website;
                $doctorHospital->hospital_phone   = $request->hospital_phone;
                $doctorHospital->hospital_email   = $request->hospital_email;

                if ($doctorHospital->save()) {
                    return response()->json([
                                'status' => true,
                                'message' => 'Doctor hospital updated successfully.',
                                    ], 200);
                }
            
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
     /**
     * Doctor Clinic delete
     * @param type $id
     * @return type
     */
    public function hospitalDelete($id) {
        try {
            $doctorHospital = DoctorHospital::find($id);
            if ($doctorHospital->delete()) {
                return response()->json([
                            'status' => true,
                            'message' => 'Doctor hospital deleted successfully.',
                                ], 200);
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

}
