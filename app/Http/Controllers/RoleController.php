<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('permission:role_list', ['only' => ['roleListing']]);
        $this->middleware('permission:role_create', ['only' => ['index']]);
        $this->middleware('permission:role_edit', ['only' => ['updateRole', 'editRole']]);
        $this->middleware('permission:role_delete', ['only' => ['deleteRole']]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $rules = [
            'name' => 'required|regex: /^[a-zA-Z ]{2,40}$/|max:255|unique:roles',
        ];

        $messages = [
            'name.required' => 'The Role field is required.',
            'name.regex' => 'The Role format is invalid.',
            'name.max' => 'The Role format is invalid.The name must not be greater than 255 characters.',
            'name.unique' => 'The Role has already created.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        try {
            $role = new Role();
            $role->name = $request->input('name');
            $role->guard_name ="web";
            if ($role->save()) {
                $permissions = explode(',', $request->input('permissions'));
                $role->syncPermissions($permissions);
                $data = array(
                    'status' => true,
                    'message' => 'Role stored successfully.',
                    "response" => $role,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }
    /**
     * Role listing
     * @param Request $request
     * @return type
     */
    public function roleListing(Request $request) {
       $length = isset($request->length)?$request->length:10;
       $query = Role::query();
            if (isset($request->search)) {
                $query->where(function ($query1) use ($request) {
                        $query1->where('name', 'like', '%' . $request->search . '%');
                         });
            }
            if ($request->has(['field', 'sortOrder']) && $request->field != null) {
                 $query->orderBy(request('field'), request('sortOrder'));
            }else{
                 $query->orderBy('created_at', 'DESC');
            }
            $roles = $query->paginate($length); 
        return $roles;
    }
    /**
     * Role delete
     * @param type $id
     * @return type
     */
    public function deleteRole($id) {
        try{
        $role = Role::find($id);
         if (!empty($role)) {
        if ($role->users()->count()) {
            return response()->json(array(
                    'status' => false,
                    'message' => 'Role cannot be deleted, this role is attached to user(s) available in system!.',
             ), 422);        
            } else {
              foreach ($role->getAllPermissions() as $permission) {
                $role->revokePermissionTo($permission);
                $permission->removeRole($role);
              }  

                if ($role->delete()) {
                    return response()->json(array(
                        'status' => true,
                        'message' => 'Role deleted successfully.',
                     ), 200);
                } else {
                   return response()->json(array(
                        'status' => false,
                        'message' => 'Something went wrong, Please try again later!',

                    ), 422);
                }  
            }
            } else {
                return response()->json(array(
                        'status' => false,
                        'message' => 'Role doesn\'t exists!',

                    ), 422);
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }
    /**
     * Edit role
     * @param type $id
     * @return type
     */
    public function editRole($id) {
        return Role::with('permissions')->where('id', $id)->first();
    }
    /**
     * update Role
     * @param Request $request
     * @return type
     */
    public function updateRole(Request $request) {
        $rules = [
            'name' => 'required|regex: /^[a-zA-Z ]{2,40}$/|max:255|' . Rule::unique('roles')->ignore($request->input('id')),
        ];
        $messages = [
            'name.required' => 'The Role field is required.',
            'name.regex' => 'The Role format is invalid.',
            'name.max' => 'The Role format is invalid.The name must not be greater than 255 characters.',
            'name.regex' => 'The Role has already created.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }

        try {
            $role = Role::find($request->input('id'));
            $role->name = $request->input('name');
            if ($role->save()) {
                $permissions = explode(',', $request->input('permissions'));
                $role->syncPermissions($permissions);
                $data = array(
                    'status' => true,
                    'message' => 'Role Updated successfully.',
                    "response" => $role,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * get all roles
     * 
     * @return type
     */
    public function getRoles() {
        $roles = Role::all();
        return response($roles);
    }

    /**
     * get all permissions
     * 
     * @return type
     */
    public function getPermission() {
        $permissions = Permission::all();
        $data = array();
        if (count($permissions)) {
            foreach ($permissions as $key => $value) {
                $data[$key] = ['name' => $value->name, 'id' => $value->id];
            }
        }
        return response($data);
    }
    /**
     * get User Permission
     * @param Request $request
     * @return type
     */
    public function getUserPermission(Request $request){
       $permissions = $request->user()->getAllPermissions();
       if(count($permissions)){
       $permissions = array_column($permissions->toArray(), 'name'); 
       }
       return response($permissions);
    }

}
