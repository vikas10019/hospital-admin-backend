<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ActivityLogController extends Controller
{
    public function index(Request $request) {
        try {
      $length = $request->length ?? 10; 
      $query = Activity::query()->with('causer','subject'); // Load the causer relationship 
      if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('log_name', 'like', '%' . $request->search . '%')
                        ->orWhereHas('causer', function ($query3) use ($request) {
                            $query3->where('name', 'like', '%' . $request->search . '%');
                        })
                        ->orWhere('properties->operation', 'like', '%' . $request->search . '%'); 
//                        ->orWhere('properties->causer_name', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->latest();
        }
      $activities = $query->paginate($length);
         return response()->json(array(
                        'status' => true,
                        'message' => 'Activity log data fetch successfully.',
                        'response' => $activities
                            ), 200);
       } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        
    }
    
}
