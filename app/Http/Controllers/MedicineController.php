<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medicine;
use App\Models\DrugType;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class MedicineController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Medicine::query()->with('drugType');
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('quantity', 'like', '%' . $request->search . '%')
                        ->orWhereHas('drugType', function ($query3) use ($request) {
                            $query3->where('name', 'like', '%' . $request->search . '%');
                        });
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        return $query->paginate($length);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return Medicine::with('drugType')->find($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|unique:medicine,name',
                        'drug_type_id' => 'required|numeric',
                        'quantity' => 'required|numeric',
                        'unit' => 'required',
                        'description' => 'nullable|string|max:255',
            ]);
            if ($validator->fails()) {
                return response()->json(array(
                            'success' => false,
                            'message' => 'There are incorect values in the form!',
                            'errors' => $validator->getMessageBag()->toArray()
                                ), 422);
            }
            $medicine = Medicine::create([
                        'name' => $request->name,
                        'drug_type_id' => $request->drug_type_id,
                        'quantity' => $request->quantity,
                        'unit' => $request->unit,
                        'description' => $request->description,
            ]);
            if ($medicine) {
                return response()->json(array(
                            'status' => true,
                            'message' => 'Medicine Stored successfully.',
                            'response' => $medicine,
                                ), 200);
            } else {
                return response()->json(array(
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                            "response" => '',
                                ), 400);
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $medicines = Medicine::where('drug_type_id', $id)->get();
        $drug = array();
        foreach ($medicines as $key => $medicine) {
            $drug[$key]['id'] = $medicine->id;
            $drug[$key]['value'] = $medicine->name;
            $drug[$key]['label'] = ucwords($medicine->name);
        }
        return response()->json($drug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function quantityUnit($id) {
        $drugs = array();
        $medicine = Medicine::find($id);
        $drugs['quantity'] = $medicine->quantity;
        $drugs['unit'] = $medicine->unit;
        return response()->json($drugs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|unique:medicine,name,' . $id,
                        'drug_type_id' => 'required|numeric',
                        'quantity' => 'required|numeric',
                        'unit' => 'required',
                        'description' => 'nullable|string|max:255',
            ]);
            if ($validator->fails()) {
                return response()->json(array(
                            'success' => false,
                            'message' => 'There are incorect values in the form!',
                            'errors' => $validator->getMessageBag()->toArray()
                                ), 422);
            }
            $medicine = Medicine::find($id);
            $medicine->name = $request->name;
            $medicine->drug_type_id = $request->drug_type_id;
            $medicine->quantity = $request->quantity;
            $medicine->unit = $request->unit;
            $medicine->description = $request->description;
            if ($medicine->save()) {
                return response()->json(array(
                            'status' => true,
                            'message' => 'Medicine Updated successfully.',
                            'response' => $medicine,
                                ), 200);
            } else {
                return response()->json(array(
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                            "response" => '',
                                ), 400);
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $medicine = Medicine::doesnthave('prescibeMedicine')->where('id', $id)->first();
            if (!empty($medicine)) {
                $medicine->delete();
                return response()->json(array(
                            'status' => true,
                            'message' => 'Medicine deleted successfully.',
                            'response' => $medicine,
                                ), 200);
            } else {
                return response()->json(array(
                            'status' => false,
                            'message' => 'You can\'t delete this medicine,it attached!',
                            "response" => '',
                                ), 400);
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
    }

    /**
     * common medicine list
     * @return type
     */
    public function medicineList() {
        return Medicine::with('drugType')->get();
    }

}
