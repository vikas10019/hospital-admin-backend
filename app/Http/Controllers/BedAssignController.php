<?php

namespace App\Http\Controllers;

use App\Models\BedAssign;
use Illuminate\Support\Facades\Validator;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BedAssignController extends Controller {

    /**
     * New BedAssign create
     * 
     * @param Request $request
     * @return type
     */
    public function addBedAssign(Request $request) {
        $rules1 = [
            'mobile' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'assign_date' => 'required|date|date_format:Y/m/d',
            'discharge_date' => 'required|date|date_format:Y/m/d',
            'description' => 'nullable|max:255'
        ];
        $validator1 = Validator::make($request->all(), $rules1);
        if ($validator1->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator1->getMessageBag()->toArray()
                            ), 423);
        }
        try {
            $record = BedAssign::latest()->first();
            if ($record == null) {
                $serialNum = 'BA-001';
            } else {
                $serialNum = $record->serial;
            }
            $expNum = explode('-', $serialNum);
            $numbers = str_pad($expNum[1] + 1, 4, '0', STR_PAD_LEFT);
            $serial = $expNum[0] . '-' . $numbers;
            $bedassign = new BedAssign();
            $bedassign->serial = $serial;
            $bedassign->patient_id = $request->patient_id;
            $bedassign->mobile = $request->input('mobile');
            $bedassign->bed_id = $request->input('bed_id');
            $bedassign->description = $request->input('description');
            $bedassign->assign_date = $request->input('assign_date');
            $bedassign->discharge_date = $request->input('discharge_date');
            $bedassign->status = $request->input('status');
            $bedassign->save();
            $data = [
                'status' => 'success',
                'response' => $bedassign,
            ];
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
        return response($data);
    }

    /**
     * Listing all BedAssign
     * 
     * @param type $id
     * @return type
     */
    function BedAssignListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = BedAssign::query()->with('bed', 'patient.user');
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('mobile', 'like', '%' . $request->search . '%')
                        ->orWhere('serial', 'like', '%' . $request->search . '%')
                        ->orWhereHas('bed', function ($query3) use ($request) {
                            $query3->where('type', 'like', '%' . $request->search . '%');
                        })
                        ->orWhereHas('patient.user', function ($query3) use ($request) {
                            $query3->where('name', 'like', '%' . $request->search . '%');
                        });
            });
        }
        if ($request->assign_date != '') {
            $assign_date = explode('-', $request->assign_date);
            $start = Carbon::createFromFormat('d/m/Y', trim($assign_date[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($assign_date[1]))->format('Y-m-d');
            $query->whereDate('assign_date', '>=', $start)->whereDate('assign_date', '<=', $end);
        }
        if ($request->discharge_date != '') {
            $discharge_date = explode('-', $request->discharge_date);
            $start1 = Carbon::createFromFormat('d/m/Y', trim($discharge_date[0]))->format('Y-m-d');
            $end1 = Carbon::createFromFormat('d/m/Y', trim($discharge_date[1]))->format('Y-m-d');
            $query->whereDate('discharge_date', '>=', $start1)->whereDate('discharge_date', '<=', $end1);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') != 'bed.type' && request('field') != 'patient.user.name') {
                $query->orderBy(request('field'), request('sortOrder'));
            }
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') == 'bed.type' || request('field') == 'patient.user.name') {
                if (strtoupper(request('sortOrder')) == "DESC") {
                    $bedAssign = $query->get()->sortByDesc(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                } else {
                    $bedAssign = $query->get()->sortBy(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                }
                // Convert JSON data to associative array
                $arrayData = json_decode($bedAssign, true);

                // Convert the associative array into the desired format
                $formattedArray = array_values($arrayData);

                // Output the formatted array as JSON
                $bedAssign1 = json_encode($formattedArray, JSON_PRETTY_PRINT);

                $bedAssigns = paginate(json_decode($bedAssign1, true), $length, null, ['path' => $request->url(), 'query' => $request->query()]);
            }
        } else {
            $bedAssigns = $query->paginate($length);
        }
        if (isset($bedAssigns)) {
            return $bedAssigns;
        } else {
            return $bedAssigns = $query->paginate($length);
        }
    }

    /**
     * BedAssign delete by id
     * 
     * @param type $id
     * @return type
     */
    function BedAssignDelete($id) {
        $result = BedAssign::where('id', $id)->delete();
        if ($result) {
            return ['result' => 'BedAssign has been deleted'];
        }
    }

    /**
     * BedAssign detail by id
     * 
     * @param type $id
     * @return type
     */
    public function BedAssignEdit($id) {
        return BedAssign::find($id);
    }

    /**
     * BedAssign detail update by id 
     * 
     * @param Request $request
     * @return type
     */
    public function BedAssignUpdate(Request $request, $id) {
        $rules1 = [
            'mobile' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'assign_date' => 'required|date|date_format:Y/m/d',
            'discharge_date' => 'required|date|date_format:Y/m/d',
            'description' => 'nullable|max:255'
        ];
        $validator1 = Validator::make($request->all(), $rules1);
        if ($validator1->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator1->getMessageBag()->toArray()
                            ), 423);
        }
        try {
            $bedassign = BedAssign::find($id);
            $bedassign->patient_id = $request->input('patient_id');
            $bedassign->mobile = $request->input('mobile');
            $bedassign->bed_id = $request->input('bed_id');
            $bedassign->description = $request->input('description');
            $bedassign->assign_date = $request->input('assign_date');
            $bedassign->discharge_date = $request->input('discharge_date');
//            $bedassign->assign_by = $request->input('assign_by');
            $bedassign->status = $request->input('status');
            $bedassign->save();
            $data = [
                'status' => 'success',
                'response' => $bedassign,
            ];
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
        return response($data);
    }

    public function assignJoin() {
        $bedassign = DB::table('bedassign')
                ->join('patient', 'patient.id', '=', 'bedassign.patient_id')
                ->join('bed', 'bed.id', '=', 'bedassign.bed_id')
                ->get();
        return $bedassign;
    }

}
