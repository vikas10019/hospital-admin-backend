<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\hasfile;
class SettingController extends Controller
{
     public function __construct()
    {
        $this->middleware('permission:setting_list',['only' => ['index','store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::first();
        return response()->json(array(
                        'status' => true,
                        'message' => 'Setting fetch successfully.',
                        "response" => $setting,
                            ), 200);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'timings' => 'required'
        ];
        if(!isset($request->id)){
           $rules['bill_logo'] = 'required|mimes:jpg,png,jpeg,gif,svg,webp|max:1024';
           $rules['prescription_logo'] = 'required|mimes:jpg,png,jpeg,gif,svg,webp|max:1024';
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
        if(isset($request->id)){
           $setting = Setting::find($request->id); 
        }else{
           $setting = new Setting();  
        }   
        $setting->name = $request->name;
        $setting->email = $request->email;
        $setting->mobile = $request->mobile;
        $setting->timings = $request->timings;
        $setting->notes = $request->notes;
        $setting->address = $request->address;
        if (isset($request->bill_logo)) {
                $path = storage_path('app/public/setting/bill');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                if ($request->hasfile('bill_logo')) {
                    $file = $request->file('bill_logo');
                    $fileName = time() . '.' . $request->bill_logo->extension();
                    $request->bill_logo->move($path, $fileName);
                    $setting->bill_logo = '/setting/bill/'. $fileName;
                }
        }
        if (isset($request->prescription_logo)) {
                $path1 = storage_path('app/public/setting/prescription');
                if (!File::isDirectory($path1)) {
                    File::makeDirectory($path1, 0777, true, true);
                }
                if ($request->hasfile('prescription_logo')) {
                    $file1 = $request->file('prescription_logo');
                    $fileName1 = time() . '.' . $request->prescription_logo->extension();
                    $request->prescription_logo->move($path1, $fileName1);
                    $setting->prescription_logo = '/setting/prescription/'. $fileName1;
                }
        }        
        if ($setting->save()) {
               return response()->json(array(
                    'status' => true,
                    'message' => 'Setting updated successfully.',
                    "response" => null,
                ), 200);
            } else {
                return response()->json(array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                ), 200);
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }

}
