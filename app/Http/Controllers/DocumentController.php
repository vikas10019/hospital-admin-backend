<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Doctor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;

class DocumentController extends Controller {

    function __construct() {
        $this->middleware('permission:appointment_list', ['only' => ['AppointmentListing']]);
        $this->middleware('permission:appointment_create', ['only' => ['addAppointment']]);
        $this->middleware('permission:appointment_edit', ['only' => ['editAppointment', 'updateAppointment']]);
        $this->middleware('permission:appointment_delete', ['only' => ['deleteAppointment']]);
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function addDocument(Request $request) {

        $rules = [
            'doctor_id' => 'required',
            'description' => 'required|max:255',
            'patient_document' => 'required|mimes:jpeg,png,jpg,bmp,gif,doc,docx,odt,pdf,zip|max:5120',
        ];
        $message = [
            'doctor_id.required' => "The Doctor field is required.",
            'description.required' => "The Description field is required.",
            'description.max' => "The description must not be greater than 255 characters.",
            'patient_document.required' => "The File field is required.",
            'patient_document.mimes' => "The File must be a file of type: jpeg, png, jpg, bmp, gif, doc, docx, odt, pdf, zip.",
            'patient_document.max' => "File exceeds maximum allowed size of 5MB",
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorrect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 423);
        }
        try {
            $path = "";
            if ($request->hasFile('patient_document')) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'documents'");
                $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : uniqid();
                $file_name = time() . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('patient_document')->getClientOriginalExtension();
                $path = 'document' . '/' . $request->file('patient_document')->storeAs($nextId, $fileName, 'document');
            }
            $document = new Document();
            $document->patient_id = $request->input('patient_id');
            $document->doctor_id = $request->input('doctor_id');
            $document->description = $request->input('description');
            $document->patient_document = $path;
            if ($document->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Document stored successfully.',
                    "response" => $document,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }

        return response()->json($data);
    }

    /**
     * get document list
     * 
     * @return type
     */
    public function ListingDocument(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Document::query()->with('patient.patient', 'doctor.doctor.specialization');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('doctor_id', auth()->user()->id);
        }
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->orWhereHas('patient.patient', function ($query2) use ($request) {
                            $query2->where('patient_id', 'like', '%' . $request->search . '%');
                        })
                        ->orWhereHas('patient', function ($query3) use ($request) {
                            $query3->where('name', 'like', '%' . $request->search . '%');
                        })
                        ->orWhereHas('doctor', function ($query4) use ($request) {
                            $query4->where('name', 'like', '%' . $request->search . '%');
                        });
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') != 'doctor.name' && request('field') != 'patient.name') {
                $query->orderBy(request('field'), request('sortOrder'));
            }
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') == 'doctor.name' || request('field') == 'patient.name') {
                if (strtoupper(request('sortOrder')) == "DESC") {
                    $document = $query->get()->sortByDesc(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                } else {
                    $document = $query->get()->sortBy(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                }
                // Convert JSON data to associative array
                $arrayData = json_decode($document, true);

                // Convert the associative array into the desired format
                $formattedArray = array_values($arrayData);

                // Output the formatted array as JSON
                $documents1 = json_encode($formattedArray, JSON_PRETTY_PRINT);
                $documents = paginate(json_decode($documents1), $length, null, ['path' => $request->url(), 'query' => $request->query()]);
            }
        } else {
            $documents = $query->paginate($length);
        }
        if (isset($documents)) {
            return $documents;
        } else {
            return $documents = $query->paginate($length);
        }
    }

    /**
     * delete document
     * 
     * @param type $id
     * @return type
     */
    public function deleteDocument($id) {
        try {
            $result = Document::where('id', $id)->delete();
            if ($result) {
                $data = array(
                    'status' => true,
                    'message' => 'Document deleted successfully.',
                    "response" => null,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * edit document
     * 
     * @param type $id
     * @return type
     */
    public function EditDocument($id) {
        return Document::with('patient.patient')->where('id', $id)->first();
    }

    /**
     * upload document
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateDocument(Request $request) {
        $rules = [
            'doctor_id' => 'required',
            'description' => 'required|max:255',
            'patient_document' => 'nullable|mimes:jpeg,png,jpg,bmp,gif,doc,docx,odt,pdf,zip|max:5120',
        ];
        $message = [
            'doctor_id.required' => "The Doctor field is required.",
            'description.required' => "The Description field is required.",
            'description.max' => "The description must not be greater than 255 characters.",
            'patient_document.mimes' => "The File must be a file of type: jpeg, png, jpg, bmp, gif, doc, docx, odt, pdf, zip.",
            'patient_document.max' => "File exceeds maximum allowed size of 5MB",
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorrect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 423);
        }
        try {
            $document = Document::find($request->input('id'));
            $document->doctor_id = $request->input('doctor_id');
            $document->description = $request->input('description');
            if ($request->hasFile('patient_document')) {
                $nextId = $request->input('id');
                $file_name = time() . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('patient_document')->getClientOriginalExtension();
                $path = 'document' . '/' . $request->file('patient_document')->storeAs($nextId, $fileName, 'document');
                $oldDoc = public_path() . '/storage/' . $document->patient_document;
                if (File::exists($oldDoc)) {
                    File::delete($oldDoc);
                }
                $document->patient_document = $path;
            }
            if ($document->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Document updated successfully.',
                    "response" => $document,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response()->json($document);
    }

}
