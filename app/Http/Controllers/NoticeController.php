<?php

namespace App\Http\Controllers;

use App\Models\Notice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class NoticeController extends Controller {

    /**
     * New Notice create
     * 
     * @param Request $request
     * @return type
     */
    public function addNotice(Request $request) {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'start_date' => 'required|date|date_format:Y/m/d',
            'end_date' => 'required|date|after_or_equal:start_date|date_format:Y/m/d',
            'status' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $notice = new Notice();
            $notice->title = $request->input('title');
            $notice->description = $request->input('description');
            $notice->start_date = $request->input('start_date');
            $notice->end_date = $request->input('end_date');
            $notice->status = $request->input('status');
            $notice->save();
            $data = [
                'status' => 'success',
                'response' => $notice,
            ];
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
        return response($data);
    }

    /**
     * Listing all Notice
     * 
     * @param type $id
     * @return type
     */
    function NoticeListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Notice::query();
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('title', 'like', '%' . $request->search . '%')
                        ->orWhere('description', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        $notice = $query->paginate($length);
        return $notice;
    }

    /**
     * Notice delete by id
     * 
     * @param type $id
     * @return type
     */
    function NoticeDelete($id) {
        try {
            $saved = Notice::where('id', $id)->delete();
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 422);
        }
        if ($saved) {
            $data = [
                'status' => 'success',
                'message' => 'Staff deleted successfully.'
            ];
        } else {
            $data = [
                'status' => 'error',
                'message' => 'Some thing went wrong, please try again.'
            ];
        }
        return response($data);
    }

    /**
     * Notice detail by id
     * 
     * @param type $id
     * @return type
     */
    public function NoticeEdit($id) {
        return Notice::find($id);
    }

    /**
     * Notice detail update by id 
     * 
     * @param Request $request
     * @return type
     */
    public function NoticeUpdate(Request $request, $id) {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'start_date' => 'required|date|date_format:Y/m/d',
            'end_date' => 'required|date|after_or_equal:start_date|date_format:Y/m/d',
            'status' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $notice = Notice::find($id);
            $notice->title = $request->input('title');
            $notice->description = $request->input('description');
            $notice->start_date = $request->input('start_date');
            $notice->end_date = $request->input('end_date');
            $notice->status = $request->input('status');
            $notice->save();
            $data = [
                'status' => 'success',
                'response' => $notice,
            ];
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
    }

}
