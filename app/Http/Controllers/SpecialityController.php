<?php

namespace App\Http\Controllers;

use App\Models\Speciality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SpecialityController extends Controller {

    function __construct() {
        $this->middleware('permission:speciality_list', ['only' => ['listingSpeciality']]);
        $this->middleware('permission:speciality_create', ['only' => ['addschedule']]);
        $this->middleware('permission:speciality_edit', ['only' => ['editSpeciality', 'updateSpeciality']]);
        $this->middleware('permission:speciality_delete', ['only' => ['deleteSpeciality']]);
    }

    /**
     * add Speciality
     * @param Request $request
     * @return type
     */
    public function addSpeciality(Request $request) {
        $rules = [
            'name' => 'required|max:199|unique:specialities,name',
            'description' => 'nullable|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        try {
            $speciality = new Speciality();
            $speciality->name = $request->input('name');
            $speciality->description = $request->input('description');
            $speciality->status = $request->input('status');

            if ($speciality->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'The Speciality created successfully.',
                    "response" => $speciality,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * listing Speciality
     * @param Request $request
     * @return type
     */
    public function listingSpeciality(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Speciality::query();
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        $speciality = $query->paginate($length);

        return $speciality;
    }

    /**
     * delete Speciality
     * @param type $id
     * @return type
     */
    function deleteSpeciality($id) {
        $result = Speciality::with('doctor')->find($id);
        if($result){
            if(!$result->doctor->isEmpty()){
                return response()->json(array(
                                'status' => false,
                                'message' => 'The Speciality has attached and can not be deleted.',
                                "response" => '',
                                    ), 400);
            }else{
                $result->delete();
                return response()->json(array(
                                'status' => true,
                                'message' => 'The Speciality has been deleted.',
                                "response" => '',
                                    ), 200);
            }
        }else{
            return response()->json(array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                ), 400);
        }
    }

    /**
     * edit Speciality
     * @param type $id
     * @return type
     */
    public function editSpeciality($id) {
        return Speciality::find($id);
    }

    /**
     * update Speciality
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateSpeciality(Request $request, $id) {
        $rules = [
            'name' => 'required|max:199|unique:specialities,name,' . $id,
            'description' => 'nullable|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        try {
            $speciality = Speciality::find($id);
            $speciality->name = $request->input('name');
            $speciality->description = $request->input('description') ?? '';
            $speciality->status = $request->input('status');

            if ($speciality->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Speciality updated successfully.',
                    "response" => $speciality,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /*
     * all Speciality
     */

    public function allSpeciality() {
        return response()->json(Speciality::all(), 200);
    }

}
