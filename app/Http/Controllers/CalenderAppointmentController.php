<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use Exception;
use DB;
use DateTime;
use DateTimeZone;
use App\Notifications\AppointmentNotification;
use App\Models\Bill;
use Illuminate\Support\Str;
use App\Models\Symptom;
use App\Models\Prescription;
use App\Models\Medicine;


class CalenderAppointmentController extends Controller {

    /**
     * appointment detail store
     * 
     * @param Request $request
     * @return type
     */
    public function addAppointment(Request $request) {

        $validator = Validator::make($request->all(), [
                    'date' => 'required',
                    'problems' => 'nullable',
                    'patient_id' => 'required',
                    'doctor_id' => 'required',
                    'status' => 'required',
                    'time' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $dt = strtotime($request->input('time'));
            $chdtarr = explode("GMT", $request->input('date'));
            $newdt = strtotime($chdtarr[0]);
            $mettingAt = date("Y-m-d", $newdt) . ' ' . date('H:i:s', $dt);

            $patient = Patient::where('patient_id', $request->patient_id)->first();

            $appointment = new Appointment();
            $appointment->patient_id = $patient->user_id;
            $appointment->doctor_id = $request->input('doctor_id');
            $appointment->serial_no = null;
            $appointment->meeting_at = $mettingAt;
            $appointment->date = date("Y-m-d", $newdt);
            $appointment->time = date('H:i:s', $dt);
            $appointment->duration = null;
            $sympt = array();
            if (isset($request->problems)) {
                foreach ($request->problems as $key => $symptom) {
                    if (!isset($symptom['id'])) {
                        $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                        $sympt[$key]['id'] = $symptom_new->id;
                    } else {
                        $sympt[$key]['id'] = $symptom['id'];
                    }
                    $sympt[$key]['value'] = $symptom['value'];
                    $sympt[$key]['label'] = ucwords($symptom['value']);
                }
            }
            $appointment->problems = json_encode($sympt);
//            $appointment->description = $request->input('description');
            $appointment->status = $request->status;
            $appointment->cancellation_reason = null;
            $appointment->save();
            $nextId = $appointment ? $appointment->id : '1';
            $appointment_id = "H-APP-00$nextId";
            $appointment->appointment_id = $appointment_id;
            if ($appointment->save()) {
                $appointments = Appointment::with('doctor.doctor', 'patient.patient')->find($appointment->id);
                $invoice = new Bill();
                $invoice->bill_id = (string) Str::random(6);
                $invoice->mobile = $appointments->patient->mobile;
                $invoice->appointment_id = $appointments->id;
                $invoice->patient_id = $appointments->patient->patient->id;
                $invoice->doctor_id = $appointments->doctor->doctor->id;
                $invoice->bill_date = now()->format('Y-m-d');
                $invoice->total = $appointments->doctor->doctor->price;
                $invoice->payment_method = "Cash";
                $invoice->status = 1;
                $invoice->save();
                $appoint = array();
                $doctorName = isset($appointments->doctor->name) ? $appointments->doctor->name : '';
                $title = $appointments->appointment_id . $doctorName;
                $appoint['id'] = $appointments->id;
                $appoint['title'] = $appointments->patient->name;
                $appoint['start'] = $appointments->meeting_at;
                $appoint['patient'] = isset($appointments->patient->name) ? $appointments->patient->name : '';
                $appoint['email'] = isset($appointments->patient->email) ? $appointments->patient->email : '';
                $appoint['mobile'] = isset($appointments->patient->mobile) ? $appointments->patient->mobile : '';
                $appoint['patient_id'] = $appointments->patient->patient->patient_id;
                $appoint['doctor_id'] = $appointments->doctor_id;
                $appoint['doctor'] = $doctorName;
                $appoint['duration'] = $appointments->duration;
                $appoint['problem'] = $appointments->problems;
                $appoint['description'] = $appointments->description;
                $appoint['status'] = $appointments->status;
                $appoint['slot'] = $request->slot;
                switch ($appointments->status) {
                    case 0: $appoint['color'] = "rgb(62 201 214)";
                        $appoint['backgroundColor'] = "rgb(62 201 214 / 15%)";
                        $appoint['borderColor'] = "rgb(62 201 214)";
                        break;
                    case 1: $appoint['color'] = "rgb(44 168 127)";
                        $appoint['backgroundColor'] = "rgb(44 168 127 / 15%)";
                        $appoint['borderColor'] = "rgb(44 168 127)";
                        break;
                    case 2: $appoint['color'] = "rgb(70 128 255)";
                        $appoint['backgroundColor'] = "rgb(70 128 255 / 15%)";
                        $appoint['borderColor'] = "rgb(70 128 255)";
                        break;
                    case 3: $appoint['color'] = "rgb(229 138 0)";
                        $appoint['backgroundColor'] = "rgb(229 138 0 / 15%)";
                        $appoint['borderColor'] = "rgb(229 138 0)";
                        break;
                    case 4: $appoint['color'] = "rgb(220 38 38)";
                        $appoint['backgroundColor'] = "rgb(220 38 38 / 15%)";
                        $appoint['borderColor'] = "rgb(220 38 38)";
                        break;
                }
            } else {
                return response()->json(array(
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                            "response" => '',
                ));
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response()->json(['message' => 'Appointment created successfully.', 'response' => $appoint, 'status' => true], 200);
    }

    /**
     * all appointment liting get
     * 
     * @return type
     */
    public function AppointmentListing() {
        $query = Appointment::query()->with('doctor', 'patient');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('doctor_id', auth()->user()->id);
        }
        $appointment = $query->where('meeting_at', '>', now()->format('Y-m-d H:i:s'))->orderBy('created_at', 'DESC')->get();
        return response()->json($appointment);
    }

    /**
     * appointment delete by id
     * 
     * @param type $id
     * @return type
     */
    public function deleteAppointment($id) {
        try {
            $result = Appointment::where('id', $id)->delete();
            if ($result) {
                $data = array(
                    'status' => 200,
                    'message' => 'Appointment deleted successfully.',
                    "response" => null,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * get appointment detail by id
     * 
     * @param type $id
     * @return type
     */
    public function EditAppointment($id) {
        return Appointment::find($id);
    }

    /**
     * appointment detail update by id
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateAppointment(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'date' => 'required',
                    'problems' => 'nullable',
                    'doctor_id' => 'required',
                    'status' => 'required',
                    'time' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $dt = strtotime($request->input('time'));
            $chdtarr = explode("GMT", $request->input('date'));
            $newdt = strtotime($chdtarr[0]);
            $mettingAt = date("Y-m-d", $newdt) . ' ' . date('H:i:s', $dt);

            $appointment = Appointment::find($id);
            $appointment->doctor_id = $request->input('doctor_id');
            $appointment->meeting_at = $mettingAt;
            $appointment->duration = $request->input('duration');
            $appointment->date = date("Y-m-d", $newdt);
            $appointment->time = date('H:i:s', $dt);
            if ($request->problems) {
                $sympt = array();
                foreach ($request->problems as $key => $symptom) {
                    if (!isset($symptom['id'])) {
                        $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                        $sympt[$key]['id'] = $symptom_new->id;
                    } else {
                        $sympt[$key]['id'] = $symptom['id'];
                    }
                    $sympt[$key]['value'] = $symptom['value'];
                    $sympt[$key]['label'] = ucwords($symptom['value']);
                }
                $appointment->problems = json_encode($sympt);
            }
//            $appointment->description = $request->input('description');
            $appointment->status = $request->status;
            $appointment->cancellation_reason = null;
            if ($appointment->save()) {
                $appointments = Appointment::with('patient.patient', 'doctor')->find($id);
                $appoint = array();
                $doctorName = isset($appointments->doctor->name) ? " " . $appointments->doctor->name : '';
                $title = $appointments->appointment_id . $doctorName;
                $appoint['id'] = $appointments->id;
                $appoint['title'] = $appointments->patient->name;
                $appoint['start'] = $appointments->meeting_at;
                $appoint['patient'] = isset($appointments->patient->name) ? $appointments->patient->name : '';
                $appoint['email'] = isset($appointments->patient->email) ? $appointments->patient->email : '';
                $appoint['mobile'] = isset($appointments->patient->mobile) ? $appointments->patient->mobile : '';
                $appoint['patient_id'] = $appointments->patient->patient->patient_id;
                $appoint['doctor_id'] = $appointments->doctor_id;
                $appoint['doctor'] = $doctorName;
                $appoint['duration'] = $appointments->duration;
                $appoint['problem'] = $appointments->problems;
                $appoint['status'] = $appointments->status;
                $appoint['slot'] = $request->slot;
                switch ($appointments->status) {
                    case 0: $appoint['color'] = "rgb(62 201 214)";
                        $appoint['backgroundColor'] = "rgb(62 201 214 / 15%)";
                        $appoint['borderColor'] = "rgb(62 201 214)";
                        break;
                    case 1: $appoint['color'] = "rgb(44 168 127)";
                        $appoint['backgroundColor'] = "rgb(44 168 127 / 15%)";
                        $appoint['borderColor'] = "rgb(44 168 127)";
                        break;
                    case 2: $appoint['color'] = "rgb(70 128 255)";
                        $appoint['backgroundColor'] = "rgb(70 128 255 / 15%)";
                        $appoint['borderColor'] = "rgb(70 128 255)";
                        break;
                    case 3: $appoint['color'] = "rgb(229 138 0)";
                        $appoint['backgroundColor'] = "rgb(229 138 0 / 15%)";
                        $appoint['borderColor'] = "rgb(229 138 0)";
                        break;
                    case 4: $appoint['color'] = "rgb(220 38 38)";
                        $appoint['backgroundColor'] = "rgb(220 38 38 / 15%)";
                        $appoint['borderColor'] = "rgb(220 38 38)";
                        break;
                }

                $data = array(
                    'status' => true,
                    'message' => 'Appointment updated successfully!',
                    "response" => $appoint,
                );
                //return response()->json($appoint, 200);
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * calendar appointment by  doctor id and all
     * @param type $id
     * @return type
     * 
     */
    public function calendarAppoint($id = null) {
        try {
            if (isset($id)) {
                $appointments = Appointment::with('patient.patient', 'doctor')->where('doctor_id', $id)->get();
                $status = array();
                //today appointment status
                $status['scheduled'] = Appointment::where('doctor_id', $id)->Status(0)->Today()->count();
                $status['completed'] = Appointment::where('doctor_id', $id)->Status(1)->Today()->count();
                $status['engaged'] = Appointment::where('doctor_id', $id)->Status(2)->Today()->count();
                $status['waiting'] = Appointment::where('doctor_id', $id)->Status(3)->Today()->count();
                $status['canceled'] = Appointment::where('doctor_id', $id)->Status(4)->Today()->count();

                //today patients appointment
                $appointmentsUser = Appointment::with('patient')->Today()->where('doctor_id', $id)->where('status', '!=', 4)->where('status', '!=', 1)->get();
                $patients = array();
                foreach ($appointmentsUser as $key => $appointment) {
                    $patients[$key]['id'] = $appointment->id;
                    $patients[$key]['name'] = $appointment->patient->name;
                    $patients[$key]['status'] = $appointment->status;
                }
            } else {
                $query = Appointment::query()->with('patient.patient', 'doctor');
                if (auth()->user()->hasRole('Doctor')) {
                    $query->where('doctor_id', auth()->user()->id);
                }
                $appointments = $query->get();
                $status = array();
                //today appointment status
                $query1 = Appointment::query()->Status(0)->Today();
                if (auth()->user()->hasRole('Doctor')) {
                    $query1->where('doctor_id', auth()->user()->id);
                }
                $status['scheduled'] = $query1->count();
                $query2 = Appointment::query()->Status(1)->Today();
                if (auth()->user()->hasRole('Doctor')) {
                    $query2->where('doctor_id', auth()->user()->id);
                }
                $status['completed'] = $query2->count();
                $query3 = Appointment::query()->Status(2)->Today();
                if (auth()->user()->hasRole('Doctor')) {
                    $query3->where('doctor_id', auth()->user()->id);
                }
                $status['engaged'] = $query3->count();
                $query4 = Appointment::query()->Status(3)->Today();
                if (auth()->user()->hasRole('Doctor')) {
                    $query4->where('doctor_id', auth()->user()->id);
                }
                $status['waiting'] = $query4->count();
                $query5 = Appointment::query()->Status(4)->Today();
                if (auth()->user()->hasRole('Doctor')) {
                    $query5->where('doctor_id', auth()->user()->id);
                }
                $status['canceled'] = $query5->count();

                //today patients appointment
                $query6 = Appointment::query()->with('patient');
                if (auth()->user()->hasRole('Doctor')) {
                    $query6->where('doctor_id', auth()->user()->id);
                }
                $appointmentsUser = $query6->Today()->where('status', '!=', 4)->where('status', '!=', 1)->get();
                $patients = array();
                foreach ($appointmentsUser as $key => $appointment) {
                    $patients[$key]['id'] = $appointment->id;
                    $patients[$key]['name'] = $appointment->patient->name;
                    $patients[$key]['status'] = $appointment->status;
                }
            }
            $appoint = array();
            if (count($appointments)) {
                foreach ($appointments as $key => $value) {
                    $doctorName = isset($value->doctor->name) ? " " . $value->doctor->name : '';
                    $title = $value->appointment_id . $doctorName;
                    $getPatient = $value->patient;
                    $appoint[$key]['id'] = $value->id;
                    $appoint[$key]['title'] = $value->patient->name;
                    $appoint[$key]['start'] = $value->meeting_at;
                    $appoint[$key]['patient'] = isset($value->patient->name) ? $value->patient->name : '';
                    $appoint[$key]['email'] = isset($value->patient->email) ? $value->patient->email : '';
                    $appoint[$key]['mobile'] = isset($value->patient->mobile) ? $value->patient->mobile : '';
                    $appoint[$key]['patient_id'] = $getPatient->patient ? $getPatient->patient->patient_id : '';
                    $appoint[$key]['doctor_id'] = $value->doctor_id;
                    $appoint[$key]['doctor'] = $doctorName;
                    $appoint[$key]['duration'] = $value->duration;
                    $appoint[$key]['problem'] = $value->problems;
                    $appoint[$key]['slot'] = Carbon::parse($value->meeting_at)->format('h:i A');
                    $appoint[$key]['status'] = $value->status;
                    switch ($value->status) {
                        case 0: $appoint[$key]['color'] = "rgb(62 201 214)";
                            $appoint[$key]['backgroundColor'] = "rgb(62 201 214 / 15%)";
                            $appoint[$key]['borderColor'] = "rgb(62 201 214)";
                            break;
                        case 1: $appoint[$key]['color'] = "rgb(44 168 127)";
                            $appoint[$key]['backgroundColor'] = "rgb(44 168 127 / 15%)";
                            $appoint[$key]['borderColor'] = "rgb(44 168 127)";
                            break;
                        case 2: $appoint[$key]['color'] = "rgb(70 128 255)";
                            $appoint[$key]['backgroundColor'] = "rgb(70 128 255 / 15%)";
                            $appoint[$key]['borderColor'] = "rgb(70 128 255)";
                            break;
                        case 3: $appoint[$key]['color'] = "rgb(229 138 0)";
                            $appoint[$key]['backgroundColor'] = "rgb(229 138 0 / 15%)";
                            $appoint[$key]['borderColor'] = "rgb(229 138 0)";
                            break;
                        case 4: $appoint[$key]['color'] = "rgb(220 38 38)";
                            $appoint[$key]['backgroundColor'] = "rgb(220 38 38 / 15%)";
                            $appoint[$key]['borderColor'] = "rgb(220 38 38)";
                            break;
                    }
                }
            }
            return response()->json(['appoint' => $appoint, 'status' => $status, 'patients' => $patients], 200);
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }

    /**
     * today Appointment by patient and status, status update 
     * @param type $id
     * @param type $appointStatus
     * @return type
     */
    public function todayAppointments($id = null, $appointStatus = null) {
        $message = 'Today Appointments';
        // appointment status update
        if (isset($id)) {
            $appoint = Appointment::with('patient')->find($id);
            if ($appointStatus != 5) {
                $appoint->status = $appointStatus;
                $appoint->save();
                $message = 'Appointment status has been changed';
            } elseif ($appointStatus == 5) {
                $appoint->patient->notify(new AppointmentNotification($appoint));
                $message = 'Notification has been sent to patient';
            }
        }
        //today appointment status
        $query1 = Appointment::query()->Status(0)->Today();
        if (auth()->user()->hasRole('Doctor')) {
            $query1->where('doctor_id', auth()->user()->id);
        }
        $status['scheduled'] = $query1->count();
        $query2 = Appointment::query()->Status(1)->Today();
        if (auth()->user()->hasRole('Doctor')) {
            $query2->where('doctor_id', auth()->user()->id);
        }
        $status['completed'] = $query2->count();
        $query3 = Appointment::query()->Status(2)->Today();
        if (auth()->user()->hasRole('Doctor')) {
            $query3->where('doctor_id', auth()->user()->id);
        }
        $status['engaged'] = $query3->count();
        $query4 = Appointment::query()->Status(3)->Today();
        if (auth()->user()->hasRole('Doctor')) {
            $query4->where('doctor_id', auth()->user()->id);
        }
        $status['waiting'] = $query4->count();
        $query5 = Appointment::query()->Status(4)->Today();
        if (auth()->user()->hasRole('Doctor')) {
            $query5->where('doctor_id', auth()->user()->id);
        }
        $status['canceled'] = $query5->count();

        //today patients appointment
        $query = Appointment::query()->with('patient');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('doctor_id', auth()->user()->id);
        }
        $appointments = $query->Today()->where('status', '!=', 4)->where('status', '!=', 1)->get();
        $patients = array();
        foreach ($appointments as $key => $appointment) {
            $patients[$key]['id'] = $appointment->id;
            $patients[$key]['name'] = $appointment->patient->name;
            $patients[$key]['status'] = $appointment->status;
        }
        return response()->json(['status' => $status, 'patients' => $patients, 'message' => $message,], 200);
    }

    /**
     * appointment by patient and status, status update 
     * @param type $id
     * @param type $appointStatus
     * @return type
     */
    public function cancelAppointments(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'cancelReason' => 'required|string|max:255',
                    'status' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        // appointment status update
        if (isset($id)) {
            $appoint = Appointment::with('patient')->find($id);
            $appoint->cancellation_reason = $request->cancelReason;
            $appoint->status = $request->status;
            $appoint->save();
            $message = 'Appointment status has been successfully canceled';
        }

        //today appointment status
        $status['scheduled'] = Appointment::Status(0)->Today()->count();
        $status['completed'] = Appointment::Status(1)->Today()->count();
        $status['engaged'] = Appointment::Status(2)->Today()->count();
        $status['waiting'] = Appointment::Status(3)->Today()->count();
        $status['canceled'] = Appointment::Status(4)->Today()->count();
        //today patients appointment
        $appointments = Appointment::with('patient')->Today()->where('status', '!=', 4)->get();
        $patients = array();
        foreach ($appointments as $key => $appointment) {
            $patients[$key]['id'] = $appointment->id;
            $patients[$key]['name'] = $appointment->patient->name;
            $patients[$key]['status'] = $appointment->status;
        }
        return response()->json(['status' => $status, 'patients' => $patients, 'message' => $message,], 200);
    }

    /**
     * patient search by mobile number 
     * @param type $mobile
     * @return type
     */
    public function patientSearch($mobile) {
        $users = User::with('patient', 'city')->where(function ($query) use ($mobile) {
                    $query->where('name', 'like', '%' . $mobile . '%')
                            ->orWhere('email', 'like', '%' . $mobile . '%')
                            ->orWhere('mobile', 'like', '%' . $mobile . '%')
                            ->orWhereHas('patient', function ($query1) use ($mobile) {
                                $query1->where('patient_id', 'like', '%' . $mobile . '%');
                            });
                })->role('patient')->first();
        return response()->json($users, 200);
    }

    /**
     * get doctor schedule detail by id
     * 
     * @param type $doctor_id
     * @return type
     */
    public function doctorSchedule($doctor_id) {
        $schedule = Schedule::where('doctor_id', $doctor_id)->get();
        return response()->json($schedule);
    }

    /**
     * appointmentStatus
     * @param type $id
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function appointmentStatus(Request $request) {
        try {
            $monthStart = now()->startOfMonth()->format('Y-m-d');
            $monthEnd = now()->endOfMonth()->format('Y-m-d');
            $status = array();
            if (isset($id)) {
                $appointments = Appointment::query()->where('doctor_id', $request->id);
                if (!is_null($request->start_date) || !is_null($request->end_date)) {
                    $appointments->whereDate('meeting_at', '>=', Carbon::parse(trim($request->start_date))->format('Y-m-d'));
                    $appointments->whereDate('meeting_at', '<=', Carbon::parse(trim($request->end_date))->format('Y-m-d'));
                } else {
                    $appointments->whereDate('meeting_at', '>=', $monthStart);
                    $appointments->whereDate('meeting_at', '<=', $monthEnd);
                }
                //today appointment status
                $status['scheduled'] = $appointments->clone()->Status(0)->count();
                $status['completed'] = $appointments->clone()->Status(1)->count();
                $status['engaged'] = $appointments->clone()->Status(2)->count();
                $status['waiting'] = $appointments->clone()->Status(3)->count();
                $status['canceled'] = $appointments->clone()->Status(4)->count();
            } else {
                $appointments = Appointment::query();
                if (auth()->user()->hasRole('Doctor')) {
                    $appointments->where('doctor_id', auth()->user()->id);
                }
                if (!is_null($request->start_date) || !is_null($request->end_date)) {
                    $appointments->whereDate('meeting_at', '>=', Carbon::parse(trim($request->start_date))->format('Y-m-d'));
                    $appointments->whereDate('meeting_at', '<=', Carbon::parse(trim($request->end_date))->format('Y-m-d'));
                } else {
                    $appointments->whereDate('meeting_at', '>=', $monthStart);
                    $appointments->whereDate('meeting_at', '<=', $monthEnd);
                }
                $status['scheduled'] = $appointments->clone()->Status(0)->count();
                $status['completed'] = $appointments->clone()->Status(1)->count();
                $status['engaged'] = $appointments->clone()->Status(2)->count();
                $status['waiting'] = $appointments->clone()->Status(3)->count();
                $status['canceled'] = $appointments->clone()->Status(4)->count();
            }
            return response()->json(['status' => $status], 200);
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }
    
    public function referredSearch(Request $request){
        
        try{
            
            $referred = Prescription::where('referred_by', 'LIKE','%'.$request->search.'%')->groupBy('referred_by')->pluck('referred_by');
            return response()->json(['status' => true,  'response' => $referred], 200);
            
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }
    
    public function medicineSearch(Request $request){
        try{
            
            $medicine = Medicine::with('drugType')->where('name', 'LIKE','%'.$request->search.'%')->get();
            return response()->json(['status' => true,  'response' => $medicine], 200);
            
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
    }

}
