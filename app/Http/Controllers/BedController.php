<?php

namespace App\Http\Controllers;

use App\Models\Bed;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BedController extends Controller {

    /**
     * New Bed create
     * 
     * @param Request $request
     * @return type
     */
    public function addBed(Request $request) {
        $rules = [
            'type' => 'required',
            'limit' => 'required|numeric',
            'charge' => 'required|numeric',
            'description' => 'nullable|max:255'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $bed = new Bed();
            $bed->type = $request->input('type');
            $bed->description = $request->input('description');
            $bed->limit = $request->input('limit');
            $bed->charge = $request->input('charge');
            $bed->status = $request->input('status') ?? 1;
            $bed->save();
            $data = [
                'status' => 'success',
                'response' => $bed,
            ];
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
        return response($data);
    }

    /**
     * Listing all Bed
     * 
     * @param type $id
     * @return type
     */
    function BedListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Bed::query();
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('type', 'like', '%' . $request->search . '%')
                        ->orWhere('limit', 'like', '%' . $request->search . '%')
                        ->orWhere('charge', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        $bed = $query->paginate($length);
        return $bed;
    }

    /**
     * Bed delete by id
     * 
     * @param type $id
     * @return type
     */
    function BedDelete($id) {
        try {
            $bed = Bed::doesntHave('bedAssign')->find($id);
            if (!empty($bed)) {
                if ($bed->delete()) {
                    $data = [
                        'status' => 'success',
                        'message' => 'Bed deleted successfully.'
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'message' => 'Something went wrong, Please try again later!'
                    ];
                }
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'You cannot delete this bed as it is currently assigned.',
                    "response" => '',
                );
            }
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 422);
        }
        return response($data);
    }

    /**
     * Bed detail by id
     * 
     * @param type $id
     * @return type
     */
    public function BedEdit($id) {
        return Bed::find($id);
    }

    /**
     * Bed detail update by id 
     * 
     * @param Request $request
     * @return type
     */
    public function BedUpdate(Request $request, $id) {
        $rules = [
            'type' => 'required',
            'limit' => 'required|numeric',
            'charge' => 'required|numeric',
            'description' => 'nullable|max:255'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $bed = Bed::find($id);
            $bed->type = $request->input('type');
            $bed->description = $request->input('description');
            $bed->limit = $request->input('limit');
            $bed->charge = $request->input('charge');
            $bed->status = $request->input('status');
            $bed->save();
            $data = [
                'status' => 'success',
                'response' => $bed,
            ];
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => [
                    'error' => $e->getMessage(),
                    'errorLine' => $e->getLine(),
                    'errorFile' => $e->getFile()
                ],
                'response' => null
            ];
        }
        return response($data);
    }

}
