<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Models\Appointment;
use App\Models\Bill;
use App\Jobs\EmailVerificationJob;

class StaffController extends Controller {

    function __construct() {
        $this->middleware('permission:staff_list', ['only' => ['listingSpeciality']]);
        $this->middleware('permission:staff_create', ['only' => ['addStaff']]);
        $this->middleware('permission:staff_edit', ['only' => ['editStaff', 'updateStaff']]);
        $this->middleware('permission:staff_delete', ['only' => ['deleteStaff']]);
    }

    /**
     * list staff
     * @param Request $request
     * @return type
     */
    public function index(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = User::query()->with('roles')->whereDoesntHave("roles", function($q) {
            $q->whereIn("name", ["Doctor","Patient","Admin"]);
        });
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('email', 'like', '%' . $request->search . '%')
                        ->orWhere('mobile', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        $staff = $query->paginate($length);
        return $staff;
    }

    /**
     * New staff create
     * 
     * @param Request $request
     * @return type
     */
    public function addStaff(Request $request) {
        $rules = [
            'name' => 'required|regex:/^[a-z\s\.]+$/i|min:2|max:255',
            'email' => 'required|email|unique:users|email:rfc,dns',
            'password' => 'required|min:8',
            'gender' => 'required',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
            'picture' => 'nullable|mimes:jpeg,jpg,png,gif,svg,webp|max:5120',
            'address' => 'required|max:255',
            'user_role' => 'required',
            'status' => 'required',
        ];
        $messages = [
            'name.required' => 'The Name field is required.',
            'name.max' => 'The Name must not be greater than 255 characters.',
            'password.required' => 'The Password field is required.',
            'password.confirmed' => 'The password confirmation does not match.',
            'password.min' => 'The Password must be at least 8 characters.',
            'gender.required' => 'The Gender field is required.',
            'mobile.required' => 'The Mobile field is required.',
            'mobile.numeric' => 'The Mobile must be a number.',
            'mobile.min' => 'The Mobile must be at least 10.',
            'mobile.unique' => 'The Mobile has already been taken.',
            'picture.required' => 'The Picture field is required.',
            'picture.mimes' => 'The Picture must be a file of type: jpeg, jpg, png, gif, svg, webp.',
            'picture.max' => 'The Picture may not be greater than 5MB.',
            'address.required' => 'The Address field is required.',
            'address.max' => 'The Address must not be greater than 255 characters.',
            'email.required' => 'The Email field is required.',
            'email.email' => 'The Email must be a valid email address.',
            'email.unique' => 'The Email has already been taken.',
            'status.required' => 'The Status field is required.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {
            $path = NULL;
            if ($request->hasFile('picture')) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'users'");
                $nextId = uniqid();
                $file_name = trim($request->name) . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $path = 'staff' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'staff');
            }
            $staff = new User;
            $staff->name = $request->input('name');
            $staff->email = $request->input('email');
            $staff->password = Hash::make($request->input('password'));
            $staff->mobile = $request->input('mobile');
            $staff->gender = $request->input('gender');
            $staff->picture = $path;
            $staff->address = $request->input('address');
            $staff->status = $request->input('status');
            $saved = $staff->save();
            $staff->assignRole($request->user_role);
            if ($saved) {
                dispatch(new EmailVerificationJob($staff->id));
                return response()->json(array(
                            'status' => true,
                            'message' => 'Staff added successfully.',
                            "response" => $staff,
                                ), 200);
            } else {
                return response()->json(array(
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                            "response" => '',
                                ), 422);
            }
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 422);
        }
    }

    /**
     * Staff delete by id
     * 
     * @param type $id
     * @return type
     */
    function deleteStaff($id) {
        try {
            $user = User::with('roles')->find($id);
            $user->roles()->detach();
            $saved = $user->delete();
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 422);
        }
        if ($saved) {
            $data = [
                'status' => 'success',
                'message' => 'Staff deleted successfully.'
            ];
        } else {
            $data = [
                'status' => 'error',
                'message' => 'Some thing went wrong, please try again.'
            ];
        }
        return response($data);
    }

    /**
     * staff detail by id
     * 
     * @param type $id
     * @return type
     */
    public function editStaff($id) {
        return User::with('roles')->find($id);
    }

    /**
     * staff detail update by id 
     * 
     * @param Request $request
     * @return type
     */
    public function updateStaff(Request $request) {
        $rules = [
            'name' => 'required|regex:/^[a-z\s\.]+$/i|min:2|max:255',
            'email' => 'required|email|email:rfc,dns|' . Rule::unique('users')->ignore($request->input('id')),
            'gender' => 'required',
            'mobile' => 'required|numeric|digits:10|' . Rule::unique('users')->ignore($request->input('id')),
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'address' => 'required|max:255',
            'status' => 'required',
            'user_role' => 'required',
            'id' => 'required',
        ];
        $messages = [
            'name.required' => 'The Name field is required.',
            'name.max' => 'The Name must not be greater than 255 characters.',
            'password.required' => 'The Password field is required.',
            'password.confirmed' => 'The password confirmation does not match.',
            'password.min' => 'The Password must be at least 8 characters.',
            'gender.required' => 'The Gender field is required.',
            'mobile.required' => 'The Mobile field is required.',
            'mobile.numeric' => 'The Mobile must be a number.',
            'mobile.min' => 'The Mobile must be at least 10.',
            'mobile.unique' => 'The Mobile has already been taken.',
            'picture.mimes' => 'The Picture must be a file of type: jpeg, jpg, png, gif, svg, webp.',
            'picture.max' => 'The Picture may not be greater than 5MB',
            'address.required' => 'The Address field is required.',
            'address.max' => 'The Address must not be greater than 255 characters.',
            'email.required' => 'The Email field is required.',
            'email.email' => 'The Email must be a valid email address.',
            'email.unique' => 'The Email has already been taken.',
            'status.required' => 'The Status field is required.',
            'id.required' => 'The Id field is required.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        $saved = false;
        try {
            $staff = User::where('id', $request->input('id'))->first();
            $staff->name = $request->input('name');
            $staff->email = $request->input('email');
            $staff->password = Hash::make($request->input('password'));
            $staff->mobile = $request->input('mobile');
            $staff->gender = $request->input('gender');
            if ($request->hasFile('picture')) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'users'");
                $nextId = uniqid();
                $file_name = trim($request->name) . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $path = 'staff' . '/' . $request->file('picture')->storeAs($request->user()->id, $fileName, 'staff');
                if (File::exists($staff->picture)) {
                    unlink($staff->picture);
                }
                $staff->picture = $path;
            }
            $staff->address = $request->input('address');
            $staff->status = $request->input('status');

            if ($staff->save()) {
                if ($staff->roles->isNotEmpty()) {
                    if ($request->user_role != $staff->roles->first()->id) {
                        $staff->removeRole($staff->roles->first()->id);
                        // assign Role to User
                        $staff->assignRole($request->user_role);
                    }
                }
                return response()->json(array(
                            'status' => true,
                            'message' => 'Staff updated successfully.',
                            "response" => $staff,
                                ), 200);
            } else {
                return response()->json(array(
                            'status' => false,
                            'message' => 'Something went wrong, Please try again later!',
                            "response" => '',
                                ), 422);
            }
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $e->getMessageBag()->toArray()
                            ), 500);
        }
    }
    public function dashboard() {
         $staff = User::role('Staff')->where('status',1)->count();
        $query = User::query()->role('Doctor');
                if (auth()->user()->hasRole('Doctor')) {
            $query->where('id', auth()->user()->id);
        }
        $doctor = $query->where('status',1)->count();
        $patient_query = User::query()->role('Patient')->where('status',1);
        $patient = $patient_query->clone()->count();
        $new_patient = $patient_query->clone()->whereYear('created_at', Carbon::now()->year)
    ->whereMonth('created_at', Carbon::now()->month)->count();
        $query1 = Appointment::query();
        if (auth()->user()->hasRole('Doctor')) {
            $query1->where('doctor_id', auth()->user()->id);
        }
        $appointments = $query1->clone()->count();
        $complete_appointments = $query1->clone()->where('status',1)->count();
        $schedule_appointments = $query1->clone()->where('status',0)->count();
        $cancel_appointments   = $query1->clone()->where('status',4)->count();
        $query2 = Bill::query();
        if (auth()->user()->hasRole('Doctor')) {
            $query2->where('doctor_id', auth()->user()->id);
        }
        $revenue = $query2->where('status',1)->sum('total');

        return response()->json(['staff'=>$staff,
            'doctor'=>$doctor,
            'appointments' =>$appointments,
            'revenue' =>$this->number_shorten($revenue),
            'patient' =>$patient,
            'total_revenue'=>$revenue,
            'complete_appointments'=>$complete_appointments,
            'schedule_appointments'=>$schedule_appointments,
            'cancel_appointments' =>$cancel_appointments,
            'new_patient' =>$new_patient,
                ]);
    }
    public function number_shorten($number, $precision = 2)
    {
        $suffixes = ['', 'K', 'M', 'B', 'T', 'Qa', 'Qi'];
        $index = (int) log(abs($number), 1000);
        $index = max(0, min(count($suffixes) - 1, $index)); // Clamps to a valid suffixes' index
        return number_format($number / 1000 ** $index, $precision) . $suffixes[$index];
    }

}
