<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use Exception;
use DB;
use DateTime;
use DateTimeZone;
use App\Models\Symptom;
use Illuminate\Support\Facades\Http;
use App\Models\Bill;
use Illuminate\Support\Str;

class AppointmentController extends Controller {

    function __construct() {
        $this->middleware('permission:appointment_list', ['only' => ['AppointmentListing']]);
        $this->middleware('permission:appointment_create', ['only' => ['addAppointment']]);
        $this->middleware('permission:appointment_edit', ['only' => ['editAppointment', 'updateAppointment']]);
        $this->middleware('permission:appointment_delete', ['only' => ['deleteAppointment']]);
    }

    /**
     * appointment detail store
     * 
     * @param Request $request
     * @return type
     */
    public function addAppointment(Request $request) {
        $rules = [
            'date' => 'required|date|after:yesterday',
            'time' => 'required',
            'problems' => 'nullable',
            'patient_id' => 'required',
            'doctor_id' => 'required'
        ];

        $messages = [
            'date.required' => 'The Appointment Date field is required.',
            'time.required' => 'The Appointment Time field is required.',
            'patient_id.required' => 'Search patient by mobile number.',
            'doctor_id.required' => 'The Doctor field is required.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'appointment'");
            $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : '1';
            $appointment_id = "H-APP-00$nextId";
            $appointment = new Appointment();
            $appointment->appointment_id = $appointment_id;
            $appointment->patient_id = $request->input('patient_id');
            $appointment->doctor_id = $request->input('doctor_id');
            $appointment->serial_no = null;
            $appointment->date = Carbon::parse($request->input('date'))->format('Y-m-d');
            $appointment->time = Carbon::parse($request->input('time'))->format('H:i');
            $appointment->meeting_at = Carbon::parse($request->input('date'))->format('Y-m-d') . " " . Carbon::parse($request->input('time'))->format('H:i');
            $appointment->duration = null;
            $sympt = array();
            if (isset($request->problems)) {
                foreach ($request->problems as $key => $symptom) {
                    if (!isset($symptom['id'])) {
                        $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                        $sympt[$key]['id'] = $symptom_new->id;
                    } else {
                        $sympt[$key]['id'] = $symptom['id'];
                    }
                    $sympt[$key]['value'] = $symptom['value'];
                    $sympt[$key]['label'] = ucwords($symptom['value']);
                }
            }
            $appointment->problems = json_encode($sympt);
            $appointment->status = 0;
            $appointment->payment_status = $request->payment_status;
            if ($appointment->save()) {
                $appointment = Appointment::with('doctor.doctor', 'patient.patient')->find($appointment->id);
                // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'appointment';
            $operation = 'appointment_add';
            $description = 'Dr. '.$appointment?->doctor?->name.' appointment added for patient ' . $appointment?->patient?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $appointment, $custom_properties, $log_name, $description);
            $invoice = new Bill();
            $invoice->mobile = $appointment->patient->mobile;
            $invoice->appointment_id = $appointment->id;
            $invoice->patient_id = $appointment->patient->patient->id;
            $invoice->doctor_id = $appointment->doctor->doctor->id;
            $invoice->bill_date = now()->format('Y-m-d');
            $invoice->total = $appointment->doctor->doctor->price;
            $invoice->payment_method = "Cash";
            $invoice->status = 1;
            if($invoice->save()){
                // Activity logs
             $causer = null;
             $log_name = 'bill';
             $operation = 'bill_add';
             $description = 'Consultation Bill of Dr. '.$appointment?->doctor?->name.' appointment added for patient ' . $appointment?->patient?->name;
             $custom_properties = ['application' => config('app.name'),
                 'operation' => $operation,
                 'causer_name' => null,
             ];
             store_activity_log($causer, $invoice, $custom_properties, $log_name, $description);
            }
                    $series = "B.ID-000000";
                    $expNum = explode('-', $series);
                    $numbers = str_pad($expNum[1] + (int)$invoice->id, 6, "0", STR_PAD_LEFT);
                    $invoice->bill_id = $expNum[0] . '-' . $numbers;
                    $invoice->save();
                $webhookUrl = $appointment->doctor->doctor->webhook_url;
                if($webhookUrl){
                    $apiKey = $appointment->doctor->doctor->api_key;
                // Example appointment data
                $appointmentData['appointment_id'] = $appointment->appointment_id;
//                $appointmentData['start'] = $appointments->meeting_at;
                $appointmentData['patient_name'] = isset($appointment->patient->name) ? $appointment->patient->name : '';
                $appointmentData['patient_email'] = isset($appointment->patient->email) ? $appointment->patient->email : '';
                $appointmentData['patient_mobile'] = isset($appointment->patient->mobile) ? $appointment->patient->mobile : '';
                $appointmentData['patient_id'] = $appointment->patient->patient->patient_id;
                $appointmentData['doctor_name'] = isset($appointment->doctor->name) ? $appointment->doctor->name : '';
                $appointmentData['doctor_email'] = isset($appointment->doctor->email) ? $appointment->doctor->email : '';
                $appointmentData['doctor_mobile'] = isset($appointment->doctor->mobile) ? $appointment->doctor->mobile : '';
//                $appointmentData['duration'] = $appointments->duration;
                $appointmentData['problem'] = $appointment->problems;
                $appointmentData['status'] = $appointment->status;
                $appointmentData['date'] = $appointment->date;
                $appointmentData['time'] = $appointment->time;
                
                // Send the webhook
                $result = $this->sendWebhook($appointmentData, $webhookUrl, $apiKey);      
                }
                $data = array(
                    'status' => true,
                    'message' => 'Appointment created successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * all appointment liting get
     * 
     * @return type
     */
    public function AppointmentListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Appointment::query()->with('doctor.doctor.specialization', 'patient.patient', 'bill','prescription');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('doctor_id', auth()->user()->id);
        }
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('appointment_id', 'like', '%' . $request->search . '%')
                        ->orWhereHas('doctor', function ($query2) use ($request) {
                            $query2->where('name', 'like', '%' . $request->search . '%');
                        })
                        ->orWhereHas('patient', function ($query3) use ($request) {
                            $query3->where('name', 'like', '%' . $request->search . '%');
                        });
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('date', '>=', $start)->whereDate('date', '<=', $end);
        }
        if (isset($request->status) && $request->status != '') {            
            $query->where('status',  $request->status);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') != 'doctor.name' && request('field') != 'patient.name') {
                $query->orderBy(request('field'), request('sortOrder'));
            }
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') == 'doctor.name' || request('field') == 'patient.name') {
                if (strtoupper(request('sortOrder')) == "DESC") {
                    $appointment = $query->get()->sortByDesc(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                } else {
                    $appointment = $query->get()->sortBy(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                }
                // Convert JSON data to associative array
                $arrayData = json_decode($appointment, true);

                // Convert the associative array into the desired format
                $formattedArray = array_values($arrayData);

                // Output the formatted array as JSON
                $appointments1 = json_encode($formattedArray, JSON_PRETTY_PRINT);

                $appointments = paginate(json_decode($appointments1, true), $length, null, ['path' => $request->url(), 'query' => $request->query()]);
            }
        } else {
            $appointments = $query->paginate($length);
        }

        if (isset($appointments)) {
            return $appointments;
        } else {
            return $appointments = $query->paginate($length);
        }
    }

    /**
     * appointment delete by id
     * 
     * @param type $id
     * @return type
     */
    public function deleteAppointment($id) {
        try {
            $result = Appointment::with('doctor.doctor', 'patient.patient')->doesntHave('bill')->doesntHave('prescription')->find($id);
            if (!empty($result)) {
                      // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'appointment';
            $operation = 'appointment_delete';
            $description = 'Dr. '.$result?->doctor?->name.' appointment delete for patient ' . $result?->patient?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $result, $custom_properties, $log_name, $description);
               if($result->delete()){
                $data = array(
                    'status' => true,
                    'message' => 'Appointment deleted successfully.',
                    "response" => null,
                );
               } 
            }else{
                $data = array(
                    'status' => false,
                    'message' => 'The appointment is associated with an bill or prescription and cannot be deleted.',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * get appointment detail by id
     * 
     * @param type $id
     * @return type
     */
    public function editAppointment($id) {
        return Appointment::with('patient.patient')->where('id', $id)->first();
    }

    /**
     * appointment detail update by id
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateAppointment(Request $request, $id) {

        $rules = [
            'date' => 'required|date',
            'time' => 'required',
            'problems' => 'nullable',
            'doctor_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $appointment = Appointment::find($id);
            $appointment->doctor_id = $request->input('doctor_id');
            $appointment->date = Carbon::parse($request->input('date'))->format('Y-m-d');
            $appointment->time = date('H:i:s', strtotime($request->input('time')));
            $appointment->duration = $request->input('duration');
            $appointment->meeting_at = Carbon::parse($request->input('date'))->format('Y-m-d') . " " . date('H:i:s', strtotime($request->input('time')));
            $sympt = array();
            if (isset($request->problems)) {
                foreach ($request->problems as $key => $symptom) {
                    if (!isset($symptom['id'])) {
                        $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                        $sympt[$key]['id'] = $symptom_new->id;
                    } else {
                        $sympt[$key]['id'] = $symptom['id'];
                    }
                    $sympt[$key]['value'] = $symptom['value'];
                    $sympt[$key]['label'] = ucwords($symptom['value']);
                }
            }
            $appointment->problems = json_encode($sympt);
            $appointment->status = 0;
            $appointment->payment_status = $request->payment_status;
            if ($appointment->save()) {
                // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'appointment';
            $operation = 'appointment_modification';
            $description = 'Dr. '.$appointment?->doctor?->name.' appointment updated for patient ' . $appointment?->patient?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $appointment, $custom_properties, $log_name, $description);
                $data = array(
                    'status' => true,
                    'message' => 'Appointment updated successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * doctor slot create
     * 
     * @param Request $request
     * @return type
     */
    public function doctorSlot(Request $request) {
        $day = Carbon::parse($request->input('date'))->format('D');
        $date = Carbon::parse($request->input('date'))->format('Y-m-d');
        $schedules = Schedule::where('doctor_id', $request->input('doctor_id'))->get();
        $slots = array();
        foreach ($schedules as $schedule) {
            if ($schedule->available_days == 'Sunday' && $day == 'Sun') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Monday' && $day == 'Mon') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Tuesday' && $day == 'Tue') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Wednesday' && $day == 'Wed') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Thursday' && $day == 'Thu') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Friday' && $day == 'Fri') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Saturday' && $day == 'Sat') {
                $slots = $this->getSlot($schedule, $day, $date);
            }
        }
        return response($slots);
    }

    /**
     * generate doctor slots
     * @param type $schedule
     * @param type $day
     * @param type $date
     * @return type
     */
    static public function getSlot($schedule, $day, $date) {
        $startTime = strtotime($date . ' ' . $schedule->start_time);
        $endTime = strtotime($date . ' ' . $schedule->end_time);
        $durationTime = Carbon::parse($schedule->per_patient_time)->format('i') * 60;
        $dr = $schedule->doctor_id;
        $times = [];
        while ($startTime < $endTime) {
            if ($startTime > strtotime(date('Y-m-d H:i:s'))) {
                $times[] = $startTime;
            }
            $startTime += $durationTime;
        }
        $slots = [];
        foreach ($times as $time) {
            if (date("H:i", $time) <= date("H:i", $endTime)) {
                $slots[] = [
                    'time' => date("h:i A", $time),
                    'duration' => $schedule->per_patient_time,
                    'booked' => self::isSlotBooked($date, $time, $dr),
                ];
            }
        }
        return $slots;
    }

    /**
     * get appointment by  doctor id, date, and time
     * @param type $day
     * @param type $time
     * @param type $dr
     * @return type
     */
    static public function isSlotBooked($day, $time, $dr) {
        return $appointment = Appointment::where('meeting_at', Carbon::parse($day)->format('Y-m-d') . ' ' . date("H:i:s", $time))
                        ->where('doctor_id', $dr)
                        ->where('status', '!=', 4)->count();
    }

    /**
     * calendarAppoint
     * @return type
     */
    public function calendarAppoint() {
        try {
            $appointments = Appointment::with('patient', 'doctor')->get();
            $appoint = array();
            if (count($appointments)) {
                foreach ($appointments as $key => $value) {
                    $doctorName = isset($value->doctor->name) ? " " . $value->doctor->name : '';
                    $title = $value->appointment_id . $doctorName;
                    $appoint[$key]['title'] = $title;
                    $appoint[$key]['start'] = Carbon::parse($value->meeting_at)->format('Y-m-d h:i:s');
                    $appoint[$key]['patient'] = isset($value->patient->name) ? $value->patient->name : '';
                    $appoint[$key]['mobile'] = isset($value->patient->mobile) ? $value->patient->mobile : '';
                    $appoint[$key]['doctor'] = $doctorName;
                    $appoint[$key]['duration'] = $value->duration;
                    $appoint[$key]['problem'] = $value->problems;
                    $appoint[$key]['status'] = $value->status;
                }
                $data = array(
                    'status' => true,
                    'message' => 'Appointment list available',
                    "response" => $appoint,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Appointment list no available',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * update Appointment Status
     * @param type $id
     * @param type $status
     * @return type
     */
    public function updateAppointmentStatus($id, $status) {
        try {
            $appointment = Appointment::with('bill', 'patient.patient')->find($id);
            $appointment->status = $status;
            if ($appointment->save()) {
                if($status == 1){
                    $appointment->bill()->update(['status'=>1]);
                    $appointment->patient->patient()->update([
                        'visit_count' => 1 + $appointment?->patient?->patient?->visit_count,
                        'last_visit_date' =>$appointment->date
                            ]);
                }
                // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'appointment';
            $operation = 'appointment_status_update';
            $description = 'Dr. '.$appointment?->doctor?->name.' appointment status updated for patient ' . $appointment?->patient?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $appointment, $custom_properties, $log_name, $description);
                $data = array(
                    'status' => true,
                    'message' => 'Appointment status updated successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }
    // Function to send webhook
    public function sendWebhook($appointmentData, $webhookUrl, $apiKey)
    {
        $response = Http::withHeaders([
                        'x-api-key' => $apiKey,
                    ])->post($webhookUrl, $appointmentData);

        return $response->json();
    }
    
    public function appointmentDetails($id) {
        $appointment = Appointment::with('doctor.doctor.specialization', 'patient.patient', 'bill')->find($id);
        return response()->json($appointment, 200);
    }

}
