<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Patient;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\hasfile;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Exception;
use Carbon\Carbon;
use App\Models\Appointment;
use App\Models\Bill;
use App\Models\City;
use App\Models\Prescription;
use App\Models\Document;
use Maatwebsite\Excel\Facades\Excel; 
use App\Imports\PatientsImport;

class PatientController extends Controller {

    function __construct() {
        $this->middleware('permission:patient_list', ['only' => ['patientListing', 'city']]);
        $this->middleware('permission:patient_create', ['only' => ['addPatient']]);
        $this->middleware('permission:patient_edit', ['only' => ['EditPatient', 'updatePatient']]);
        $this->middleware('permission:patient_delete', ['only' => ['deletepatient']]);
    }

    /**
     * add Patient
     * @param Request $request
     * @return type
     */
    public function addPatient(Request $request) {
        $rules = [
            'name' => 'required|regex:/^[a-z\s\.]+$/i|min:2|max:255',
            'email' => 'nullable|email|email:rfc,dns|unique:users',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'address' => 'required|max:255',
            'gender' => 'required',
            'dob' => 'nullable|before:today',
            'age' => 'required|numeric',
        ];
        $messages = [
            'picture.max' => 'The picture may not be greater than 5MB',
            'dob.before' => 'The date of birth must be a date before today',
            'dob.date' => 'The date format is invalid'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {

            if ($request->hasFile('picture')) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'patient'");
                $nameUniq = $statement[0]->Auto_increment ? $statement[0]->Auto_increment : uniqid();
                $file_name = time() . '_' . $nameUniq;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $path = 'patient' . '/' . $request->file('picture')->storeAs($nameUniq, $fileName, 'patient');
            } else {
                $path = NULL;
            }

            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->mobile = $request->input('mobile');
            $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
//            $user->password = Hash::make($request->input('password'));
            $user->address = $request->input('address');
            $user->picture = $path;
            $user->gender = $request->input('gender');
            $user->status = 1;
            $user->city = isset($request->city) ? $request->city : NULL;
            $user->save();

            $user->assignRole(4);
            $patient = new Patient();
            $patient->user_id = $user->id;
            $patient->blood_group = $request->input('blood_group');
            $patient->age = $request->age;
            $patient->save();
            $nextId = $patient ? $patient->id : '1';
            $patientId = "H-PA-00$nextId";
            $patient->patient_id = $patientId;
            if ($patient->save()) {
                 // Activity logs
            $causer = auth()->user();
            $log_name = 'patient';
            $operation = 'patient_add';
            $description = 'patient ' . $user?->name .' added';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                $data = array(
                    'status' => true,
                    'message' => 'Patient details stored successfully.',
                    "response" => $patient,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /*
     * * Patient Listing. 
     */

    public function patientListing(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = User::query()->with('patient', 'city')->role('Patient');
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('email', 'like', '%' . $request->search . '%')
                        ->orWhere('mobile', 'like', '%' . $request->search . '%')
                        ->orWhereHas('patient', function ($query2) use ($request) {
                            $query2->where('patient_id', 'like', '%' . $request->search . '%');
                        });
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            $query->orderBy(request('field'), request('sortOrder'));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        $patients = $query->paginate($length);
        return $patients;
    }

    /*
     * * Delete Patient 
     */

    public function deletepatient($id) {

        try {
            $user = User::doesntHave('userAppointment')->doesntHave('prescription')->doesntHave('document')->with('Patient')->role('Patient')->find($id);
            if (!empty($user)) {
//                $user->removeRole('Patient');
                $user->patient()->delete();
                if ($user->delete()) {
                    $data = array(
                        'status' => true,
                        'message' => 'The patient deleted successfully.',
                        "response" => null,
                    );
                } else {
                    $data = array(
                        'status' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        "response" => '',
                    );
                }
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'The patient is associated with an appointment, prescription or documents and cannot be deleted.',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    public function EditPatient($id) {
        return User::with('patient', 'city')->role('patient')->find($id);
    }

    /*
     * *  Update Patient
     */

    public function updatePatient(Request $request) {
        $rules = [
            'name' => 'required|regex:/^[a-z\s\.]+$/i||min:2|max:255',
            'email' => 'nullable|email|email:rfc,dns|unique:users,email,' . $request->input('id'),
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,' . $request->input('id'),
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg,webp|max:5120',
            'address' => 'required|max:255',
            'gender' => 'required',
            'dob' => 'nullable|before:today',
            'age' => 'required|numeric',
        ];
        $messages = [
            'picture.max' => 'The picture may not be greater than 5MB',
            'gender.required' => 'The gender field is required',
            'dob.before' => 'The date of birth must be a date before today',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $user = User::with('patient')->role('Patient')->find($request->input('id'));
            $path = "";
            if ($request->hasFile('picture')) {
                $nextId = $request->input('id');
                $file_name = time() . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $path = 'patient' . '/' . $request->file('picture')->storeAs($nextId, $fileName, 'patient');
                if (File::exists($user->picture)) {
                    unlink($user->picture);
                }
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->mobile = $request->input('mobile');
            $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
//            $user->password = Hash::make($request->input('password'));
            $user->address = $request->input('address');
            if (!empty($path)) {
                $user->picture = $path;
            }
            $user->gender = $request->input('gender');
            $user->status = $request->input('status') ?? 1;
            $user->city = isset($request->city) ? $request->city : NULL;
            $user->save();

            $patient = $user->patient;
            $patient->user_id = $user->id;
            $patient->blood_group = $request->input('blood_group');
            $patient->age = $request->age;
            if ($patient->save()) {
                       // Activity logs
            $causer = auth()->user();
            $log_name = 'patient';
            $operation = 'patient_modification';
            $description = 'modification in patient ' . $user?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                $response = User::with('patient')->role('Patient')->where('id', $request->id)->first();
                $data = array(
                    'status' => true,
                    'message' => 'Patient details updated successfully.',
                    "response" => $response,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * search patient by mobile number
     * 
     * @param type $mobile
     * @return type
     */
    public function searchPatient($search) {
        try {
            $user = User::with('patient', 'city')->where(function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%')
                                ->orWhere('email', 'like', '%' . $search . '%')
                                ->orWhere('mobile', 'like', '%' . $search . '%')
                                ->orWhereHas('patient', function ($query1) use ($search) {
                                    $query1->where('patient_id', 'like', '%' . $search . '%');
                                });
                    })->role('Patient')->get();
            if ($user->isNotEmpty()) {
                $data = array(
                    'status' => true,
                    'message' => 'Patient data fetched successfully.',
                    "response" => $user,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Patient not found in our records!',
                    "response" => $user,
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * search patient by mobile number
     * 
     * @param type $mobile
     * @return type
     */
    public function patientSearch($search) {

        $user = User::with('patient', 'city')->where(function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%')
                            ->orWhere('email', 'like', '%' . $search . '%')
                            ->orWhere('mobile', 'like', '%' . $search . '%')
                            ->orWhereHas('patient', function ($query1) use ($search) {
                                $query1->where('patient_id', 'like', '%' . $search . '%');
                            });
                })->role('Patient')->first();
        return response()->json($user, 200);
    }

    /**
     * get patient detail by id
     * 
     * @param type $id
     * @return type
     */
    public function patientDetails($id) {
        $patient = User::with('patient', 'city')->role('Patient')->where('id', $id)->first();
        return response($patient);
    }

    /**
     * patient password update by id
     * 
     * @param Requesy $request
     */
    public function updatePassword(Request $request) {
        $user = User::where('id', $request->input('id'))->role('Patient')->first();
        $validator = Validator::make($request->all(), [
                    'old_password' => [
                        'required', function ($attribute, $value, $fail) use ($user) {
                            if (!Hash::check($value, $user->password)) {
                                $fail('Old Password didn\'t match');
                            }
                        },
                    ],
                    'password' => ['required', 'string', 'min:8', 'different:old_password', 'confirmed'],
        ]);

        //checking validation
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {
            $user = User::where('id', $request->input('id'))->update(array('password' => Hash::make(strip_tags($request->password))));
            if ($user) {
                // Activity logs
            $causer = auth()->user();
            $log_name = 'patient';
            $operation = 'patient_password_update';
            $description = 'Password update in patient ' . $user?->name;
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
            ];
            store_activity_log($causer, $user, $custom_properties, $log_name, $description);
                $data = array(
                    'status' => true,
                    'message' => 'Password updated successfully.',
                    "response" => '',
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Patient Appointment History
     * @param type $id
     * @return type
     */
    public function patientAppointmentHistory($id) {
        try {
            $query = Appointment::query()->with('doctor.doctor.specialization', 'patient', 'bill')->where('patient_id', $id);
            if (auth()->user()->hasRole('Doctor')) {
                $query->where('doctor_id', auth()->user()->id);
            }
            $appointments = $query->paginate(10);
            if ($appointments) {
                $data = array(
                    'status' => true,
                    'message' => 'Patient appointment history fetched successfully.',
                    "response" => $appointments,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Patient Bill History
     * @param type $id
     * @return type
     */
    public function patientBillHistory($id) {
        try {
            $query = Bill::query()->with('doctor.specialization', 'doctor.user', 'patient.user')->with(['appointment' => function ($query) {
                            $query->whereNotNull('appointment_id');
                        }])->where('patient_id', $id);
            if (auth()->user()->hasRole('Doctor')) {
                $query->where('doctor_id', auth()->user()->id);
            }
            $bills = $query->paginate(10);
            if ($bills) {
                $data = array(
                    'status' => true,
                    'message' => 'Patient bills fetched successfully.',
                    "response" => $bills,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * city
     * @return type
     */
    public function city() {
        return response()->json(City::where('state_id', 33)->get(['id', 'name']), 200);
    }

    /**
     * Patient Prescription History
     * @param type $id
     * @return type
     */
    public function patientPrescriptionHistory($id) {
        try {
            $query = Prescription::query()->with('exercise', 'prescibeMedicine', 'patient.patient.medicalHistory', 'patient.patient', 'doctor.doctor.specialization', 'appointment')->where('patient_id', $id);
            if (auth()->user()->hasRole('Doctor')) {
                $query->where('doctor_id', auth()->user()->id);
            }
            $query->orderBy('created_at', 'DESC');
            $prescriptions = $query->paginate(10);
            if ($prescriptions) {
                $data = array(
                    'status' => true,
                    'message' => 'Patient Prescription history fetched successfully.',
                    "response" => $prescriptions,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Patient Document History
     * @param type $id
     * @return type
     */
    public function patientDocumentHistory($id) {
        try {
            $query = Document::query()->with('patient.patient', 'doctor.doctor.specialization')->where('patient_id', $id);
            if (auth()->user()->hasRole('Doctor')) {
                $query->where('doctor_id', auth()->user()->id);
            }
            $query->orderBy('created_at', 'DESC');
            $documents = $query->paginate(10);
            if ($documents) {
                $data = array(
                    'status' => true,
                    'message' => 'Patient Document history fetched successfully.',
                    "response" => $documents,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return response($data);
    }
    
    public function import(Request $request) { 
          $rules = [
            'file' => 'required|mimes:xlsx|max:5120'
          ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        
        try {
            Excel::import(new PatientsImport, $request->file('file')->store('temp'));
        return response()->json([ 
            'status' => true, 
            'message' => 'Patients imported successfully!' 
            ], 200);  
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $e->getMessage()
                            ), 500);
        }
    }

}
