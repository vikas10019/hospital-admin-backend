<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patient;
use App\Models\User;
use App\Models\Doctor;
use App\Models\Appointment;
use App\Models\Medicine;
use App\Models\DrugType;
use App\Models\PrExercise;
use App\Models\Symptom;
use App\Models\Investigation;
use App\Models\Test;
use App\Models\PrMedicalHistory;
use App\Models\Prescription;
use App\Models\PrescribeMedicine;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Layout;
use App\Models\Setting;

class PrescriptionController extends Controller {

    /**
     * Display a listing of the prescriptions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Prescription::query()->with('exercise', 'prescibeMedicine', 'patient.patient.medicalHistory', 'patient.patient', 'doctor.doctor.specialization', 'appointment');
        if (auth()->user()->hasRole('Doctor')) {
            $query->where('doctor_id', auth()->user()->id);
        }
        if (isset($request->search)) {
            $query->where(function ($query1) use ($request) {
                $query1->whereHas('doctor', function ($query2) use ($request) {
                            $query2->where('name', 'like', '%' . $request->search . '%');
                        })
                        ->orWhereHas('patient', function ($query3) use ($request) {
                            $query3->where('name', 'like', '%' . $request->search . '%')
                            ->orWhereHas('patient', function ($query3) use ($request) {
                                $query3->where('patient_id', 'like', '%' . $request->search . '%');
                            });
                        });
            });
        }
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') != 'doctor.name' && request('field') != 'patient.name') {
                $query->orderBy(request('field'), request('sortOrder'));
            }
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        if ($request->has(['field', 'sortOrder']) && $request->field != null) {
            if (request('field') == 'doctor.name' || request('field') == 'patient.name') {
                if (strtoupper(request('sortOrder')) == "DESC") {
                    $prescription = $query->get()->sortByDesc(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                } else {
                    $prescription = $query->get()->sortBy(request('field'), SORT_NATURAL | SORT_FLAG_CASE);
                }
                // Convert JSON data to associative array
                $arrayData = json_decode($prescription, true);

                // Convert the associative array into the desired format
                $formattedArray = array_values($arrayData);

                // Output the formatted array as JSON
                $prescription1 = json_encode($formattedArray, JSON_PRETTY_PRINT);
                $prescriptions = paginate(json_decode($prescription1, true), $length, null, ['path' => $request->url(), 'query' => $request->query()]);
            }
        } else {
            $prescriptions = $query->paginate($length);
        }
        if (isset($prescriptions)) {
            return $prescriptions;
        } else {
            return $prescriptions = $query->paginate($length);
        }      
    }

    /**
     * Show the form for creating a new prescriptions.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $drugtypes = DrugType::all();
        return response()->json($drugtypes);
    }

    /**
     * Store a newly created prescriptions in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //create new prescriptions
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'age' => 'required',
                    'phone' => 'required',
                    'height' => 'nullable|integer|digits_between:2,3',
                    'weight' => 'nullable|integer|digits_between:2,3',
                    'bp_high' => 'nullable|integer|digits_between:2,3',
                    'bp_low' => 'nullable|integer|digits_between:2,3',
                    'pulse_rate' => 'nullable|integer|digits_between:2,3',
                    'diet_advice' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i',
                    'clinic_diagnosis' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i',
                    'symptoms' => 'required',
                    'medicineForm.*.drugId' => 'required',
                    'medicineForm.*.quantity' => 'required|numeric',
                    'medicineForm.*.unit' => 'required|alpha_dash',
                    'medicineForm.*.days' => 'required|integer',
                    'medicineForm.*.instruction' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i',
                    'medicineForm.*.frequency_day' => 'required',
                    'medicineForm.*.frequency_type' => 'required',
                    'medicineForm.*.frequency_intake' => 'required',
                    'medicineForm.*.frequency_time' => 'required',
                    'exerciseForm.*.exercise_time' => 'nullable|integer',
                    'exerciseForm.*.exercise_name' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i|max:50',
                    'exerciseForm.*.exercise_notes' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i|max:255',
                        ], [
                    'medicineForm.*.drugId.required' => 'The Drug type field  is required.',
                    'medicineForm.*.days.required' => 'This field  is required.',
                    'medicineForm.*.frequency_type.required' => 'The Frequency Type field is required.',
                    'medicineForm.*.frequency_time.required' => 'The Frequency Time field is required.',
                    'medicineForm.*.frequency_intake.required' => 'The Frequency Intake field is required.',
                    'medicineForm.*.frequency_day.required' => 'The Frequency Day field is required.',
                    'medicineForm.*.days.integer' => 'This field  is not valid.',
                    'exerciseForm.*.exercise_time.integer' => 'This field  is not valid.',
                    'exerciseForm.*.exercise_name.alpha_dash' => 'This field  must only contain letters, numbers, dashes and underscores.',
                    'exerciseForm.*.exercise_name.max' => 'This field  must only contain 50 characters.',
                    'exerciseForm.*.exercise_notes.regex' => 'This field  is not valid.',
                    'medicineForm.*.note.regex' => 'This field  is not valid.',
                    'medicineForm.*.quantity.required' => 'The medicine quantity field is required',
                    'medicineForm.*.unit.required' => 'The medicine unit field is required',
        ]);
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }
        if (isset($request->patient_id)) {
            $patient = Patient::with('user', 'medicalHistory')->where('user_id',$request->patient_id)->first();
            if (isset($patient?->medicalHistory)) {
                $patient_history = PrMedicalHistory::find($patient->medicalHistory->id);
            } else {
                $patient_history = new PrMedicalHistory();
            }
            $patient_history->patient_id = $patient->id;
            $patient_history->height = $request->height;
            $patient_history->weight = $request->weight;
            $patient_history->bp_high = $request->bp_high;
            $patient_history->bp_low = $request->bp_low;
            $patient_history->pulse_rate = $request->pulse_rate;
            $invest = array();
            if (isset($request->genetic_history)) {
                foreach ($request->genetic_history as $key => $geneticHistory) {
                    if (!isset($geneticHistory['id'])) {
                        $investigation = Investigation::firstOrCreate([
                                    'name' => $geneticHistory['value'],
                        ]);
                        $invest[$key]['id'] = $investigation->id;
                    } else {
                        $invest[$key]['id'] = $geneticHistory['id'];
                    }
                    $invest[$key]['value'] = $geneticHistory['value'];
                    $invest[$key]['label'] = ucwords($geneticHistory['value']);
                }
            }
            $patient_history->genetic_history = json_encode($invest);
            $patient_history->pulse_rate = $request->pulse_rate;
            $patient_history->save();
        }

        $prescription = new Prescription();
        $prescription->patient_id = $patient->user_id;
        $prescription->doctor_id = $request->doctor_id;
        $prescription->appointment_id = $request->appointment_id;
        $sympt = array();
        if (isset($request->symptoms)) {
            foreach ($request->symptoms as $key => $symptom) {
                if (!isset($symptom['id'])) {
                    $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                    $sympt[$key]['id'] = $symptom_new->id;
                } else {
                    $sympt[$key]['id'] = $symptom['id'];
                }
                $sympt[$key]['value'] = $symptom['value'];
                $sympt[$key]['label'] = ucwords($symptom['value']);
            }
        }
        $prescription->symptoms = json_encode($sympt);
        $prescription->clinic_diagnosis = $request->clinic_diagnosis;
        $tests = array();
        if (isset($request->prescribed_test)) {
            foreach ($request->prescribed_test as $key => $test) {
                if (!isset($test['id'])) {
                    $test_new = Test::firstOrCreate(['name' => $test['value']]);
                    $tests[$key]['id'] = $test_new->id;
                } else {
                    $tests[$key]['id'] = $test['id'];
                }
                $tests[$key]['value'] = $test['value'];
                $tests[$key]['label'] = ucwords($test['value']);
            }
        }
        $prescription->test_prescribed = json_encode($tests);
        $prescription->diet_advice = $request->diet_advice;
        $prescription->referred_by = $request->referred_by;
        $prescription->save();
        if (isset($request->medicineForm)) {
            foreach ($request->medicineForm as $key => $prescribes) {
                $prescribe = new PrescribeMedicine();
                $prescribe->prescription_id = $prescription->id;
                if (isset($prescribes['medicineId'])) {
                    $prescribe->medicine_id = $prescribes['medicineId'];
                } else {
                    $medicine = Medicine::firstOrCreate([
                                'name' => $prescribes['medicine'],
                                'drug_type_id' => $prescribes['drugId'],
                                'quantity' => $prescribes['quantity'],
                                'unit' => $prescribes['unit']
                    ]);
                    $prescribe->medicine_id = $medicine->id;
                }
                $prescribe->drug_type_id = $prescribes['drugId'];
                $prescribe->note = $prescribes['instruction'];
                $prescribe->days = $prescribes['days'];
                $prescribe->frequency_time = isset($prescribes['frequency_time']) ? $prescribes['frequency_time'] : null;
                $prescribe->frequency_type = isset($prescribes['frequency_type']) ? $prescribes['frequency_type'] : null;
                $prescribe->frequency_intake = isset($prescribes['frequency_intake']) ? $prescribes['frequency_intake'] : null;
                $prescribe->frequency_day = isset($prescribes['frequency_day']) ? $prescribes['frequency_day'] : null;
                $prescribe->save();
            }
        }

        if (isset($request->exerciseForm)) {
            foreach ($request->exerciseForm as $key => $exercise) {
                if ($exercise['exercise_name'] != null) {
                    $exercises = new PrExercise();
                    $exercises->prescription_id = $prescription->id;
                    $exercises->name = $exercise['exercise_name'];
                    $exercises->turns = $exercise['exercise_time'];
                    $exercises->notes = $exercise['exercise_notes'];
                    $exercises->save();
                }
            }
        }

        return response()->json(array(
                    'status' => true,
                    'message' => 'Prescription Store successfully.',
                    'response' => $prescription,
        ));
    }

    /**
     * Display the patient Search resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function patientSearch($id) {
        $appointment = Appointment::with('patient.patient.medicalHistory', 'doctor')->find($id);
        //    $getUser = User::with('patient.medicalHistory')->where('mobile', 'LIKE', "%$mobile%")->role('patient')->first();
        $user = array();
        $user['name'] = $appointment->patient->name;
        $user['mobile'] = $appointment->patient->mobile;
        $age = optional($appointment->patient->patient)->age;
        $user['age'] = $age;
        switch ($appointment->patient->gender) {
            case 1: $gender = 'male';
                break;
            case 0: $gender = 'female';
                break;
            case 3: $gender = 'transgender';
                break;
        }
        $user['gender'] = $gender;
        $user['height'] = optional($appointment->patient->patient->medicalHistory)->height;
        $user['weight'] = optional($appointment->patient->patient->medicalHistory)->weight;
        $user['bp_high'] = optional($appointment->patient->patient->medicalHistory)->bp_high;
        $user['bp_low'] = optional($appointment->patient->patient->medicalHistory)->bp_low;
        $user['pulse_rate'] = optional($appointment->patient->patient->medicalHistory)->pulse_rate;
        $user['patient_id_show'] = optional($appointment->patient->patient)->patient_id;
        $user['patient_id'] = optional($appointment->patient)->id;
        $user['blood_group'] = optional($appointment->patient->patient)->blood_group;
        $user['address'] = $appointment->patient->address;
        $user['doctor_id'] = optional($appointment->doctor)->id;
        $user['doctor_name'] = optional($appointment->doctor)->name;
        $user['doctor_fee'] = optional($appointment->doctor->doctor)->price;
        $user['appointment_id'] = $appointment->id;
        $user['appointment_id_show'] = $appointment->appointment_id;
        $user['genetic_history'] = json_decode(optional($appointment->patient->patient->medicalHistory)->genetic_history);
        $user['symptoms'] = json_decode(optional($appointment)->problems);
        return response()->json($user, 200);
    }

    /**
     * Show the form for editing the specified prescriptions.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $prescription = Prescription::with('exercise', 'prescibeMedicine.medicine', 'prescibeMedicine.drugType')->find($id);
        $patient = Patient::with('medicalHistory')->where('user_id', $prescription->patient_id)->first();
        return response()->json(['prescription' => $prescription, 'patient' => $patient]);
    }

    /**
     * Update the prescription in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // update prescription
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'age' => 'required',
                    'phone' => 'required',
                    'height' => 'nullable|integer|digits_between:2,3',
                    'weight' => 'nullable|integer|digits_between:2,3',
                    'bp_high' => 'nullable|integer|digits_between:2,3',
                    'bp_low' => 'nullable|integer|digits_between:2,3',
                    'pulse_rate' => 'nullable|integer|digits_between:2,3',
                    'diet_advice' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i',
                    'clinic_diagnosis' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i',
                    'symptoms' => 'required',
                    'medicineForm.*.drugId' => 'required',
                    'medicineForm.*.quantity' => 'required|numeric',
                    'medicineForm.*.unit' => 'required|alpha_dash',
                    'medicineForm.*.days' => 'required|integer',
                    'medicineForm.*.instruction' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i',
                    'medicineForm.*.frequency_day' => 'required',
                    'medicineForm.*.frequency_type' => 'required',
                    'medicineForm.*.frequency_intake' => 'required',
                    'medicineForm.*.frequency_time' => 'required',
                    'exerciseForm.*.exercise_time' => 'nullable|integer',
                    'exerciseForm.*.exercise_name' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i|max:50',
                    'exerciseForm.*.exercise_notes' => 'nullable|regex:/^[a-z\s\_0-9\/\.\,\&\(\)\-]+$/i|max:255',
                        ], [
                    'medicineForm.*.drugId.required' => 'The Drug type field  is required.',
                    'medicineForm.*.days.required' => 'This field  is required.',
                    'medicineForm.*.frequency_type.required' => 'The Frequency Type field is required.',
                    'medicineForm.*.frequency_time.required' => 'The Frequency Time field is required.',
                    'medicineForm.*.frequency_intake.required' => 'The Frequency Intake field is required.',
                    'medicineForm.*.frequency_day.required' => 'The Frequency Day field is required.',
                    'medicineForm.*.days.integer' => 'This field  is not valid.',
                    'exerciseForm.*.exercise_time.integer' => 'This field  is not valid.',
                    'exerciseForm.*.exercise_name.alpha_dash' => 'This field  must only contain letters, numbers, dashes and underscores.',
                    'exerciseForm.*.exercise_name.max' => 'This field  must only contain 50 characters.',
                    'exerciseForm.*.exercise_notes.regex' => 'This field  is not valid.',
                    'medicineForm.*.note.regex' => 'This field  is not valid.',
                    'medicineForm.*.quantity.required' => 'The medicine quantity field is required',
                    'medicineForm.*.unit.required' => 'The medicine unit field is required',
                        ]
        );
        if ($validator->fails()) {
            return response()->json(array(
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 422);
        }

        if (isset($request->patient_id)) {
            $patient = Patient::with('user', 'medicalHistory')->where('user_id',$request->patient_id)->first();
            if (isset($patient?->medicalHistory)) {
                $patient_history = PrMedicalHistory::find($patient->medicalHistory->id);
            } else {
                $patient_history = new PrMedicalHistory();
            }
            $patient_history->patient_id = $patient->id;
            $patient_history->height = $request->height;
            $patient_history->weight = $request->weight;
            $patient_history->bp_high = $request->bp_high;
            $patient_history->bp_low = $request->bp_low;
//            $genetic_history = json_encode($request->genetic_history);
            $invest = array();
            if (isset($request->genetic_history)) {
                foreach ($request->genetic_history as $key => $geneticHistory) {
                    if (!isset($geneticHistory['id'])) {
                        $investigation = Investigation::firstOrCreate([
                                    'name' => $geneticHistory['value'],
                        ]);
                        $invest[$key]['id'] = $investigation->id;
                    } else {
                        $invest[$key]['id'] = $geneticHistory['id'];
                    }
                    $invest[$key]['value'] = $geneticHistory['value'];
                    $invest[$key]['label'] = ucwords($geneticHistory['value']);
                }
            }
            $patient_history->genetic_history = json_encode($invest);
            $patient_history->pulse_rate = $request->pulse_rate;
            $patient_history->save();
        }

        $prescription = Prescription::with('exercise', 'prescibeMedicine')->find($id);
        $prescription->patient_id = $patient->user_id;
        $prescription->doctor_id = $request->doctor_id;
        $prescription->appointment_id = $request->appointment_id;
        $sympt = array();
        if (isset($request->symptoms)) {
            foreach ($request->symptoms as $key => $symptom) {
                if (!isset($symptom['id'])) {
                    $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                    $sympt[$key]['id'] = $symptom_new->id;
                } else {
                    $sympt[$key]['id'] = $symptom['id'];
                }
                $sympt[$key]['value'] = $symptom['value'];
                $sympt[$key]['label'] = ucwords($symptom['value']);
            }
        }
        $prescription->symptoms = json_encode($sympt);
        $prescription->clinic_diagnosis = $request->clinic_diagnosis;
        $tests = array();
        if (isset($request->prescribed_test)) {
            foreach ($request->prescribed_test as $key => $test) {
                if (!isset($test['id'])) {
                    $test_new = Test::firstOrCreate(['name' => $test['value']]);
                    $tests[$key]['id'] = $test_new->id;
                } else {
                    $tests[$key]['id'] = $test['id'];
                }
                $tests[$key]['value'] = $test['value'];
                $tests[$key]['label'] = ucwords($test['value']);
            }
            $prescription->test_prescribed = json_encode($tests);
        }
        $prescription->diet_advice = $request->diet_advice;
        $prescription->referred_by = $request->referred_by;
        $prescription->save();

        PrescribeMedicine::where('prescription_id', $prescription->id)->whereNotIn('id', $request->prescribe_medicine_id)->delete();
        if (isset($request->medicineForm)) {
            foreach ($request->medicineForm as $key => $prescribes) {
                if (isset($request->prescribe_medicine_id[$key])) {
                    $prescribe = PrescribeMedicine::find($request->prescribe_medicine_id[$key]);
                } else {
                    $prescribe = new PrescribeMedicine();
                }
                $prescribe->prescription_id = $prescription->id;
                if (isset($prescribes['medicineId'])) {
                    $prescribe->medicine_id = $prescribes['medicineId'];
                } else {
                    $medicine = Medicine::firstOrCreate([
                                'name' => $prescribes['medicine'],
                                'drug_type_id' => $prescribes['drugId'],
                                'quantity' => $prescribes['quantity'],
                                'unit' => $prescribes['unit']
                    ]);
                    $prescribe->medicine_id = $medicine->id;
                }
//                $prescribe->medicine_id = $prescribes['medicine_id'];
                $prescribe->drug_type_id = $prescribes['drugId'];
                $prescribe->note = $prescribes['instruction'];
                $prescribe->days = $prescribes['days'];
                $prescribe->frequency_time = isset($prescribes['frequency_time']) ? $prescribes['frequency_time'] : null;
                $prescribe->frequency_type = isset($prescribes['frequency_type']) ? $prescribes['frequency_type'] : null;
                $prescribe->frequency_intake = isset($prescribes['frequency_intake']) ? $prescribes['frequency_intake'] : null;
                $prescribe->frequency_day = isset($prescribes['frequency_day']) ? $prescribes['frequency_day'] : null;
                $prescribe->save();
            }
        }

        PrExercise::where('prescription_id', $prescription->id)->whereNotIn('id', $request->pr_exercises_id)->delete();
        if (isset($request->exerciseForm)) {
            foreach ($request->exerciseForm as $key => $exercise) {
                if ($exercise['exercise_name'] != null) {
                    if (isset($request->pr_exercises_id[$key])) {
                        $exercises = PrExercise::find($request->pr_exercises_id[$key]);
                    } else {
                        $exercises = new PrExercise();
                    }

                    $exercises->prescription_id = $prescription->id;
                    $exercises->name = $exercise['exercise_name'];
                    $exercises->turns = $exercise['exercise_time'];
                    $exercises->notes = $exercise['exercise_notes'];
                    $exercises->save();
                }
            }
        }

        return response()->json(array(
                    'status' => true,
                    'message' => 'Prescription update successfully.',
                    "response" => $prescription,
        ));
    }

    /**
     * Remove the specified Prescription from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
        $prescription = Prescription::doesntHave('appointment')->with('exercise', 'prescibeMedicine')->find($id);
        if(!empty($prescription)){
        $prescription->prescibeMedicine()->delete();
        $prescription->exercise()->delete();
          if($prescription->delete()){
               return response()->json([
                                'status' => true,
                                'message' => 'Prescription deleted successfully.',
                                    ], 200);
            }   
        }else{
               return response()->json([
                                'status' => false,
                                'message' => 'The prescription is associated with an appointment and cannot be deleted.',
                                    ], 400);
        }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        
    }

    /**
     * list of all Symptoms
     * @return type
     */
    public function symptoms() {
        $symptoms = Symptom::all();
        $symptom = array();
        if ($symptoms->isNotEmpty()) {
            foreach ($symptoms as $key => $sympt) {
                $symptom[$key]['id'] = $sympt->id;
                $symptom[$key]['value'] = $sympt->name;
                $symptom[$key]['label'] = ucwords($sympt->name);
            }
        }
        return response()->json($symptom);
    }

    /**
     * add Symptoms
     * @param Request $request
     */
    public function addSymptoms(Request $request) {
        Symptom::firstOrCreate([
            'name' => $request->name,
        ]);
    }

    /**
     * list of all Investigation
     * @return type
     */
    public function investigation() {
        $investigations = Investigation::all();
        $investigation_list = array();
        if ($investigations->isNotEmpty()) {
            foreach ($investigations as $key => $investigation) {
                $investigation_list[$key]['id'] = $investigation->id;
                $investigation_list[$key]['value'] = $investigation->name;
                $investigation_list[$key]['label'] = ucwords($investigation->name);
            }
        }
        return response()->json($investigation_list);
    }

    /**
     * add Investigation
     * @param Request $request
     */
    public function addInvestigation(Request $request) {
        Investigation::firstOrCreate([
            'name' => $request->name,
        ]);
    }

    /**
     * list of all test
     * @return type
     */
    public function test() {
        $tests = Test::all();
        $test_list = array();
        if ($tests->isNotEmpty()) {
            foreach ($tests as $key => $test) {
                $test_list[$key]['id'] = $test->id;
                $test_list[$key]['value'] = $test->name;
                $test_list[$key]['label'] = ucwords($test->name);
            }
        }
        return response()->json($test_list);
    }

    /**
     * add Test
     * @param Request $request
     */
    public function addTest(Request $request) {
        Test::firstOrCreate([
            'name' => $request->name,
        ]);
    }

    /**
     * Prescription show
     * @param type $id
     * @return type
     */
    public function show($id) {
        $prescription = Prescription::with('exercise', 'prescibeMedicine.medicine', 'prescibeMedicine.drugType', 'patient.patient.medicalHistory', 'patient.patient', 'doctor.doctor', 'doctor.doctor.specialization', 'doctor.schedule', 'appointment','doctor.doctorSpecialityServices.specialityServices.doctorService','doctor.doctorExperience','doctor.doctorEducation')->find($id);
        $layout = Layout::where('doctor_id',$prescription->doctor->id)->first();
        $setting = Setting::first();
        $base64Image ="NULL";
        $base64Image1 ="NULL";
        $base64Image2 ="NULL";
        if($setting?->bill_logo){
        $path = public_path('storage/'.$setting?->bill_logo);
        // Read the file contents
        $imageData = file_get_contents($path);
        // Convert image data to Base64
        $base64 = base64_encode($imageData);
        // Get the mime type of the image (e.g., image/jpeg, image/png)
        $mimeType = mime_content_type($path);
        // Create the base64 data URL (optional)
        $base64Image = $setting?->bill_logo ? "data:" . $mimeType . ";base64," . $base64 :"NULL";
        }
         if($setting?->prescription_logo){
        $path1 = public_path('storage/'.$setting?->prescription_logo);
        $imageData1 = file_get_contents($path1);
        $base641 = base64_encode($imageData1);
        $mimeType1 = mime_content_type($path1);
        $base64Image1 = $setting?->prescription_logo ? "data:" . $mimeType1 . ";base64," . $base641:"NULL";
        }
         if($prescription?->doctor?->doctor?->clinic_logo){
        $path2 = public_path('storage/'.$prescription?->doctor?->doctor?->clinic_logo);
        $imageData2 = file_get_contents($path2);
        $base642 = base64_encode($imageData2);
        $mimeType2 = mime_content_type($path2);
        $base64Image2 = $prescription?->doctor?->doctor?->clinic_logo ? "data:" . $mimeType2 . ";base64," . $base642:"NULL";
        }
     

        return response()->json(['prescription'=>$prescription,
        'layout'=>$layout,
        'setting'=>$setting, 
        'bill_logo' => $base64Image, 
        'prescription_logo' => $base64Image1, 
        'clinic_logo' => $base64Image2]);
    }

}
