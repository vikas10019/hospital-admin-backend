<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\FormLead;
use App\Models\Lead;
use App\Models\Source;
use App\Models\ServiceType;
use App\Models\User;
use App\Models\Industry;
use App\Models\LeadSource;
use App\Models\LeadStatus;
use App\Models\Note;
use App\Models\Attachment;
use App\Models\FollowUp;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Session,
    Response;
use Spatie\Activitylog\Models\Activity;
use App\Models\Campaign;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Patient;

class LeadCollectController extends Controller {

    /**
     * Show the form for creating a new lead.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $users = User::role(['Doctor', 'Staff'])->where('status', 1)->orWhere(function ($query) {
                    $query->where('id', 1)
                            ->where('status', 0);
                })->get();
        $industries = Industry::all();
        $lead_sources = LeadSource::where('status', 1)->get();
        $lead_statuses = LeadStatus::all();
        $service_types = ServiceType::where('status', 1)->get();
        $countries = Country::all();
        $state = State::all();
        $city = City::all();
        $auth = auth()->user()->id;
        return response()->json(['message' => 'Leads data fetch successfully.', 'response' => ['lead_statuses' => $lead_statuses, 'users' => $users, 'lead_sources' => $lead_sources, 'service_types' => $service_types, 'industries' => $industries, 'countries' => $countries, 'state' => $state, 'city' => $city, 'auth_user' => $auth], 'status' => true], 200);
    }

    /**
     * Store a newly created lead in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
                    'company' => 'nullable|regex:/^[a-z0-9\s\-\.\&]+$/i|max:255',
                    'title' => 'nullable|max:255',
                    'first_name' => 'required|regex:/^[a-z\s\-\.]+$/i|max:255',
                    'last_name' => 'nullable|regex:/^[a-z\s\-\.]+$/i|max:255',
                    'email' => 'required_without:phone|nullable|email:filter',
                    'secondary_email' => 'nullable|email:filter',
                    'phone' => 'required_without:email|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:30',
                    'mobile' => 'nullable|regex:/^([0-9]*)$/|digits:10',
                    'whatsapp' => 'nullable|regex:/^([0-9]*)$/|digits:10',
                    'website' => 'nullable|url',
                    'zip_code' => 'nullable', //|regex:/^[0-9]{6}$/
                    'street1' => 'nullable|max:255',
                    'street2' => 'nullable|max:255',
                    'lead_title' => 'nullable|regex:/^[a-z0-9\s\-\.\&]+$/i|max:255',
                    'skype_id' => 'nullable|regex:/^[a-z][a-z0-9\.,\-_]{5,31}$/i',
                    'twitter' => 'nullable|regex:/^[A-Za-z0-9_]{1,15}$/',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            $lead = new Lead();
            $lead->company = $request->company;
            $lead->title = $request->title;
            $lead->first_name = $request->first_name;
            $lead->last_name = $request->last_name;
            $lead->lead_title = $request->lead_title;
            $lead->lead_owner_id = 1;
            $lead->email = $request->email;
            $lead->phone = '+91' . $request->phone;
            $lead->mobile = $request->mobile;
            $lead->whatsapp = $request->whatsapp;
            $lead->lead_source_id = 18;
            $lead->industry_id = $request->industry_id;
            $lead->website = $request->website;
            $lead->lead_status_id = $request->lead_status_id;
            $lead->assigned_id = $request->assigned_id;
            $lead->skype_id = $request->skype_id;
            $lead->secondary_email = $request->secondary_email;
            $lead->twitter = $request->twitter;
            $lead->street1 = $request->street1;
            $lead->street2 = $request->street2;
            $lead->country_id = $request->country_id;
            $lead->state_id = $request->state;
            $lead->city_id = $request->city;
            $lead->zip_code = $request->zip_code;
            $lead->description = $request->description;
            $lead->is_public = 1;
            $lead->currency = $request->currency;
            $lead->service_type_id = $request->service_type_id;
            $lead->campaign_name = $request->campaign_name;

            if ($request->campaign_name) {
                if (!Campaign::where("name", $request->campaign_name)->exists()) {
                    $campaign = new Campaign();
                    $campaign->name = $request->campaign_name;
                    $campaign->save();
                }
            }

            $saved = $lead->save();

            $path = storage_path('app/public/leadsFile/' . $lead->id);
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            if ($request->hasfile('lead_image')) {
                $file = $request->file('lead_image');
                $fileName = time() . '.' . $request->lead_image->extension();
                $save = $request->lead_image->move($path, $fileName);
                $lead->lead_image = 'leadsFile/' . $lead->id . '/' . $fileName;
                $lead->save();
            }
            // Activity logs
            $causer = null;
            $log_name = 'lead';
            $operation = 'lead_create_from_out_source';
            $description = 'lead of ' . $lead?->full_name . ($lead?->company ? " ({$lead->company})" : "");
            $custom_properties = [
                'application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer,
            ];
            store_activity_log($causer, $lead, $custom_properties, $log_name, $description);
            if ($saved) {
                $form_lead = new FormLead();
                $form_lead->lead_id = $lead->id;
                $form_lead->contact_form = isset($request->contact_form) ? $request->contact_form : 'Enquire Now';
                $form_lead->ip_address = $request->client_ip;
                $form_lead->page_url = $request->page_url;
                $form_lead->device_type = $request->device_type;
                $form_lead->subject = !empty($request->treatment_type) ? $request->treatment_type[0] : '';
                $form_lead->form_data = json_encode($request->form_data, true);
                $form_lead->save();
                return response()->json(['status' => true, 'message' => 'Lead collected successfully', 'response' => $lead], 201);
            } else {
                return response()->json(['status' => false, 'error' => 'Something went wrong!'], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Display a listing of the lead.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        try {
            $length = isset($request->length) ? $request->length : 10;
            $users = User::role(['Doctor', 'Staff'])->where('status', 1)->orWhere(function ($query) {
                        $query->where('id', 1)
                                ->where('status', 0);
                    })->get();
            $lead_statuses = LeadStatus::all();
            $lead_sources = LeadSource::where('status', 1)->get();
            $service_types = ServiceType::where('status', 1)->get();
            $sources = Source::all();
            $query = Lead::query()->with('leadOwner', 'leadSource', 'industry', 'leadStatus', 'assigned', 'latestFollowUp');
            if ($request->lead_status_id != '') {
                $query->where('lead_status_id', $request->lead_status_id);
            }
            if ($request->assigned_id != '') {
                $query->where('assigned_id', $request->assigned_id);
            }
            if ($request->lead_source_id != '') {
                $query->where('lead_source_id', $request->lead_source_id);
            }
            if ($request->service_type_id != '') {
                $query->where('service_type_id', $request->service_type_id);
            }
            if ($request->campaign_name != '') {
                $query->where('campaign_name', $request->campaign_name);
            }
            if ($request->follow_date != '') {
                $follow_date = explode('-', $request->follow_date);
                $start1 = Carbon::createFromFormat('d/m/Y', trim($follow_date[0]))->format('Y-m-d');
                $end1 = Carbon::createFromFormat('d/m/Y', trim($follow_date[1]))->format('Y-m-d');
                $query->whereHas('latestFollowUp', function ($query) use ($start1, $end1) {
                    $query->whereBetween('next_follow_up_date', [$start1, $end1])->latest('next_follow_up_date');
                });
            }
            if ($request->daterange != '') {
                $daterange = explode('-', $request->daterange);
                $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
                $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
                $query->whereBetween('created_at', [$start, $end]);
            }
            if (auth()->user()->hasAnyRole(1)) {
                $lead = $query->latest()->get()->sortByDesc('latestFollowUp.next_follow_up_date');
            } else {
                $lead = $query->where(function ($query) {
                            $query->where('is_public', 1);
                            $query->orWhere('assigned_id', auth()->user()->id);
                            $query->where('is_public', 0);
                        })->latest()->get()->sortByDesc('latestFollowUp.next_follow_up_date');
            }   
            // Convert JSON data to associative array
            $arrayData = json_decode($lead, true);

            // Convert the associative array into the desired format
            $formattedArray = array_values($arrayData);

            // Output the formatted array as JSON
            $lead1 = json_encode($formattedArray, JSON_PRETTY_PRINT);
            $leads = paginate(json_decode($lead1, true), $length, null, ['path' => $request->url(), 'query' => $request->query()]);

            return response()->json(['message' => 'Leads data fetch successfully.', 'response' => ['leads' => $leads, 'lead_statuses' => $lead_statuses, 'users' => $users, 'lead_sources' => $lead_sources, 'sources' => $sources, 'service_types' => $service_types], 'status' => true], 200);
            
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
    }

    /**
     * Show the form for editing the specified lead.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        try {
            $lead = Lead::find($id);
            $users = User::role(['Doctor', 'Staff'])->where('status', 1)->orWhere(function ($query) {
                        $query->where('id', 1)
                                ->where('status', 0);
                    })->get();
            $industries = Industry::all();
            $lead_sources = LeadSource::where('status', 1)->get();
            $lead_statuses = LeadStatus::all();
            $service_types = ServiceType::where('status', 1)->get();
            $countries = Country::all();
            $state = State::all();
            $city = City::all();
            $auth = auth()->user()->id;
            if (!is_null($lead?->country_id)) {
                $state = State::where('country_id', $lead->country_id)->get();
            }
            if (!is_null($lead?->state_id)) {
                $city = City::where('state_id', $lead->state_id)->get();
            }
            if ($lead) {
                return response()->json(['message' => 'Lead data fetch successfully.', 'response' => ['users' => $users, 'industries' => $industries, 'lead_sources' => $lead_sources, 'lead_statuses' => $lead_statuses, 'countries' => $countries, 'state' => $state, 'city' => $city, 'lead' => $lead, 'service_types' => $service_types, 'auth_user' => $auth], 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
        return view('leads.edit', compact('users', 'industries', 'lead_sources', 'lead_statuses', 'countries', 'state', 'city', 'lead', 'service_types'));
    }

    /**
     * Display the specified lead.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $lead = Lead::with('leadOwner', 'followUps', 'attachments', 'notes', 'formLead', 'leadSource', 'leadStatus', 'assigned', 'country', 'state', 'city', 'serviceType')->find($id);
            $users = User::role(['Doctor', 'Staff'])->where('status', 1)->orWhere(function ($query) {
                        $query->where('id', 1)
                                ->where('status', 0);
                    })->get();
            $industries = Industry::all();
            $lead_sources = LeadSource::where('status', 1)->get();
            $lead_statuses = LeadStatus::all();
            $service_types = ServiceType::where('status', 1)->get();
            $sources = Source::all();
            $countries = Country::all();
            $state = State::all();
            $city = City::all();
            if (!is_null($lead?->country_id)) {
                $state = State::where('country_id', $lead->country_id)->get();
            }
            if (!is_null($lead?->state_id)) {
                $city = City::where('state_id', $lead->state_id)->get();
            }
            $lead_followUps = $lead->followUps()->pluck('id')->toArray();
            $lead_attachments = $lead->attachments()->pluck('id')->toArray();
            $lead_notes = $lead->notes()->pluck('id')->toArray();
            $model_name[] = 'App\Models\Lead';
            $model_name[] = 'App\Models\FollowUp';
            $model_name[] = 'App\Models\Attachment';
            $model_name[] = 'App\Models\Note';
            $activities = Activity::whereIn('subject_type', $model_name)->where(function ($query) use ($id, $lead_followUps, $lead_notes, $lead_attachments) {
                        $query->where('subject_id', $id)
                                ->orWhereIn('subject_id', $lead_followUps)
                                ->orWhereIn('subject_id', $lead_attachments)
                                ->orWhereIn('subject_id', $lead_notes);
                    })->orderBy('created_at', 'desc')->get();

            if ($lead) {
                return response()->json(['message' => 'Lead data fetch successfully.', 'response' => ['users' => $users, 'industries' => $industries, 'lead_sources' => $lead_sources, 'lead_statuses' => $lead_statuses, 'countries' => $countries, 'state' => $state, 'city' => $city, 'lead' => $lead, 'service_types' => $service_types, 'sources' => $sources, 'activities' => $activities], 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
    }

    /**
     * Update the specified lead in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'company' => 'nullable|regex:/^[a-z0-9\s\-\.\&]+$/i|max:255',
                    'title' => 'nullable',
                    'first_name' => 'required|regex:/^[a-z\s\-\.]+$/i|max:255',
                    'last_name' => 'nullable|regex:/^[a-z\s\-\.]+$/i|max:255',
                    'email' => 'required_without:phone|nullable|email:filter|unique:leads,email,' . $id,
                    'secondary_email' => 'nullable|email:filter',
                    'phone' => 'required_without:email|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:30|unique:leads,phone,' . $id,
                    'mobile' => 'nullable|regex:/^([0-9]*)$/|digits:10',
                    'whatsapp' => 'nullable|regex:/^([0-9]*)$/|digits:10',
                    'website' => ['nullable', 'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'],
                    // 'website' => ['nullable', 'regex:/^(http:\/\/www\.|https:\/\/www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i', new Hostname],
                    'zip_code' => 'nullable', //|regex:/^[0-9]{6}$/
                    'street1' => 'nullable|max:255',
                    'street2' => 'nullable|max:255',
                    'lead_title' => 'nullable|regex:/^[a-z0-9\s\-\.\&]+$/i|max:255',
                    'skype_id' => 'nullable|regex:/^[a-z][a-z0-9\.,\-_]{5,31}$/i',
                    'twitter' => 'nullable|regex:/^[A-Za-z0-9_]{1,15}$/',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }

        try {
            $lead = Lead::find($id);
            $saved = $this->save($lead, $request);
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'lead';
            $operation = 'lead_modification';
            $description = 'lead of ' . $lead?->full_name . ($lead?->company ? " ({$lead->company})" : "");
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $lead, $custom_properties, $log_name, $description);

            if (isset($request->save)) {
                $requestSave = $request->save;
            } elseif (isset($request->saveAndNew)) {
                $requestSave = $request->saveAndNew;
            } else {
                $requestSave = 0;
            }
        } catch (Exception $e) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $e->getMessage()
                            ), 500);
        } finally {
            if ($saved) {
                return response()->json(['message' => 'Leads updated successfully.', 'response' => ['lead' => $lead, 'requestSave' => $requestSave], 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        }
    }

    /**
     * save and update function
     * @param Lead $lead
     * @param Request $request
     * @return type
     */
    private function save($lead, Request $request) {
        $lead->company = $request->company;
        $lead->title = $request->title;
        $lead->first_name = $request->first_name;
        $lead->last_name = $request->last_name;
        $lead->lead_title = $request->lead_title;
        $lead->lead_owner_id = $request->lead_owner_id;
        $lead->email = $request->email;
        $lead->phone = $request->phone;
        $lead->mobile = $request->mobile;
        $lead->whatsapp = $request->whatsapp;
        $lead->lead_source_id = $request->lead_source_id;
        $lead->industry_id = $request->industry_id;
        $lead->website = $request->website;
        $lead->lead_status_id = $request->lead_status_id;
        $lead->assigned_id = $request->assigned_id;
        $lead->skype_id = $request->skype_id;
        $lead->secondary_email = $request->secondary_email;
        $lead->twitter = $request->twitter;
        $lead->street1 = $request->street1;
        $lead->street2 = $request->street2;
        $lead->country_id = $request->country_id;
        $lead->state_id = $request->state;
        $lead->city_id = $request->city;
        $lead->zip_code = $request->zip_code;
        $lead->description = $request->description;
        $lead->is_public = $request->is_public;
        $lead->currency = $request->currency;
        $lead->service_type_id = $request->service_type_id;
        $lead->campaign_name = $request->campaign_name;

        if ($request->campaign_name) {
            if (!Campaign::where("name", $request->campaign_name)->exists()) {
                $campaign = new Campaign();
                $campaign->name = $request->campaign_name;
                $campaign->save();
            }
        }

        $saved = $lead->save();

        $path = storage_path('app/public/leadsFile/' . $lead->id);
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        if ($request->hasfile('lead_image')) {
            $file = $request->file('lead_image');
            $fileName = time() . '.' . $request->lead_image->extension();
            $save = $request->lead_image->move($path, $fileName);
            $lead->lead_image = 'leadsFile/' . $lead->id . '/' . $fileName;
            $lead->save();
        }
        return $saved;
    }

    /**
     * Leads convert to Patient
     * @param type $id
     * @param type $is_convert
     * @return type
     */
    public function patientConvert($id) {
        try {
            $lead = Lead::find($id);
            if (empty($lead)) {
                return response()->json(['message' => 'This Lead not found !', 'response' => "", 'status' => false], 400);
            }
            if (User::where(function ($query) use ($lead) {
                        if ($lead?->email) {
                            $query->where('email', $lead->email);
                        }
                        if ($lead?->phone) {
                            // Normalize the phone number by stripping out the country code
                            $phone = preg_replace('/^\+?\d{1,2}/', '', $lead->phone); // Removes country code (e.g., +91 or any country code)
                            // Compare the mobile number without the country code
                            $query->orWhere(function ($query) use ($phone) {
                                $query->where('mobile', $phone); // Check for mobile number without the country code
                            });
                        }
                    })->exists()) {
                return response()->json(['message' => 'This Patient already avaliable !', 'response' => "", 'status' => false], 400);
            }
            $user = new User();
            $user->name = trim(($lead?->title ? $lead?->title . ' ' : '') .
                    ($lead?->first_name ? $lead?->first_name . ' ' : '') .
                    $lead?->last_name);
            $user->email = $lead->email;
            $user->mobile = $lead?->phone ? preg_replace('/^\+?\d{1,2}/', '', $lead->phone) : NULL;
//            $user->password = Hash::make($request->input('password'));
            $user->address = trim(($lead?->street1 ? $lead?->street1 . ' ' : '') . $lead?->street2);
            $user->country = $lead->country_id;
            $user->city = $lead->city_id;
            $user->state = $lead->state_id;
            $user->pincode = $lead->zip_code;
            $user->status = 1;
            $user->save();

            $user->assignRole(4);
            $patient = new Patient();
            $patient->user_id = $user->id;
            $patient->converted = 1;
            $patient->save();
            $nextId = $patient ? $patient->id : '1';
            $patientId = "H-PA-00$nextId";
            $patient->patient_id = $patientId;
            if ($patient->save()) {
                $lead->is_convert = 1;
                $lead->save();
                // Activity logs
                $causer = auth()->user();
                $roleName = $causer->roles()->pluck('name')->first();
                $log_name = 'convertPatient';
                $operation = 'lead_convert_patient';
                $description = 'Lead ' . $lead?->full_name . ($lead?->company ? " ({$lead->company})" : "") . " convert to patient";
                $custom_properties = ['application' => config('app.name'),
                    'operation' => $operation,
                    'causer_name' => $causer->name,
                    'customer_id' => $patient->id,
                    'role_name' =>$roleName
                ];
                store_activity_log($causer, $lead, $custom_properties, $log_name, $description);
                return response()->json(['message' => 'Lead converted successfully.', 'response' => $lead, 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
    }

    /**
     * Remove the specified lead from storage.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $lead = Lead::find($id);
            if ($lead) {
                $lead->notes()->delete();
                $lead->attachments()->delete();
                $lead->followUps()->delete();
                $lead?->formLead()->delete();
                $lead->delete();
                return response()->json(['message' => 'Leads deleted successfully.', 'response' => '', 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        }
    }

    /**
     * update in show
     * @param Request $request
     * @return type
     */
    public function showUpdate(Request $request) {
        $le = Lead::find($request->params['id']);
        $key = array_keys($request->params);
        $value = array_values($request->params);
        $required = ['first_name'];
//        $required = ['title', 'first_name', 'last_name'];
        $title = ['company', 'lead_title', 'first_name', 'last_name', 'service_type', 'campaign_name'];
        $mail = ['email', 'secondary_email'];
        $phone = ['mobile', 'whatsapp'];
        $street = ['street1', 'street1'];
        if (in_array($key[1], $required)) {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'required',
                            ], ['params.' . $key[1] . '.required' => 'The ' . str_replace('_', ' ', $key[1]) . ' field is required.']);
        }
        if (in_array($key[1], $title)) {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'regex:/^[a-z0-9\s\-\.\&]+$/i',
                            ], ['params.' . $key[1] . '.regex' => 'The ' . str_replace('_', ' ', $key[1]) . ' field format is invalid.']);
        }
        if ($key[1] == 'email') {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => [Rule::requiredIf(function () {
                                return empty($le?->phone);
                            })]
                            ], ['params.' . $key[1] . '.required' => 'The ' . $key[1] . ' field  is required if phone is empty.',
            ]);
        }
        if (in_array($key[1], $mail)) {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|email:filter',
                            ], ['params.' . $key[1] . '.email' => 'The ' . str_replace('_', ' ', $key[1]) . ' must be a valid email address.']);
        }
        if (in_array($key[1], $phone)) {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|regex:/^([0-9]*)$/|digits:10',
                            ], ['params.' . $key[1] . '.regex' => 'The ' . $key[1] . ' number field format is invalid.',
                        'params.' . $key[1] . '.min' => 'The ' . $key[1] . ' number field must be at least 10 digits.',
            ]);
        }
        if (in_array($key[1], $street)) {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|max:255',
                            ], ['params.' . $key[1] . '.max' => 'The ' . $key[1] . ' must not be greater than 255 characters.',
            ]);
        }
        if ($key[1] == 'phone') {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => Rule::requiredIf(function () {
                            return empty($le?->email);
                        }), '|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:30',
                            ], ['params.' . $key[1] . '.required' => 'The ' . $key[1] . ' field  is required if email is empty.',
                        'params.' . $key[1] . '.regex' => 'The ' . $key[1] . ' number field format is invalid.',
                        'params.' . $key[1] . '.min' => 'The ' . $key[1] . ' number field must be at least 10 digits.',
                        'params.' . $key[1] . '.max' => 'The ' . $key[1] . ' must not be greater than 30 digits.',
            ]);
        }
        if ($key[1] == 'website') {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|url',
                            ], ['params.' . $key[1] . '.regex' => 'The ' . $key[1] . ' must be a valid host name.']);
        }
        if ($key[1] == 'twitter') {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|regex:/^[A-Za-z0-9_]{1,15}$/',
                            ], ['params.' . $key[1] . '.regex' => 'The ' . $key[1] . ' field format is invalid.']);
        }
        if ($key[1] == 'zip_code') {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|regex:/^[0-9]{6}$/',
                            ], ['params.' . $key[1] . '.regex' => 'The ' . str_replace('_', ' ', $key[1]) . ' field format is invalid.']);
        }
        if ($key[1] == 'skype_id') {
            $validator = Validator::make($request->all(), [
                        'params.' . $key[1] => 'nullable|regex:/^[a-z][a-z0-9\.,\-_]{5,31}$/i',
                            ], ['params.' . $key[1] . '.regex' => 'The ' . str_replace('_', ' ', $key[1]) . ' field format is invalid.']);
        }
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        if ($key[1] == 'campaign_name') {
            if (!Campaign::where("name", $key[1])->exists()) {
                $campaign = new Campaign();
                $campaign->name = $value[1];
                $campaign->save();
            }
        }
        $leads = Lead::updateOrCreate(['id' => $request->params['id']], [
                    $key[1] => $value[1]
                        ],);
        // Activity logs
        $causer = auth()->user();
        $log_name = 'lead';
        $operation = 'lead_modification';
        $description = 'lead of ' . $leads?->full_name . " (" . $leads?->company . ")";
        $custom_properties = ['application' => config('app.name'),
            'operation' => $operation,
            'causer_name' => $causer->name,
            'new_value' => 'hashed',
            'old_value' => 'hashed',
        ];
        store_activity_log($causer, $leads, $custom_properties, $log_name, $description);
        return ['value' => $leads[$key[1]], 'key' => str_replace('_', ' ', $key[1])];
    }

    /**
     * leads notes
     * @param Request $request
     * @return Note
     */
    public function leadNotes(Request $request) {
        $validator = Validator::make($request->all(), [
                    'comment' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            if (isset($request->id)) {
                $note = Note::findorFail($request->id);
            } else {
                $note = new Note();
            }

            $note->lead_id = $request->lead;
            $note->comments = $request->comment;
            $note->user_id = auth()->id();
            $note->save();
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'leadNotes';
            $operation = 'lead_note_create_or_update';
            $description = "lead " . $note?->lead?->full_name . ($note?->lead?->company ? " ({$note?->lead?->company})" : "") . " has a note";
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $note, $custom_properties, $log_name, $description);

            if ($note) {
                return Response::json(['status' => true,
                            'response' => $note->with('user')->find($note->id),
                            'message' => 'The lead note updated successfully.'], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * leads note delete
     * @param type $note
     * @return type
     */
    public function notesDelete($note) {
        $note = Note::findorFail($note);
        if ($note->delete()) {
            return Response::json(['status' => true,
                        'response' => '',
                        'message' => 'Note deleted successfully.'], 200);
        } else {
            return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
        }
    }

    /**
     * leads attachment
     * @param Request $request
     * @return Attachment
     */
    public function leadAttach(Request $request) {
        $validator = Validator::make($request->all(), [
                    'attachment' => 'required',
                    'attachment.*' => 'max:20000'
                        ], ['attachment.*.max' => 'Only 2MB attachment is allowed'
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            $at = [];
            $path = storage_path('app/public/leadsFile/' . $request->lead_id . '/Attachment');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            if ($request->hasfile('attachment')) {
                $files = $request->file('attachment');
                foreach ($files as $key => $file) {
                    $attach = new Attachment();
                    $attach->lead_id = $request->lead_id;
                    $fileName = $file->getClientOriginalName();
                    $save = $file->move($path, $fileName);
                    $attach->title = $fileName;
                    $attach->file_path = 'leadsFile/' . $request->lead_id . '/Attachment/' . $fileName;
                    $attach->save();
                    $at[$key] = $attach;
                }
                // Activity logs
                $causer = auth()->user();
                $roleName = $causer->roles()->pluck('name')->first();
                $log_name = 'leadAttachment';
                $operation = 'lead_attachment';
                $description = "lead " . $attach?->lead?->full_name . ($attach?->lead?->company ? " ({$attach?->lead?->company})" : "") . " has a attachment";
                $custom_properties = ['application' => config('app.name'),
                    'operation' => $operation,
                    'causer_name' => $causer->name,
                    'role_name' =>$roleName
                ];
                store_activity_log($causer, $attach, $custom_properties, $log_name, $description);
            }

            return Response::json(['status' => true,
                        'response' => $at,
                        'message' => 'The Attachment added successfully.'], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * leads attachment delete
     * @param type $attach
     * @return type
     */
    public function attachmentDelete($attach) {
        $attachment = Attachment::findorFail($attach);
        if ($attachment->delete()) {
            return Response::json(['status' => true,
                        'response' => '',
                        'message' => 'Attachment deleted successfully.'], 200);
        } else {
            return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
        }
    }

    /**
     * leads address update
     * @param Request $request
     * @return type
     */
    public function leadAddress(Request $request) {
        $validator = Validator::make($request->all(), [
                    'street1' => 'nullable|max:255',
                    'street2' => 'nullable|max:255',
                    'zip_code' => 'nullable|regex:/^[0-9]{6}$/',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            $lead = Lead::findorFail($request->lead_id);
            $lead->street1 = $request->street1;
            $lead->street2 = $request->street2;
            $lead->country_id = $request->country_id;
            $lead->state_id = $request->state;
            $lead->city_id = $request->city;
            $lead->zip_code = $request->zip_code;
            $lead->save();
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'leadAddress';
            $operation = 'lead_address_update';
            $description = "Lead " . $lead?->full_name . ($lead?->company ? " ({$lead->company})" : "") . " update address";
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $lead, $custom_properties, $log_name, $description);
            if ($lead) {
                return Response::json(['status' => true,
                            'response' => $lead,
                            'message' => 'The lead address updated successfully.'], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * leads follow up
     * @param Request $request
     * @return FollowUp
     */
    public function followUp(Request $request) {
        $validator = Validator::make($request->all(), [
                    'follower_id' => 'required',
                    'source' => 'required',
                    'date' => 'required|date_format:Y-m-d',
                    'next_follow_up_date' => 'required|date_format:Y-m-d',
                        ], ['date.date_format' => 'The date does not match the format d/m/Y',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            if (isset($request->id)) {
                $followUp = FollowUp::findorFail($request->id);
            } else {
                $followUp = new FollowUp();
            }

            $followUp->lead_id = $request->lead_id;
            $followUp->follower_id = $request->follower_id;
            $followUp->date = $request->date;
            $followUp->source_id = $request->source;
            $followUp->next_follow_up_date = $request->next_follow_up_date;
            $followUp->comments = $request->follow_up_comments;
            $followUp->save();
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'followup';
            $operation = 'followUp_add_or_update';
            $description = 'FollowUp of lead ' . $followUp?->lead?->full_name . ($followUp?->lead?->company ? " ({$followUp?->lead?->company})" : "");
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $followUp, $custom_properties, $log_name, $description);

            if ($followUp) {
                return Response::json(['status' => true,
                            'response' => $followUp,
                            'message' => 'The FollowUp added/updated successfully.'], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * lead follow up delete
     * @param type $param
     * @return type
     */
    public function followUpDelete($param) {
        $followUp = FollowUp::findorFail($param);
        if ($followUp->delete()) {
            return Response::json(['status' => true,
                        'response' => '',
                        'message' => 'Follow Up deleted successfully.'], 200);
        } else {
            return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
        }
    }

    /**
     * lead Source
     * @param Request $request
     * @return LeadSource
     */
    public function leadSource(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|regex:/^[a-z0-9\s\-\.\&]+$/i|unique:lead_sources,name',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            $lead_source = new LeadSource();
            $lead_source->name = $request->name;
            $lead_source->save();
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'leadSource';
            $operation = 'leadSource_create';
            $description = 'Lead Source ' . $lead_source->name . ' create';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $lead_source, $custom_properties, $log_name, $description);
            if ($lead_source) {
                return Response::json(['status' => true,
                            'response' => $lead_source,
                            'message' => 'The lead source added successfully'], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Service Type
     * @param Request $request
     * @return LeadSource
     */
    public function serviceType(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|regex:/^[a-z0-9\s\-\.\&]+$/i|unique:service_types,name',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
        try {
            $service_type = new ServiceType();
            $service_type->name = $request->name;
            $service_type->save();
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'serviceType';
            $operation = 'serviceType_create';
            $description = 'Service Type' . $service_type->name . ' create';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $service_type, $custom_properties, $log_name, $description);
            if ($service_type) {
                return Response::json(['status' => true,
                            'response' => $service_type,
                            'message' => 'The service type added successfully'], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * auto complete for campaign.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request) {
        $data = Campaign::select("name as value", "id")
                ->where('name', 'LIKE', '%' . $request->get('search') . '%')
                ->get();
        return Response::json(['status' => true,
                    'response' => $data,
                    'message' => 'get Campaign'], 200);
    }

    /**
     * for send list of follow up
     * @param Request $request
     * @return type
     */
    public function getFollowUp(Request $request) {
        $followUp = followUp::with('follower', 'source')->where('lead_id', $request->id)->orderBy('next_follow_up_date', 'desc')->take(4)->get();
        $users = User::all();
        $sources = Source::all();
        return Response::json(['status' => true,
                    'response' => ['followUp' => $followUp, 'users' => $users, 'sources' => $sources],
                    'message' => 'The lead followup fetch successfully!'], 200);
    }

    /**
     * popup follow up save
     * @param Request $request
     * @return type
     */
    public function folowUpSubmit(Request $request) {
        $validator = Validator::make($request->all(), [
                    'follower_id' => 'required',
                    'source' => 'required',
                    'date' => 'required',
                    'next_follow_up_date' => 'required',
        ]);
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->getMessageBag()->toArray(),
                            ), 400);
        }
        try {
            if (isset($request->id)) {
                $followUp = FollowUp::find($request->id);
            } else {
                $followUp = new FollowUp();
            }
            $followUp->lead_id = $request->lead_id;
            $followUp->follower_id = $request->follower_id;
            $followUp->date = date('Y-m-d', strtotime($request->date));
            $followUp->source_id = $request->source;
            $followUp->next_follow_up_date = date('Y-m-d', strtotime($request->next_follow_up_date));
            $followUp->comments = $request->follow_up_comments;
            $followUp->save();
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'followup';
            $operation = 'followUp_add_or_update';
            $description = 'FollowUp of lead ' . $followUp?->lead?->full_name . ($followUp?->lead?->company ? " ({$followUp?->lead?->company})" : "");
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $followUp, $custom_properties, $log_name, $description);
            $followUp = followUp::with('follower', 'source')->where('lead_id', $request->lead_id)->orderBy('next_follow_up_date', 'desc')->take(4)->get();
            return Response::json(['status' => true,
                        'response' => $followUp,
                        'message' => 'The lead followup fetch successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * get Source
     * @return type
     */
    public function getSource() {
        $sources = Source::all();
        return Response::json(['status' => true,
                    'response' => $sources,
                    'message' => 'get source up!'], 200);
    }

    /**
     * get Follow
     * @return type
     */
    public function getFollow() {
        $followers = User::all();
        return Response::json(['status' => true,
                    'response' => $followers,
                    'message' => 'followers follow up!'], 200);
    }

    /**
     * destroy Follow Up
     * @param Request $request
     * @return type
     */
    public function destroyFollowUp(Request $request) {
        $followUp = FollowUp::find($request->id)->delete();
        $followUp = followUp::with('follower', 'source')->where('lead_id', $request->lead_id)->orderBy('next_follow_up_date', 'desc')->take(4)->get();
        return Response::json(['status' => true,
                    'response' => $followUp,
                    'message' => 'destroy follow up!'], 200);
    }

    /**
     * Store a newly created lead in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLead(Request $request) {

        $validator = Validator::make($request->all(), [
                    'company' => 'nullable|regex:/^[a-z0-9\s\-\.\&]+$/i|max:255',
                    'title' => 'nullable|max:255',
                    'first_name' => 'required|regex:/^[a-z\s\-\.]+$/i|max:255',
                    'last_name' => 'nullable|regex:/^[a-z\s\-\.]+$/i|max:255',
                    'email' => 'required_without:phone|nullable|email:filter|unique:leads,email',
                    'secondary_email' => 'nullable|email:filter',
                    'phone' => 'required_without:email|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:30||unique:leads,phone',
                    'mobile' => 'nullable|regex:/^([0-9]*)$/|digits:10',
                    'whatsapp' => 'nullable|regex:/^([0-9]*)$/|digits:10',
                    'website' => 'nullable|url',
                    'zip_code' => 'nullable', //|regex:/^[0-9]{6}$/
                    'street1' => 'nullable|max:255',
                    'street2' => 'nullable|max:255',
                    'lead_title' => 'nullable|regex:/^[a-z0-9\s\-\.\&]+$/i|max:255',
                    'skype_id' => 'nullable|regex:/^[a-z][a-z0-9\.,\-_]{5,31}$/i',
                    'twitter' => 'nullable|regex:/^[A-Za-z0-9_]{1,15}$/',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'message' => 'Validation error',
                        'errors' => $validator->errors(),
                            ], 422);
        }
//, 'regex:/^(http:\/\/www\.|https:\/\/www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i', new Hostname],
        try {
            $lead = new Lead();
            $saved = $this->save($lead, $request);
            // Activity logs
            $causer = auth()->user();
            $roleName = $causer->roles()->pluck('name')->first();
            $log_name = 'lead';
            $operation = 'lead_create';
            $description = 'lead of ' . $lead?->full_name . ($lead?->company ? " ({$lead->company})" : "");
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'role_name' =>$roleName
            ];
            store_activity_log($causer, $lead, $custom_properties, $log_name, $description);
            if (isset($request->save)) {
                $requestSave = $request->save;
            } elseif (isset($request->saveAndNew)) {
                $requestSave = $request->saveAndNew;
            } else {
                $requestSave = 0;
            }
        } catch (Exception $ex) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $ex->getMessage()
                            ), 500);
        } finally {
            if ($saved) {
                return response()->json(['message' => 'Leads added successfully.', 'response' => ['lead' => $lead, 'requestSave' => $requestSave], 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, Please try again later!', 'response' => "", 'status' => false], 400);
            }
        }
    }

    /**
     * for get states
     * @param Request $request
     * @return type
     */
    public function getStates(Request $request) {
        $data['states'] = State::where("country_id", $request->country_id)->get(["name", "id"]);
        return response()->json($data, 200);
    }

    /**
     * for get cities
     * @param Request $request
     * @return type
     */
    public function getCities(Request $request) {
        $data['cities'] = City::where("state_id", $request->state_id)->get(["name", "id"]);
        return response()->json($data, 200);
    }

    /**
     * Notes List
     * @param type $id
     * @return type
     */
    public function notesList($id) {
        try {
            $notes = Note::where('lead_id', $id)->get();

            return Response::json(['status' => true,
                        'response' => $notes,
                        'message' => 'The Notes fetch successfully'], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Attachment List
     * @param type $id
     * @return type
     */
    public function attachmentList($id) {
        try {
            $attachments = Attachment::where('lead_id', $id)->get();

            return Response::json(['status' => true,
                        'response' => $attachments,
                        'message' => 'The Attachment fetch successfully'], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Update Lead Assign
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateLeadAssign(Request $request, $id) {
        try {
            $leads = Lead::updateOrCreate(['id' => $id], [
                        'assigned_id' => $request->assigned_id
            ]);
            return Response::json(['status' => true,
                        'response' => $leads,
                        'message' => 'The lead assignment has been updated successfully.'], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    /**
     * Update Lead Status
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateLeadStatus(Request $request, $id) {
        try {
            $leads = Lead::updateOrCreate(['id' => $id], [
                        'lead_status_id' => $request->lead_status_id
            ]);
            return Response::json(['status' => true,
                        'response' => $leads,
                        'message' => 'The lead status has been updated successfully.'], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }
    
    public function leadConvertReport(Request $request) {
        $length = isset($request->length) ? $request->length : 10;
        $query = Lead::query()->where('is_convert', 1);
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->where('created_at', '>=', $start);
            $query->where('created_at', '<=', $end);
        }
        $leadCountSystem = $query->count();

        $query = User::query()->with('patient')->role('Patient');
        if ($request->daterange != '') {
            $daterange = explode('-', $request->daterange);
            $start = Carbon::createFromFormat('d/m/Y', trim($daterange[0]))->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y', trim($daterange[1]))->format('Y-m-d');
            $query->where('created_at', '>=', $start);
            $query->where('created_at', '<=', $end);
        }
        $patients = $query->latest()->get();

        $leadCounts = 0;
        $convertedPatients = [];

        foreach ($patients as $key => $patient) {
            $leadCountSys = Lead::where('is_convert', 1)->distinct()
                            ->where(function ($query) use ($patient) {
                                $query->where('email', $patient->email?$patient->email:'')
                                ->orWhere('phone', $patient->phone?$patient->phone:'');
                            })->count();
            $leadCount = Lead::where('is_convert', 0)->distinct()
                            ->where(function ($query) use ($patient) {
                                $query->where('email', $patient->email?$patient->email:'')
                                ->orWhere('phone', $patient->phone?$patient->phone:'');
                            })->count();

            if ($leadCountSys) {
                $convertedPatients[] = $patient;
            }
            if ($leadCount) {
                $leadCounts++;
                $convertedPatients[] = $patient;
            }
        }
        // Convert the array to JSON 
        $convertedPatientsJson = json_encode($convertedPatients, true);
        // Decode the JSON to array 
        $convertedPatientsArray = json_decode($convertedPatientsJson, true);
        // Paginate the results 
        $convertedPatientsPaginated = paginate($convertedPatientsArray, $length, null, ['path' => $request->url(), 'query' => $request->query()]);

        return response()->json([
                    'status' => true,
                    'response' => [
                        'leadConvertCountSystem' => $leadCountSystem,
                        'leadConvertCountManual' => $leadCounts,
                        'leadConvertCount' => $leadCountSystem + $leadCounts,
                        'convertedPatients' => $convertedPatientsPaginated
                    ],
                    'message' => 'Lead Convert Report fetched successfully.',
                        ], 200);
    }

}
