<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\Doctor;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Symptom;
use App\Models\User;
use App\Models\Schedule;
use App\Models\Patient;
use Illuminate\Support\Facades\DB;

class AppointmentSynchronizeController extends Controller {

    public function index(Request $request) {
        $apiKey = $request->header('X-API-KEY');
        $doctor = Doctor::where('api_key', $apiKey)->first();
        if ($doctor) {
            $appointment = Appointment::with('doctor', 'patient')->where('doctor_id', $doctor->user->id)->orderBy('created_at', 'DESC')->get();
        }
        return response()->json($appointment, 200);
    }

    /**
     * search patient by mobile number
     * 
     * @param type $mobile
     * @return type
     */
    public function patientSearch($mobile) {

        $user = User::with('patient')->where('mobile', 'LIKE', "%$mobile%")->orwhere('email', 'LIKE', "%$mobile%")->first();
        return response()->json($user, 200);
    }

    /*
     * * Create a Patient
     */

    public function addPatient(Request $request) {
        $rules = [
            'name' => 'required|regex: /^[a-zA-Z ]{2,255}$/',
            'email' => 'nullable|email|regex:/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/|unique:users|email:rfc,dns',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
            'picture' => 'nullable|mimes:jpg,png,jpeg,gif,svg|max:5120',
            'address' => 'nullable|max:255',
            'gender' => 'required',
            'dob' => 'nullable|before:today'
        ];
        $messages = [
            'name.required' => 'The name field is required',
            'name.regex' => 'The name may not be greater than 2-255',
            'email.regex' => 'The email invalid format',
            'email.unique' => 'Email address is already taken',
            'mobile.required' => 'The mobile number is required',
            'mobile.numeric' => 'The mobile number format is invalid.',
            'mobile.min' => 'The mobile number must be at least 10 characters.',
            'mobile.unique' => 'The mobile number unique',
            'picture.mimes' => 'The picture invalid format jpg,png,jpeg,gif,svg',
            'picture.max' => 'The picture may not be greater than 5MB',
            'address.required' => 'The address field is required',
            'address.max' => 'The address may not be greater than 255',
            'gender.required' => 'The gender field is required',
            'dob.before' => 'The date of birth must be a date before today',
            'dob.date' => 'The date format is invalid'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {

            if ($request->hasFile('picture')) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'patient'");
                $nameUniq = $statement[0]->Auto_increment ? $statement[0]->Auto_increment : uniqid();
                $file_name = time() . '_' . $nameUniq;
                $fileName = $file_name . '.' . $request->file('picture')->getClientOriginalExtension();
                $path = 'patient' . '/' . $request->file('picture')->storeAs($nameUniq, $fileName, 'patient');
            } else {
                $path = NULL;
            }

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
//            $user->password = Hash::make($request->input('password'));
            $user->address = $request->address;
            $user->picture = $path;
            $user->gender = $request->gender;
            $user->status = $request->status ?? 0;
            $user->save();

            $user->assignRole(4);
            $patient = new Patient();
            $patient->user_id = $user->id;
            $patient->blood_group = $request->blood_group;
            $patient->age = $request->age;
            $patient->save();
            $nextId = $patient ? $patient->id : '1';
            $patientId = "H-PA-00$nextId";
            $patient->patient_id = $patientId;
            if ($patient->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Patient detail Store successfully.',
                    "response" => $user,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * doctor slot create
     * 
     * @param Request $request
     * @return type
     */
    public function doctorSlot($date, $time, $doctor_id) {
        $day = Carbon::parse($date)->format('D');
        $date = Carbon::parse($date)->format('Y-m-d');
//        $apiKey = $request->header('X-API-KEY');
        $doctor = Doctor::findOrFail($doctor_id);
        $schedules = Schedule::where('doctor_id', $doctor->user_id)->get();
        $slots = array();
        foreach ($schedules as $schedule) {
            if ($schedule->available_days == 'Sunday' && $day == 'Sun') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Monday' && $day == 'Mon') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Tuesday' && $day == 'Tue') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Wednesday' && $day == 'Wed') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Thursday' && $day == 'Thu') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Friday' && $day == 'Fri') {
                $slots = $this->getSlot($schedule, $day, $date);
            } elseif ($schedule->available_days == 'Saturday' && $day == 'Sat') {
                $slots = $this->getSlot($schedule, $day, $date);
            }
        }
        return $slots;
    }

    /**
     * generate doctor slots
     * @param type $schedule
     * @param type $day
     * @param type $date
     * @return type
     */
    static public function getSlot($schedule, $day, $date) {
        $startTime = strtotime($date . ' ' . $schedule->start_time);
        $endTime = strtotime($date . ' ' . $schedule->end_time);
        $durationTime = Carbon::parse($schedule->per_patient_time)->format('i') * 60;
        $dr = $schedule->doctor_id;
        $times = [];
        while ($startTime < $endTime) {
            if ($startTime > strtotime(date('Y-m-d H:i:s'))) {
                $times[] = $startTime;
            }
            $startTime += $durationTime;
        }
        $slots = [];
        foreach ($times as $time) {
            if (date("H:i", $time) <= date("H:i", $endTime)) {
                if (!self::isSlotBooked($date, $time, $dr)) {
                    $slots[] = [
                        'time' => date("h:i A", $time),
//                    'duration' => $schedule->per_patient_time,
//                    'booked' => self::isSlotBooked($date, $time, $dr),
                    ];
                }
            }
        }
        return $slots;
    }

    /**
     * check slot booking by  doctor id, date, and time
     * @param type $day
     * @param type $time
     * @param type $dr
     * @return type
     */
    static public function isSlotBooked($day, $time, $dr) {
        return $appointment = Appointment::where('meeting_at', Carbon::parse($day)->format('Y-m-d') . ' ' . Carbon::parse($time)->format('H:i:s'))
                        ->where('doctor_id', $dr)
                        ->where('status', '!=', 4)->count();
    }

    /**
     * Check Date By Booking
     * @param type $day
     * @param type $time
     * @param type $dr
     * @return type
     */
    static public function checkDateByBooking($day, $time, $dr) {
        return $appointment = Appointment::where('date', Carbon::parse($day)->format('Y-m-d'))
                        ->where('doctor_id', $dr)
                        ->where('status', '!=', 4)->count();
    }

    /**
     * list of all Symptoms
     * @return type
     */
    public function symptoms() {
        $symptoms = Symptom::all();
        $symptom = array();
        if ($symptoms->isNotEmpty()) {
            foreach ($symptoms as $key => $sympt) {
                $symptom[$key]['id'] = $sympt->id;
                $symptom[$key]['value'] = $sympt->name;
                $symptom[$key]['label'] = ucwords($sympt->name);
            }
        }
        return response()->json($symptom);
    }

    /**
     * Appointment Store
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {
        $rules = [
            'date' => 'required|date|after:yesterday',
            'time' => 'required',
            'problems' => 'nullable',
            'name' => 'required|regex: /^[a-zA-Z ]{2,255}$/',
            'email' => 'nullable|email|regex:/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/|email:rfc,dns',
            'mobile' => 'required|numeric|digits:10',
            'address' => 'nullable|max:255',
            'gender' => 'nullable',
            'dob' => 'nullable|before:today',
            'age' => 'nullable|numeric',
//            'patient_id' => 'required',
//            'doctor_id' => 'required'
        ];

        $messages = [
            'date.required' => 'The Appointment Date field is required.',
            'time.required' => 'The Appointment Time field is required.',
            'name.required' => 'The patient name field is required',
            'name.regex' => 'The Patient name may not be greater than 2-255',
            'email.regex' => 'The Patient email invalid format',
            'email.unique' => 'Email address is already taken',
            'mobile.required' => 'The Patient mobile number is required',
            'mobile.numeric' => 'The Patient mobile number format is invalid.',
            'mobile.min' => 'The Patient mobile number must be at least 10 characters.',
            'mobile.unique' => 'The Patient mobile number unique',
            'address.max' => 'The Patient address may not be greater than 255',
            'gender.required' => 'The Patient gender field is required',
            'dob.before' => 'The Patient date of birth must be a date before today',
//            'patient_id.required' => 'Search patient by mobile number.',
//            'doctor_id.required' => 'The Doctor field is required.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {
            $user = User::where('mobile', $request->mobile)->orWhere('name', $request->name)->orWhere('email', $request->email)->first();
            if (empty($user)) {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->mobile = $request->mobile;
                $user->dob = isset($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : '';
                $user->address = $request->address ?? '';
                $user->gender = $request->gender;
                $user->status = 1;
                $user->save();

                $user->assignRole(4);
                $patient = new Patient();
                $patient->user_id = $user->id;
                $patient->blood_group = $request->blood_group ?? '';
                $patient->age = $request->age;
                $patient->save();
                $nextId = $patient ? $patient->id : '1';
                $patientId = "H-PA-00$nextId";
                $patient->patient_id = $patientId;
                $patient->save();
            }
            $apiKey = $request->header('X-API-KEY');
            $doctor = Doctor::where('api_key', $apiKey)->first();
            $appointments = $this->isSlotBooked($request->date, $request->time, $doctor->user_id);
            if ($appointments) {
                return response()->json(array(
                            'success' => false,
                            'message' => 'Sorry, Your selected slot is already booked',
                            "response" => '',
                                ), 422);
            }
            $slots = $this->doctorSlot($request->date, $request->time, $doctor->id);
            $time = Carbon::parse($request->time)->format('H:i');
//            dd($slots,in_array($time, array_column($slots, 'time'),true));
            if (!in_array($request->time, array_column($slots, 'time'), true)) {
                $checkDateByBooking = $this->checkDateByBooking($request->date, $request->time, $doctor->user_id);
                if ($checkDateByBooking) {
                    return response()->json(array(
                                'success' => false,
                                'message' => 'Your current schedule has conflicts with your appointments. You have appointments for this date. If you want to take another appointment, please update your schedule.',
                                "response" => 'change',
                                    ), 409);
                } else {
                    return response()->json(array(
                                'success' => false,
                                'message' => 'Your current schedule has conflicts with your appointments. Please update your schedule to accommodate new appointments.',
                                "response" => 'change',
                                    ), 409);
                }
            }

            $statement = DB::select("SHOW TABLE STATUS LIKE 'appointment'");
            $nextId = ( isset($statement[0]->Auto_increment) ) ? $statement[0]->Auto_increment : '1';
            $appointment_id = "H-APP-00$nextId";
            $appointment = new Appointment();
            $appointment->appointment_id = $appointment_id;
            $appointment->patient_id = $user->id;
            $appointment->doctor_id = $doctor->user_id;
            $appointment->serial_no = null;
            $appointment->date = Carbon::parse($request->date)->format('Y-m-d');
            $appointment->time = $time;
            $appointment->meeting_at = Carbon::parse($request->date)->format('Y-m-d') . " " . Carbon::parse($request->time)->format('H:i');
            $appointment->duration = null;
            $appointment->come_from = $request->come_from;
            $sympt = array();
            if (isset($request->problems)) {
                $problems = explode(',', $request->problems);
                foreach ($problems as $key => $symptom) {
                    $symptom_old= Symptom::where('name','LIKE',trim($symptom))->first();
                    if($symptom_old){
                     $sympt[$key]['id'] = $symptom_old->id;                       
                    }else{
                    $symptom_new = Symptom::firstOrCreate(['name' => trim($symptom)]);                       
                    $sympt[$key]['id'] = $symptom_new->id;
                    }
                    $sympt[$key]['value'] = trim($symptom);
                    $sympt[$key]['label'] = ucwords(trim($symptom));
                }
//                foreach ($request->problems as $key => $symptom) {
//                    $symptom_old= Symptom::where('name','LIKE',$symptom)->first();
//                    if($symptom_old){
//                     $sympt[$key]['id'] = $symptom_old->id;                       
//                    }else{
//                    $symptom_new = Symptom::firstOrCreate(['name' => $symptom]);                       
//                    $sympt[$key]['id'] = $symptom_new->id;
//                    }
//                    $sympt[$key]['value'] = $symptom;
//                    $sympt[$key]['label'] = ucwords($symptom);
//                }
            }
            $appointment->problems = json_encode($sympt);
            $appointment->status = 0;
            if ($appointment->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Appointment created successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }

        return response()->json($data, 201);
    }

    public function show($id) {
        $appointment = Appointment::with('patient.patient')->findOrFail($id);
        return response()->json($appointment);
    }

    public function update(Request $request, $id) {
        $rules = [
            'date' => 'required|date',
            'time' => 'required',
            'problems' => 'nullable',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $appointment = Appointment::findOrFail($id);
            $appointment->date = Carbon::parse($request->date)->format('Y-m-d');
            $appointment->time = date('H:i:s', strtotime($request->time));
            $appointment->duration = $request->duration;
            $appointment->meeting_at = Carbon::parse($request->date)->format('Y-m-d') . " " . date('H:i:s', strtotime($request->time));
            $sympt = array();
            foreach ($request->problems as $key => $symptom) {
                if (!isset($symptom['id'])) {
                    $symptom_new = Symptom::firstOrCreate(['name' => $symptom['value']]);
                    $sympt[$key]['id'] = $symptom_new->id;
                } else {
                    $sympt[$key]['id'] = $symptom['id'];
                }
                $sympt[$key]['value'] = $symptom['value'];
                $sympt[$key]['label'] = ucwords($symptom['value']);
            }
            $appointment->problems = json_encode($sympt);
            $appointment->status = 0;
            if ($appointment->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Appointment updated successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response()->json($data);
    }

    public function destroy($id) {
        $appointment = Appointment::findOrFail($id);
        $appointment->delete();

        return response()->json(null, 204);
    }

    /**
     * 
     * @param type $id
     * @param type $status
     * @return type
     */
    public function updateAppointmentSatus($id, $status) {
        try {
            $appointment = Appointment::find($id);
            $appointment->status = $status;
            if ($appointment->save()) {
                $data = array(
                    'status' => true,
                    'message' => 'Appointment status updated successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Booked Slots
     * @param Request $request
     * @return type
     */
    public function bookedSlots(Request $request) {
        try {
            $apiKey = $request->header('X-API-KEY');
            $doctor = Doctor::where('api_key', $apiKey)->first();
            $appointment = Appointment::where('doctor_id', $doctor->user_id)->get(['date', 'time', 'meeting_at']);
            if ($appointment) {
                $data = array(
                    'status' => true,
                    'message' => 'Booked appointment fetched successfully.',
                    "response" => $appointment,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

    /**
     * Change Schedule if not match with website schedule
     * @param Request $request
     * @return type
     */
    public function changeSchedule(Request $request) {
        $rules = [
//            'date' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required|after_or_equal:start_date',
            'per_patient_time' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $date = $request->date;
            $start_time = $request->start_time;
            $end_time = $request->end_time;
//            $patient_time = explode(' ', $request->per_patient_time);
//            if(isset($patient_time[1])){
//               if(strtolower($patient_time[1]) == 'minute' ||strtolower($patient_time[1]) == 'minutes'){
                   $ptime = (int)$request->per_patient_time * 60;
//               }elseif(strtolower($patient_time[1]) == 'hour' ||strtolower($patient_time[1]) == 'hours') {
//                   $ptime = (int)$patient_time[0]*3600;
//               }
//            }            
            $per_patient_time = date('H:i:s', strtotime('00:00:00') + $ptime);
//            $per_patient_time = Carbon::parse($request->per_patient_time)->format('H:i:s');
            $day = Carbon::parse($date)->format('l');
            $apiKey = $request->header('X-API-KEY');
            $doctor = Doctor::where('api_key', $apiKey)->first();
            $schedules = Schedule::where('doctor_id', $doctor->user_id)->get();
            foreach($schedules as $schedule){
            $sch = Schedule::find($schedule->id);
            $sch->start_time = Carbon::parse($start_time)->format('H:i');
            $sch->end_time = Carbon::parse($end_time)->format('H:i');
            $sch->per_patient_time = $per_patient_time;
            $saved = $sch->save();
            }

            if ($saved) {
                $data = array(
                    'status' => true,
                    'message' => 'Appointment Schedule is changed successfully.',
                    "response" => $schedules,
                );
            } else {
                $data = array(
                    'status' => false,
                    'message' => 'Something went wrong, Please try again later!',
                    "response" => '',
                );
            }
        } catch (Exception $exception) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'Something went wrong, Please try again later!',
                        'errors' => $exception->getMessage()
                            ), 500);
        }
        return response($data);
    }

}
