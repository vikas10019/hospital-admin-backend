 <?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SpecialityController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\AddDoctorController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\BedController;
use App\Http\Controllers\BedAssignController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\PrescriptionController;
use App\Http\Controllers\MedicineController;
use App\Http\Controllers\CalenderAppointmentController;
use App\Http\Controllers\BillController;
use App\Http\Controllers\AppointmentSynchronizeController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\LeadCollectController;
use App\Http\Controllers\ActivityLogController;
/*
|--------------------------------------------------------------------------
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('upload_files', [AddDoctorController::class, 'uploadImages']);

Route::middleware('auth:sanctum')->group(function ()
{
    Route::get('user', [AuthController::class, 'user']);
    Route::post('logout', [AuthController::class, 'logout']);

    Route::get('editUser/{id}', [AddDoctorController::class, 'userEdit']);
    Route::post('update-user/{id}', [AddDoctorController::class, 'updateUser']);

    Route::post('add-doctor', [AddDoctorController::class, 'addDoctor']);
    Route::get('list-doctor', [AddDoctorController::class, 'doctorListing']);
    Route::get('getDoctors', [AddDoctorController::class, 'getDoctor']);
    Route::get('delete-doctor/{id}', [AddDoctorController::class,'deleteDoctor']);
    Route::get('edit-doctor/{id}', [AddDoctorController::class, 'doctorEdit']);
    Route::post('update-doctor/{id}', [AddDoctorController::class, 'updateDoctor']);
    Route::get('doctor-details/{id}', [AddDoctorController::class, 'doctorDetails']);
    Route::post('update-doctor-details', [AddDoctorController::class, 'updateDoctorDetails']);
    Route::post('update-doctor-password', [AddDoctorController::class, 'updateDoctorpassword']);
    Route::post('doctor-setting/{id}', [AddDoctorController::class, 'doctorSetting']);        

    Route::post('resetPassword', [ChangePasswordController::class,'passwordResetProcess']);

    Route::post('add-role', [RoleController::class, 'index']);
    Route::get('list-role', [RoleController::class, 'roleListing']);
    Route::get('delete-role/{id}', [RoleController::class, 'deleteRole']);
    Route::get('edit-role/{id}', [RoleController::class, 'editRole']);
    Route::post('update-role', [RoleController::class, 'updateRole']);

    Route::get('get-roles', [RoleController::class, 'getRoles']);
    Route::get('get-permissions', [RoleController::class, 'getPermission']);
    
    Route::get('get-user-permissions', [RoleController::class, 'getUserPermission']);

    Route::post('add-patient', [PatientController::class, 'addPatient']);
    Route::get('list-patient', [PatientController::class, 'patientListing']);
    Route::get('delete-patient/{id}', [PatientController::class, 'deletepatient']);
    Route::get('edit-patient/{id}',[PatientController::class, 'EditPatient']);
    Route::post('update-patient',[PatientController::class, 'updatePatient']);
    Route::get('edit-Search/{mobile}',[PatientController::class, 'searchPatient']);
    
    Route::get('get-patient-detail/{id}',[PatientController::class, 'patientDetails']);
    Route::post('update-patient-password',[PatientController::class, 'updatePassword']);

    Route::post('add-document', [DocumentController::class, 'addDocument']);
    Route::get('list-document', [DocumentController::class, 'ListingDocument']);
    Route::delete('delete-document/{id}', [DocumentController::class, 'deleteDocument']);
    Route::get('edit-document/{id}',[DocumentController::class, 'EditDocument']);
    Route::post('update-document',[DocumentController::class, 'updateDocument']);

    Route::post('add-Document', [DocumentController::class, 'addDocument']);
    Route::get('list-Document', [DocumentController::class, 'ListingDocument']);
    Route::delete('delete-Document/{id}', [DocumentController::class, 'deleteDocument']);
    Route::get('edit-Document/{id}',[DocumentController::class, 'EditDocument']);
    Route::post('update-Document/{id}',[DocumentController::class, 'updateDocument']);

    Route::post('add-speciality', [SpecialityController::class, 'addSpeciality']);
    Route::get('list-speciality', [SpecialityController::class, 'listingSpeciality']);
    Route::delete('delete-speciality/{id}', [SpecialityController::class, 'deleteSpeciality']);
    Route::post('edit-speciality/{id}',[SpecialityController::class, 'editSpeciality']);
    Route::post('update-speciality/{id}',[SpecialityController::class, 'updateSpeciality']);
    Route::get('all-speciality', [SpecialityController::class, 'allSpeciality']);

    Route::post('add-bed', [BedController::class, 'addBed']);
    Route::get('list-bed', [BedController::class, 'BedListing']);
    Route::delete('delete-bed/{id}', [BedController::class,'BedDelete']);
    Route::post('edit-bed/{id}', [BedController::class, 'BedEdit']);
    Route::post('update-bed/{id}', [BedController::class, 'BedUpdate']);
    Route::post('bed-report/{id}', [BedController::class, 'BedReport']);

    Route::post('add-bedassign', [BedAssignController::class, 'addBedAssign']);
    Route::delete('delete-bedassign/{id}', [BedAssignController::class,'BedAssignDelete']);
    Route::post('edit-bedassign/{id}', [BedAssignController::class, 'BedAssignEdit']);
    Route::post('update-bedassign/{id}', [BedAssignController::class, 'BedAssignUpdate']);
    Route::get('assign-listing', [BedAssignController::class, 'BedAssignListing']);

    Route::post('add-notice', [NoticeController::class, 'addNotice']);
    Route::get('list-notice', [NoticeController::class, 'NoticeListing']);
    Route::delete('delete-notice/{id}', [NoticeController::class,'NoticeDelete']);
    Route::post('edit-notice/{id}', [NoticeController::class, 'NoticeEdit']);
    Route::post('update-notice/{id}', [NoticeController::class, 'NoticeUpdate']);

    Route::post('add-schedule', [ScheduleController::class, 'addschedule']);
    Route::get('list-schedule', [ScheduleController::class, 'scheduleListing']);
    Route::delete('delete-schedule/{doctor_id}', [ScheduleController::class,'deleteSchedule']);
    Route::get('edit-schedule/{doctor_id}', [ScheduleController::class, 'editSchedule']);
    Route::post('update-schedule', [ScheduleController::class, 'updateSchedule']);

    // Appointment api
    Route::post('add-appointment', [AppointmentController::class, 'addAppointment']);
    Route::get('list-appointment', [AppointmentController::class, 'AppointmentListing']);
    Route::delete('delete-appointment/{id}', [AppointmentController::class,'deleteAppointment']);
    Route::get('edit-appointment/{id}', [AppointmentController::class, 'editAppointment']);
    Route::post('update-appointment/{id}', [AppointmentController::class, 'updateAppointment']);
    Route::post('get-doctor-slot', [AppointmentController::class, 'doctorSlot']);
    Route::post('doctor-Reschdule/{id}', [AppointmentController::class, 'RescheduleAppointment']);
    Route::post('cancel-appointment/{id}', [AppointmentController::class, 'cancelAppointment']);
    Route::get('appointment/status/update/{id?}/{status?}', [AppointmentController::class, 'updateAppointmentStatus']);
    Route::get('appointment-details/{id}', [AppointmentController::class, 'appointmentDetails']);
       
    //CalenderAppointment api
    Route::get('all-appointment', [CalenderAppointmentController::class, 'calendarAppoint']);
    Route::get('calendar-appointment/{id?}', [CalenderAppointmentController::class, 'calendarAppoint']);
    Route::get('search-patient/{mobile}',[CalenderAppointmentController::class, 'patientSearch']);
    Route::get('todayAppointments/{id?}/{status?}',[CalenderAppointmentController::class, 'todayAppointments']);
    Route::post('add_appointment', [CalenderAppointmentController::class, 'addAppointment']);
    Route::delete('delete_appointment/{id}', [CalenderAppointmentController::class,'deleteAppointment']);
    Route::post('update_appointment/{id}', [CalenderAppointmentController::class, 'updateAppointment']);
    Route::get('doctor_schedule/{doctor_id}', [CalenderAppointmentController::class, 'doctorSchedule']);
    Route::post('cancelAppointment/{id}', [CalenderAppointmentController::class, 'cancelAppointments']);
    Route::get('appointment-status', [CalenderAppointmentController::class, 'appointmentStatus']);
    
    //refered search Api
    Route::post('referred-search', [CalenderAppointmentController::class, 'referredSearch']);
    
    //Medicine search
    Route::post('medicine-search', [CalenderAppointmentController::class, 'medicineSearch']);
    
    Route::get('list-staff', [StaffController::class, 'index']);
    Route::post('add-staff', [StaffController::class, 'addStaff']);
    Route::get('edit-staff/{id}', [StaffController::class, 'EditStaff']);
    Route::post('update-staff', [StaffController::class, 'updateStaff']);
    Route::get('delete-staff/{id}', [StaffController::class, 'deleteStaff']);

    Route::post('add-bill', [BillController::class, 'addBill']);
    Route::get('list-bill', [BillController::class, 'listBill']);
    Route::delete('delete-bill/{id}', [BillController::class,'deleteBill']);
    Route::get('edit-bill/{id}', [BillController::class, 'editBill']);
    Route::post('update-bill/{id}', [BillController::class, 'updateBill']);
    Route::get('bill-details/{id}', [BillController::class, 'billDetails']);

    Route::post('add-service', [ServiceController::class, 'addService']);
    Route::get('list-service', [ServiceController::class, 'listService']);
    Route::delete('delete-service/{id}', [ServiceController::class,'deleteService']);
    Route::get('edit-service/{id}', [ServiceController::class, 'editService']);
    Route::post('update-service/{id}', [ServiceController::class, 'updateService']);

    
    // prescription
    Route::get('/prescription',[PrescriptionController::class,'index']);
    Route::get('/prescription/create',[PrescriptionController::class,'create']);
    Route::post('/prescription/store',[PrescriptionController::class,'store']);
    Route::get('/prescription/search/{mobile}',[PrescriptionController::class,'patientSearch']);
    Route::get('/prescription/symptoms',[PrescriptionController::class,'symptoms']);
    Route::get('/prescription/investigation',[PrescriptionController::class,'investigation']);
    Route::get('/prescription/tests',[PrescriptionController::class,'test']);
    Route::get('/prescription/edit/{id}',[PrescriptionController::class,'edit']);
    Route::post('/prescription/update/{id}',[PrescriptionController::class,'update']);
    Route::get('/prescription/show/{id}',[PrescriptionController::class,'show']);
    Route::delete('/prescription/delete/{id}', [PrescriptionController::class,'destroy']);
    // prescribe medicine
    Route::get('/medicines/{drug_type_id}',[MedicineController::class,'show']);
    Route::get('/medicines/quantity_unit/{id}',[MedicineController::class,'quantityUnit']);
    Route::get('/medicines/edit/{id}',[MedicineController::class,'edit']);
    Route::get('/medicines',[MedicineController::class,'index']);
    Route::post('/medicines/update/{id}',[MedicineController::class,'update']);
    Route::post('/medicines/store',[MedicineController::class,'store']);
    Route::get('/medicines/delete/{id}', [MedicineController::class, 'destroy']);
    // invoice
    Route::get('download-invoice/', [BillController::class, 'downloadInvoice']);
    // patient appointment
    Route::get('patient/appointment/history/{id}',[PatientController::class, 'patientAppointmentHistory']);
    Route::get('patient/bill/history/{id}',[PatientController::class, 'patientBillHistory']);
    Route::get('patient/prescription/history/{id}',[PatientController::class, 'patientPrescriptionHistory']);
    Route::get('patient/document/history/{id}',[PatientController::class, 'patientDocumentHistory']);
    Route::get('city',[PatientController::class, 'city']);
    Route::post('update-profile/{id}', [AddDoctorController::class, 'profileUpdate']);
    //common api
    Route::get('all-service', [ServiceController::class, 'serviceList']);
    Route::get('all-medicine',[MedicineController::class,'medicineList']);
    Route::post('layouts/store', [AddDoctorController::class,'layoutStore']);
    // setting
    Route::resource('settings', SettingController::class)->only([
    'index', 'store'
    ]);
    // Dashboard
    Route::get('dashboard', [StaffController::class, 'dashboard']);
    //Leads
    Route::get('lead/patient/convert/{id}', [LeadCollectController::class, 'patientConvert'])->name('patientConvert');
    Route::post('leads', [LeadCollectController::class, 'index']);
    Route::get('lead/create', [LeadCollectController::class, 'create']);
    Route::get('lead/show/{id}', [LeadCollectController::class, 'show']);
    Route::get('lead/edit/{id}', [LeadCollectController::class, 'edit']);
    Route::post('lead/store', [LeadCollectController::class, 'storeLead']);
    Route::post('lead/update/{id}', [LeadCollectController::class, 'update']);
    Route::delete('lead/delete/{id}', [LeadCollectController::class, 'destroy']);
    
    Route::get('lead-update',[LeadCollectController::class, 'showUpdate'])->name('leadUpdate');
    Route::post('lead-notes',[LeadCollectController::class, 'leadNotes'])->name('leadNotes');
    Route::delete('lead-notes/delete/{id}',[LeadCollectController::class, 'notesDelete'])->name('notesDelete');
    Route::post('lead-attach',[LeadCollectController::class, 'leadAttach'])->name('leadAttach');
    Route::delete('lead-attachment/delete/{id}',[LeadCollectController::class, 'attachmentDelete'])->name('attachmentDelete');
    Route::post('lead-address',[LeadCollectController::class, 'leadAddress'])->name('leadAddress');
    Route::post('lead-follow-up',[LeadCollectController::class, 'followUp'])->name('followUp');
    Route::delete('lead-follow-up/delete/{id}',[LeadCollectController::class, 'followUpDelete'])->name('followUpDelete');
//    Route::get('convert-customer/{id}',[LeadCollectController::class, 'convertCustomer'])->name('convertCustomer');
    Route::post('lead-source',[LeadCollectController::class, 'leadSource'])->name('leadSource');
    Route::post('lead-service-type',[LeadCollectController::class, 'serviceType'])->name('serviceType');
    Route::get('autocomplete', [LeadCollectController::class,'autocomplete'])->name('autocomplete');
    Route::get('/getFollowUp',[LeadCollectController::class,'getFollowUp'])->name('getFollowUp');
    Route::post('folowUpSubmit',[LeadCollectController::class, 'folowUpSubmit'])->name('folowUpSubmit');
    Route::get('/destroyFollowUp',[LeadCollectController::class,'destroyFollowUp'])->name('destroyFollowUp');
    Route::get('/getSource',[LeadCollectController::class,'getSource'])->name('getSource');
    Route::get('/getFollow',[LeadCollectController::class,'getFollow'])->name('getFollow');
    
    Route::post('get-states', [LeadCollectController::class, 'getStates'])->name('getStates');
    Route::post('get-cities', [LeadCollectController::class, 'getCities'])->name('getCities');
    Route::get('lead/notes/list/{id}',[LeadCollectController::class,'notesList']);
    Route::get('lead/attachments/list/{id}',[LeadCollectController::class,'attachmentList']);
    Route::get('lead/assign/update/{id}',[LeadCollectController::class,'updateLeadAssign']);
    Route::get('lead/status/update/{id}',[LeadCollectController::class,'updateLeadStatus']);
    
    //doctor speciality service
    Route::get('specialityServices/{id}', [AddDoctorController::class, 'specialityServices']);
       // Doctor Educactions details
    Route::get('doctor/education/list/{id}',  [AddDoctorController::class, 'educationList']);
    Route::post('doctor/education/store/{id}',  [AddDoctorController::class, 'educationStore']);
    Route::get('doctor/education/edit/{id}',  [AddDoctorController::class, 'educationEdit']);
    Route::post('doctor/education/update/{id}',  [AddDoctorController::class, 'educationUpdate']);
    Route::get('doctor/education/delete/{id}',  [AddDoctorController::class, 'educationDelete']);
    // Experiences details
    Route::get('doctor/experiences/list/{id}',  [AddDoctorController::class, 'experienceList']);
    Route::post('doctor/experiences/store/{id}',  [AddDoctorController::class, 'experienceStore']);
    Route::get('doctor/experiences/edit/{id}',  [AddDoctorController::class, 'experienceEdit']);
    Route::post('doctor/experiences/update/{id}',  [AddDoctorController::class, 'experienceUpdate']);
    Route::get('doctor/experiences/delete/{id}',  [AddDoctorController::class, 'experienceDelete']);
    // doctor appointment in detail
    Route::get('doctor/appointment/list/{id}',  [AddDoctorController::class, 'doctorAppointment']);
    // Activity log
    Route::get('activity/log/list',  [ActivityLogController::class, 'index']); 
    // Doctor Clinic
    Route::get('doctor/clinic/list/{id}',  [AddDoctorController::class, 'clinicList']);
    Route::post('doctor/clinic/store/{id}',  [AddDoctorController::class, 'clinicStore']);
    Route::get('doctor/clinic/edit/{id}',  [AddDoctorController::class, 'clinicEdit']);
    Route::post('doctor/clinic/update/{id}', [AddDoctorController::class, 'clinicUpdate']);
    Route::get('doctor/clinic/delete/{id}',  [AddDoctorController::class, 'clinicDelete']);
    // Doctor hospital
    Route::get('doctor/hospital/list/{id}',  [AddDoctorController::class, 'hospitalList']);
    Route::post('doctor/hospital/store/{id}',  [AddDoctorController::class, 'hospitalStore']);
    Route::get('doctor/hospital/edit/{id}',  [AddDoctorController::class, 'hospitalEdit']);
    Route::post('doctor/hospital/update/{id}', [AddDoctorController::class, 'hospitalUpdate']);
    Route::get('doctor/hospital/delete/{id}',  [AddDoctorController::class, 'hospitalDelete']);
    // lead convert report
    Route::get('lead/convert/report',  [LeadCollectController::class, 'leadConvertReport']);
    // import patient with appointment
    Route::post('import-patients', [PatientController::class, 'import']);
});
Route::get('forget-password',[ForgotPasswordController::class,'showForgetPasswordForm']);
Route::post('forget-password',[ForgotPasswordController::class,'submitForgetPasswordForm']);
Route::get('reset-password/{token}',[ForgotPasswordController::class,'showResetPasswordForm']);
Route::post('reset-password/{token}',[ForgotPasswordController::class,'submitResetPasswordForm']);
Route::get('email/verify/{id}', [VerificationController::class,'verify'])->name('verification.verify'); // Make sure to keep this as your route name

Route::get('email/resend/{id}', [VerificationController::class,'resend'])->name('verification.resend');

// Appointment synchronize api's
Route::middleware('checkApiKey')->group(function () {
    Route::post('/leads/store', [LeadCollectController::class, 'store']);
    Route::get('/appointments', [AppointmentSynchronizeController::class, 'index']);
    Route::post('/appointments', [AppointmentSynchronizeController::class, 'store']);
    Route::get('/appointments/search/patient/{mobile}',[AppointmentSynchronizeController::class, 'patientSearch']);
    Route::post('/appointments/add/patient', [AppointmentSynchronizeController::class, 'addPatient']);
    Route::post('/appointments/get/slot', [AppointmentSynchronizeController::class, 'doctorSlot']);
    Route::get('/appointments/problems',[AppointmentSynchronizeController::class,'symptoms']);
    Route::get('/appointments/status/update/{id?}/{status?}', [AppointmentSynchronizeController::class, 'updateAppointmentSatus']);
    Route::get('/appointments/booked/slots', [AppointmentSynchronizeController::class, 'bookedSlots']);
    Route::post('/appointments/schedule/change', [AppointmentSynchronizeController::class, 'changeSchedule']);
    
    Route::get('/appointments/{id}', [AppointmentSynchronizeController::class, 'show']);
    Route::put('/appointments/{id}', [AppointmentSynchronizeController::class, 'update']);
    Route::delete('/appointments/{id}', [AppointmentSynchronizeController::class, 'destroy']);
});