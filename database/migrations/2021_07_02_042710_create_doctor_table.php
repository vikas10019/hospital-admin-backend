<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('doctor', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('title')->nullable();
            $table->string('designation')->nullable();
            $table->integer('specialization_id')->nullable();
            $table->longText('short_biography')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('degree')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->text('awards', 65535)->nullable();
            $table->float('rating')->nullable();
            $table->decimal('experience')->default(0);
            $table->boolean('availability')->default(1);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('doctor');
    }

}
