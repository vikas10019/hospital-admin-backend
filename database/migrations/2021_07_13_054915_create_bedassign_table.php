<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBedassignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bedassign', function (Blueprint $table) {
            $table->id();
            $table->string('serial');
            $table->bigInteger('patient_id')->unsigned();
            $table->string('mobile');
            $table->bigInteger('bed_id')->unsigned();
            $table->string('description')->nullable();
            $table->date('assign_date');
            $table->date('discharge_date');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('patient_id')->references('id')->on('patient')->onUpdate('cascade');
            $table->foreign('bed_id')->references('id')->on('bed')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bedassign');
    }
}
