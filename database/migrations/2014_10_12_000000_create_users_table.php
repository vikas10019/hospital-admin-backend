<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->string('dob')->nullable();
            $table->boolean('gender')->default(1);
            $table->string('picture')->nullable();
            $table->string('geolocation_address')->nullable();
            $table->unsignedInteger('city')->nullable();
            $table->unsignedInteger('state')->nullable();
            $table->unsignedInteger('country')->nullable();
            $table->string('address')->nullable();
            $table->integer('pincode')->nullable();
            $table->boolean('status')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('city')->references('id')->on('cities')->onDelete('set null');
            $table->foreign('state')->references('id')->on('states')->onDelete('set null');
            $table->foreign('country')->references('id')->on('countries')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
