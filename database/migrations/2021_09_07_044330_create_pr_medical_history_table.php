<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrMedicalHistoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pr_medical_history', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('patient_id')->unsigned();
            $table->foreign('patient_id')->references('id')->on('patient')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->integer('bp_high')->nullable();
            $table->integer('bp_low')->nullable();
            $table->json('genetic_history')->nullable();
            $table->integer('pulse_rate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('pr_medical_history');
    }

}
