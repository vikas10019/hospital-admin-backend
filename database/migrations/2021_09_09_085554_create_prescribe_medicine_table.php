<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescribeMedicineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescribe_medicine', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('prescription_id')->unsigned();
            $table->bigInteger('drug_type_id')->unsigned();
            $table->bigInteger('medicine_id')->unsigned();
            $table->text('note')->nullable();
            $table->string('days');
            $table->string('frequency_day')->nullable();
            $table->string('frequency_intake')->nullable();
            $table->string('frequency_time')->nullable();
            $table->string('frequency_type')->nullable();
            $table->timestamps();
            
            
            $table->foreign('prescription_id')->references('id')->on('prescription')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('drug_type_id')->references('id')->on('drug_type')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('medicine_id')->references('id')->on('medicine')
                    ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescribe_medicine');
    }
}
