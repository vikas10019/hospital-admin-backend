<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrExerciseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_exercise', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('prescription_id')->unsigned();
            $table->string('name')->nullable();
            $table->integer('turns')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            
            $table->foreign('prescription_id')->references('id')->on('prescription')
                    ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_exercise');
    }
}
