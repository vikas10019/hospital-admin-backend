<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('appointment', function (Blueprint $table) {
            $table->id();
            $table->string('appointment_id')->nullable();
            // $table->unsignedBigInteger('user_id');
            // $table->foreign('user_id')->references('id')->on('users');
            $table->string('patient_id')->nullable();
            $table->string('department_id')->nullable();
            $table->string('doctor_id')->nullable();
            $table->string('serial_no')->nullable();
            $table->dateTime('meeting_at')->nullable();
            $table->string('duration')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);
            $table->text('cancellation_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('appointment');
    }

}
