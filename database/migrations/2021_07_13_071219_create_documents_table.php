<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('patient_id')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->bigInteger('doctor_id')->unsigned();
            $table->string('description')->nullable();
            $table->string('patient_document')->nullable();
            //$table->foreign('patient_id')->references('id')->on('patient')->onUpdate('cascade');
            $table->foreign('doctor_id')->references('id')->on('doctor')->onUpdate('cascade');    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('documents');
    }

}
