<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path =  __DIR__ . '/sql/cities.sql';
        DB::unprepared(file_get_contents($path));
    }
}
