-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 08, 2020 at 11:57 AM
-- Server version: 5.7.28
-- PHP Version: 7.3.11-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `merihelp`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries_data`
--



--
-- Dumping data for table `countries_data`
--

INSERT INTO `countries_data` (`id`, `code`, `code_lang`, `code_lang_app`, `name`, `name_en`, `name_ms`, `url_flag`, `currency`, `code_phone`, `n_status`, `is_expand`) VALUES
(485, 'US', '', '', 'United States', 'United States', 'United States', '/united-states-of-america.jpg', 'USD', 1, 1, 0),
(486, 'CA', '', '', 'Canada', 'Canada', 'Canada', '/canada.png', '', 1, 0, 0),
(487, 'AF', '', '', 'Afghanistan', 'Afghanistan', 'Afghanistan', '/afghanistan.jpg', '', 93, 0, 0),
(488, 'AL', '', '', 'Albania', 'Albania', 'Albania', '/albania.png', '', 355, 0, 0),
(489, 'DZ', '', '', 'Algeria', 'Algeria', 'Algeria', '/algeria.png', '', 213, 0, 0),
(490, 'AS', '', '', 'American Samoa', 'American Samoa', 'American Samoa', '/american_samoa.png', '', 1684, 0, 0),
(491, 'AD', '', '', 'Andorra', 'Andorra', 'Andorra', '/andorra.png', '', 376, 0, 0),
(492, 'AO', '', '', 'Angola', 'Angola', 'Angola', '/angola.png', '', 244, 0, 0),
(493, 'AI', '', '', 'Anguilla', 'Anguilla', 'Anguilla', '/anguilla.png', '', 1264, 0, 0),
(494, 'AQ', '', '', 'Antarctica', 'Antarctica', 'Antarctica', '/antarctica.png', '', 672, 0, 0),
(495, 'AG', '', '', 'Antigua and/or Barbuda', 'Antigua and/or Barbuda', 'Antigua and/or Barbuda', '/antigua_and_barbuda.png', '', 1268, 0, 0),
(496, 'AR', '', '', 'Argentina', 'Argentina', 'Argentina', '/argentina.png', '', 54, 0, 0),
(497, 'AM', '', '', 'Armenia', 'Armenia', 'Armenia', '/armenia.png', '', 374, 0, 0),
(498, 'AW', '', '', 'Aruba', 'Aruba', 'Aruba', '/aruba.png', '', 297, 0, 0),
(499, 'AU', '', '', 'Australia', 'Australia', 'Australia', '/australia.png', '', 61, 0, 0),
(500, 'AT', '', '', 'Austria', 'Austria', 'Austria', '/austria.png', '', 43, 0, 0),
(501, 'AZ', '', '', 'Azerbaijan', 'Azerbaijan', 'Azerbaijan', '/azerbaijan.png', '', 994, 0, 0),
(502, 'BS', '', '', 'Bahamas', 'Bahamas', 'Bahamas', '/bahamas.png', '', 1242, 0, 0),
(503, 'BH', '', '', 'Bahrain', 'Bahrain', 'Bahrain', '/bahrain.png', '', 973, 0, 0),
(504, 'BD', '', '', 'Bangladesh', 'Bangladesh', 'Bangladesh', '/bangladesh.png', '', 880, 1, 0),
(505, 'BB', '', '', 'Barbados', 'Barbados', 'Barbados', '/barbados.png', '', 1246, 0, 0),
(506, 'BY', '', '', 'Belarus', 'Belarus', 'Belarus', '/belarus.png', '', 375, 0, 0),
(507, 'BE', '', '', 'Belgium', 'Belgium', 'Belgium', '/belgium.png', '', 32, 0, 0),
(508, 'BZ', '', '', 'Belize', 'Belize', 'Belize', '/belize.png', '', 501, 0, 0),
(509, 'BJ', '', '', 'Benin', 'Benin', 'Benin', '/benin.png', '', 229, 0, 0),
(510, 'BM', '', '', 'Bermuda', 'Bermuda', 'Bermuda', '/bermuda.png', '', 1441, 0, 0),
(511, 'BT', '', '', 'Bhutan', 'Bhutan', 'Bhutan', '/bhutan.png', '', 975, 0, 0),
(512, 'BO', '', '', 'Bolivia', 'Bolivia', 'Bolivia', '/bolivia.png', '', 591, 0, 0),
(513, 'BA', '', '', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', '/bosnia_and_herzegovina.png', '', 387, 0, 0),
(514, 'BW', '', '', 'Botswana', 'Botswana', 'Botswana', '/botswana.png', '', 267, 0, 0),
(515, 'BV', '', '', 'Bouvet Island', 'Bouvet Island', 'Bouvet Island', '/bouvet_island.png', '', 55, 0, 0),
(516, 'BR', '', '', 'Brazil', 'Brazil', 'Brazil', '/brazil.jpg', 'BRL', 55, 0, 0),
(517, 'IO', '', '', 'British lndian Ocean Territory', 'British lndian Ocean Territory', 'British lndian Ocean Territory', '/british_indian_ocean_territory.png', '', 246, 0, 0),
(518, 'BN', '', '', 'Brunei Darussalam', 'Brunei Darussalam', 'Brunei Darussalam', '/brunei.jpg', 'BND', 673, 0, 0),
(519, 'BG', '', '', 'Bulgaria', 'Bulgaria', 'Bulgaria', '/bulgaria.png', '', 359, 0, 0),
(520, 'BF', '', '', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', '/burkina_faso.png', '', 226, 0, 0),
(521, 'BI', '', '', 'Burundi', 'Burundi', 'Burundi', '/burundi.png', '', 257, 0, 0),
(522, 'KH', '', '', 'Cambodia', 'Cambodia', 'Cambodia', '/cambodia.png', '', 855, 0, 0),
(523, 'CM', '', '', 'Cameroon', 'Cameroon', 'Cameroon', '/cameroon.png', '', 237, 0, 0),
(524, 'CV', '', '', 'Cape Verde', 'Cape Verde', 'Cape Verde', '/cape_verde.png', '', 238, 0, 0),
(525, 'KY', '', '', 'Cayman Islands', 'Cayman Islands', 'Cayman Islands', '/cayman_islands.png', '', 1345, 0, 0),
(526, 'CF', '', '', 'Central African Republic', 'Central African Republic', 'Central African Republic', '/central_african_republic.png', '', 236, 0, 0),
(527, 'TD', '', '', 'Chad', 'Chad', 'Chad', '/chad.png', '', 235, 0, 0),
(528, 'CL', '', '', 'Chile', 'Chile', 'Chile', '/chile.png', '', 56, 0, 0),
(529, 'CN', '', '', 'China', 'China', 'China', '/china.png', '', 86, 0, 0),
(530, 'CX', '', '', 'Christmas Island', 'Christmas Island', 'Christmas Island', '/christmas_island.png', '', 61, 0, 0),
(531, 'CC', '', '', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', '/cocos_islands.png', '', 672, 0, 0),
(532, 'CO', '', '', 'Colombia', 'Colombia', 'Colombia', '/colombia.png', '', 57, 0, 0),
(533, 'KM', '', '', 'Comoros', 'Comoros', 'Comoros', '/comoros.png', '', 269, 0, 0),
(534, 'CG', '', '', 'Congo', 'Congo', 'Congo', '/congo.png', '', 242, 0, 0),
(535, 'CK', '', '', 'Cook Islands', 'Cook Islands', 'Cook Islands', '/cook_islands.png', '', 682, 0, 0),
(536, 'CR', '', '', 'Costa Rica', 'Costa Rica', 'Costa Rica', '/costa_rica.png', '', 506, 0, 0),
(537, 'HR', '', '', 'Croatia (Hrvatska)', 'Croatia (Hrvatska)', 'Croatia (Hrvatska)', '/croatia.png', '', 385, 0, 0),
(538, 'CU', '', '', 'Cuba', 'Cuba', 'Cuba', '/cuba.png', '', 53, 0, 0),
(539, 'CY', '', '', 'Cyprus', 'Cyprus', 'Cyprus', '/cyprus.png', '', 357, 0, 0),
(540, 'CZ', '', '', 'Czech Republic', 'Czech Republic', 'Czech Republic', '/czech_republic.png', '', 420, 0, 0),
(541, 'CD', '', '', 'Democratic Republic of Congo', 'Democratic Republic of Congo', 'Democratic Republic of Congo', '/democratic_republic_congo.png', '', 242, 0, 0),
(542, 'DK', '', '', 'Denmark', 'Denmark', 'Denmark', '/denmark.jpg', 'DKK', 45, 0, 0),
(543, 'DJ', '', '', 'Djibouti', 'Djibouti', 'Djibouti', '/djibouti.png', '', 253, 0, 0),
(544, 'DM', '', '', 'Dominica', 'Dominica', 'Dominica', '/dominica.png', '', 1767, 0, 0),
(545, 'DO', '', '', 'Dominican Republic', 'Dominican Republic', 'Dominican Republic', '/dominican_republic.png', '', 1809, 0, 0),
(546, 'TP', '', '', 'East Timor', 'East Timor', 'East Timor', '/east_timor.png', '', 670, 0, 0),
(547, 'EC', '', '', 'Ecudaor', 'Ecudaor', 'Ecudaor', '/ecuador.png', '', 593, 0, 0),
(548, 'EG', '', '', 'Egypt', 'Egypt', 'Egypt', '/egypt.png', '', 20, 0, 0),
(549, 'SV', '', '', 'El Salvador', 'El Salvador', 'El Salvador', '/el_salvador.png', '', 503, 0, 0),
(550, 'GQ', '', '', 'Equatorial Guinea', 'Equatorial Guinea', 'Equatorial Guinea', '/equatorial_guinea.png', '', 240, 0, 0),
(551, 'ER', '', '', 'Eritrea', 'Eritrea', 'Eritrea', '/eritrea.png', '', 291, 0, 0),
(552, 'EE', '', '', 'Estonia', 'Estonia', 'Estonia', '/estonia.png', '', 372, 0, 0),
(553, 'ET', '', '', 'Ethiopia', 'Ethiopia', 'Ethiopia', '/ethiopia.png', '', 251, 0, 0),
(554, 'FK', '', '', 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', '/falkland _islands.png', '', 500, 0, 0),
(555, 'FO', '', '', 'Faroe Islands', 'Faroe Islands', 'Faroe Islands', '/faroe_islands.png', '', 298, 0, 0),
(556, 'FJ', '', '', 'Fiji', 'Fiji', 'Fiji', '/fiji.png', '', 679, 0, 0),
(557, 'FI', '', '', 'Finland', 'Finland', 'Finland', '/finland.png', '', 358, 0, 0),
(558, 'FR', '', '', 'France', 'France', 'France', '/france.jpg', 'FRF', 33, 0, 0),
(559, 'FX', '', '', 'France, Metropolitan', 'France, Metropolitan', 'France, Metropolitan', '/france.png', '', 33, 0, 0),
(560, 'GF', '', '', 'French Guiana', 'French Guiana', 'French Guiana', '/france.png', '', 594, 0, 0),
(561, 'PF', '', '', 'French Polynesia', 'French Polynesia', 'French Polynesia', '/france.png', '', 689, 0, 0),
(562, 'TF', '', '', 'French Southern Territories', 'French Southern Territories', 'French Southern Territories', '/france.png', '', 262, 0, 0),
(563, 'GA', '', '', 'Gabon', 'Gabon', 'Gabon', '/gabon.jpg', 'CFA', 241, 0, 0),
(564, 'GM', '', '', 'Gambia', 'Gambia', 'Gambia', '/gambia.png', '', 220, 0, 0),
(565, 'GE', '', '', 'Georgia', 'Georgia', 'Georgia', '/georgia.png', '', 995, 0, 0),
(566, 'DE', '', '', 'Germany', 'Germany', 'Germany', '/germany.jpg', 'DEM', 49, 1, 0),
(567, 'GH', '', '', 'Ghana', 'Ghana', 'Ghana', '/ghana.png', '', 233, 0, 0),
(568, 'GI', '', '', 'Gibraltar', 'Gibraltar', 'Gibraltar', '/gibraltar.png', '', 350, 0, 0),
(569, 'GR', '', '', 'Greece', 'Greece', 'Greece', '/greece.png', '', 30, 0, 0),
(570, 'GL', '', '', 'Greenland', 'Greenland', 'Greenland', '/greenland.png', '', 299, 0, 0),
(571, 'GD', '', '', 'Grenada', 'Grenada', 'Grenada', '/grenada.png', '', 1473, 0, 0),
(572, 'GP', '', '', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe', '/guadeloupe.jpg', '', 590, 0, 0),
(573, 'GU', '', '', 'Guam', 'Guam', 'Guam', '/guam.png', '', 1671, 0, 0),
(574, 'GT', '', '', 'Guatemala', 'Guatemala', 'Guatemala', '/guatemala.png', '', 502, 0, 0),
(575, 'GN', '', '', 'Guinea', 'Guinea', 'Guinea', '/guinea.png', '', 224, 0, 0),
(576, 'GW', '', '', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinea-Bissau', '/guinea-bissau.png', '', 245, 0, 0),
(577, 'GY', '', '', 'Guyana', 'Guyana', 'Guyana', '/guyana.png', '', 592, 0, 0),
(578, 'HT', '', '', 'Haiti', 'Haiti', 'Haiti', '/haiti.png', '', 509, 0, 0),
(580, 'HN', '', '', 'Honduras', 'Honduras', 'Honduras', '/honduras.png', '', 504, 0, 0),
(581, 'HK', '', '', 'Hong Kong', 'Hong Kong', 'Hong Kong', '/hong-kong.jpg', 'HKD', 852, 0, 0),
(582, 'HU', '', '', 'Hungary', 'Hungary', 'Hungary', '/hungary.png', '', 36, 0, 0),
(583, 'IS', '', '', 'Iceland', 'Iceland', 'Iceland', '/iceland.png', '', 354, 0, 0),
(584, 'IN', '', '', 'India', 'India', 'India', '/india.png', '', 91, 1, 0),
(585, 'ID', 'id', 'in', 'Indonesia', 'Indonesia', 'Indonesia', '/indo.jpg', 'IDR', 62, 0, 1),
(586, 'IR', '', '', 'Iran (Islamic Republic of)', 'Iran (Islamic Republic of)', 'Iran (Islamic Republic of)', '/iran.png', '', 98, 0, 0),
(587, 'IQ', '', '', 'Iraq', 'Iraq', 'Iraq', '/iraq.png', '', 964, 0, 0),
(588, 'IE', '', '', 'Ireland', 'Ireland', 'Ireland', '/ireland.png', '', 353, 0, 0),
(589, 'IL', '', '', 'Israel', 'Israel', 'Israel', '/israel.png', '', 972, 0, 0),
(590, 'IT', '', '', 'Italy', 'Italy', 'Italy', '/italy.jpg', 'ITL', 39, 1, 0),
(591, 'CI', '', '', 'Ivory Coast', 'Ivory Coast', 'Ivory Coast', '/ivory_coast.png', '', 225, 0, 0),
(592, 'JM', '', '', 'Jamaica', 'Jamaica', 'Jamaica', '/jamaica.png', '', 1876, 0, 0),
(593, 'JP', '', '', 'Japan', 'Japan', 'Japan', '/japan.jpg', 'JPY', 81, 0, 0),
(594, 'JO', '', '', 'Jordan', 'Jordan', 'Jordan', '/jordan.png', '', 962, 0, 0),
(595, 'KZ', '', '', 'Kazakhstan', 'Kazakhstan', 'Kazakhstan', '/kazakhstan.png', '', 7, 0, 0),
(596, 'KE', '', '', 'Kenya', 'Kenya', 'Kenya', '/kenya.png', '', 254, 0, 0),
(597, 'KI', '', '', 'Kiribati', 'Kiribati', 'Kiribati', '/kiribati.png', '', 686, 0, 0),
(598, 'KR', '', '', 'South Korea', 'South Korea', 'South Korea', '/south-korea.jpg', 'KRW', 82, 0, 0),
(599, 'KR', '', '', 'Norht Korea', 'North Korea', 'North Korea', '/north_korea.png', '', 82, 0, 0),
(600, 'KW', '', '', 'Kuwait', 'Kuwait', 'Kuwait', '/kuwait.jpg', 'KWD', 965, 0, 0),
(601, 'KG', '', '', 'Kyrgyzstan', 'Kyrgyzstan', 'Kyrgyzstan', '/kyrgyzstan.png', '', 996, 0, 0),
(602, 'LA', '', '', 'Lao People\'s Democratic Republic', 'Lao People\'s Democratic Republic', 'Lao People\'s Democratic Republic', '/laos.png', '', 856, 0, 0),
(603, 'LV', '', '', 'Latvia', 'Latvia', 'Latvia', '/latvia.png', '', 371, 0, 0),
(604, 'LB', '', '', 'Lebanon', 'Lebanon', 'Lebanon', '/lebanon.png', '', 961, 0, 0),
(605, 'LS', '', '', 'Lesotho', 'Lesotho', 'Lesotho', '/lesotho.png', '', 266, 0, 0),
(606, 'LR', '', '', 'Liberia', 'Liberia', 'Liberia', '/liberia.png', '', 231, 0, 0),
(607, 'LY', '', '', 'Libyan Arab Jamahiriya', 'Libyan Arab Jamahiriya', 'Libyan Arab Jamahiriya', '/libya.png', '', 218, 0, 0),
(608, 'LI', '', '', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', '/liechtenstein.png', '', 423, 0, 0),
(609, 'LT', '', '', 'Lithuania', 'Lithuania', 'Lithuania', '/lithuania.png', '', 370, 0, 0),
(610, 'LU', '', '', 'Luxembourg', 'Luxembourg', 'Luxembourg', '/luxembourg.png', '', 352, 0, 0),
(611, 'MO', '', '', 'Macau', 'Macau', 'Macau', '/macau.png', '', 853, 0, 0),
(612, 'MK', '', '', 'Macedonia', 'Macedonia', 'Macedonia', '/macedonia.png', '', 389, 0, 0),
(613, 'MG', '', '', 'Madagascar', 'Madagascar', 'Madagascar', '/madagascar.png', '', 261, 0, 0),
(614, 'MW', '', '', 'Malawi', 'Malawi', 'Malawi', '/malawi.png', '', 265, 0, 0),
(615, 'MY', 'ms', 'ms', 'Malaysia', 'Malaysia', 'Malaysia', '/malaysia.jpg', 'MYR', 60, 1, 1),
(616, 'MV', '', '', 'Maldives', 'Maldives', 'Maldives', '/maldives.jpg', 'MVR', 960, 0, 0),
(617, 'ML', '', '', 'Mali', 'Mali', 'Mali', '/mali.png', '', 223, 0, 0),
(618, 'MT', '', '', 'Malta', 'Malta', 'Malta', '/malta.png', '', 356, 0, 0),
(619, 'MH', '', '', 'Marshall Islands', 'Marshall Islands', 'Marshall Islands', '/marshall_islands.png', '', 692, 0, 0),
(620, 'MQ', '', '', 'Martinique', 'Martinique', 'Martinique', '/martinique.png', '', 596, 0, 0),
(621, 'MR', '', '', 'Mauritania', 'Mauritania', 'Mauritania', '/mauritania.png', '', 222, 0, 0),
(622, 'MU', '', '', 'Mauritius', 'Mauritius', 'Mauritius', '/mauritius.png', '', 230, 0, 0),
(623, 'TY', '', '', 'Mayotte', 'Mayotte', 'Mayotte', '/mayotte.png', '', 262, 0, 0),
(624, 'MX', '', '', 'Mexico', 'Mexico', 'Mexico', '/mexico.png', '', 52, 0, 0),
(625, 'FM', '', '', 'Micronesia, Federated States of', 'Micronesia, Federated States of', 'Micronesia, Federated States of', '/micronesia.png', '', 691, 0, 0),
(626, 'MD', '', '', 'Moldova, Republic of', 'Moldova, Republic of', 'Moldova, Republic of', '/moldova.png', '', 373, 0, 0),
(627, 'MC', '', '', 'Monaco', 'Monaco', 'Monaco', '/monaco.png', '', 377, 0, 0),
(628, 'MN', '', '', 'Mongolia', 'Mongolia', 'Mongolia', '/mongolia.png', '', 976, 0, 0),
(629, 'MS', '', '', 'Montserrat', 'Montserrat', 'Montserrat', '/montserrat.png', '', 1664, 0, 0),
(630, 'MA', '', '', 'Morocco', 'Morocco', 'Morocco', '/morocco.png', '', 212, 0, 0),
(631, 'MZ', '', '', 'Mozambique', 'Mozambique', 'Mozambique', '/mozambique.png', '', 258, 0, 0),
(632, 'MM', '', '', 'Myanmar', 'Myanmar', 'Myanmar', '/myanmar.png', '', 95, 0, 0),
(633, 'NA', '', '', 'Namibia', 'Namibia', 'Namibia', '/namibia.png', '', 264, 0, 0),
(634, 'NR', '', '', 'Nauru', 'Nauru', 'Nauru', '/nauru.png', '', 674, 0, 0),
(635, 'NP', '', '', 'Nepal', 'Nepal', 'Nepal', '/nepal.png', '', 977, 1, 0),
(636, 'NL', '', '', 'Netherlands', 'Netherlands', 'Netherlands', '/netherlands.png', '', 31, 0, 0),
(637, 'AN', '', '', 'Netherlands Antilles', 'Netherlands Antilles', 'Netherlands Antilles', '/netherlands_antilles.png', '', 599, 0, 0),
(638, 'NC', '', '', 'New Caledonia', 'New Caledonia', 'New Caledonia', '/new_caledonia.png', '', 687, 0, 0),
(639, 'NZ', '', '', 'New Zealand', 'New Zealand', 'New Zealand', '/new-zealand.jpg', 'NZD', 64, 0, 0),
(640, 'NI', '', '', 'Nicaragua', 'Nicaragua', 'Nicaragua', '/nicaragua.png', '', 505, 0, 0),
(641, 'NE', '', '', 'Niger', 'Niger', 'Niger', '/niger.png', '', 227, 0, 0),
(642, 'NG', '', '', 'Nigeria', 'Nigeria', 'Nigeria', '/nigeria.png', '', 234, 0, 0),
(643, 'NU', '', '', 'Niue', 'Niue', 'Niue', '/niue.png', '', 683, 0, 0),
(644, 'NF', '', '', 'Norfork Island', 'Norfork Island', 'Norfork Island', '/norfork_island.png', '', 672, 0, 0),
(645, 'MP', '', '', 'Northern Mariana Islands', 'Northern Mariana Islands', 'Northern Mariana Islands', '/northern_mariana_islands.png', '', 1670, 0, 0),
(646, 'NO', '', '', 'Norway', 'Norway', 'Norway', '/norway.png', '', 47, 0, 0),
(647, 'OM', '', '', 'Oman', 'Oman', 'Oman', '/oman.jpg', 'OMR', 968, 0, 0),
(648, 'PK', '', '', 'Pakistan', 'Pakistan', 'Pakistan', '/pakistan.png', '', 92, 1, 0),
(649, 'PW', '', '', 'Palau', 'Palau', 'Palau', '/palau.png', '', 680, 0, 0),
(650, 'PA', '', '', 'Panama', 'Panama', 'Panama', '/panama.png', '', 507, 0, 0),
(651, 'PG', '', '', 'Papua New Guinea', 'Papua New Guinea', 'Papua New Guinea', '/papua-new-guinea.jpg', 'PGK', 675, 0, 0),
(652, 'PY', '', '', 'Paraguay', 'Paraguay', 'Paraguay', '/paraguay.png', '', 595, 0, 0),
(653, 'PE', '', '', 'Peru', 'Peru', 'Peru', '/peru.png', '', 51, 0, 0),
(654, 'PH', '', '', 'Philippines', 'Philippines', 'Philippines', '/philippines.png', '', 63, 0, 0),
(655, 'PN', '', '', 'Pitcairn', 'Pitcairn', 'Pitcairn', '/pitcairn.png', '', 64, 0, 0),
(656, 'PL', '', '', 'Poland', 'Poland', 'Poland', '/poland.png', '', 48, 0, 0),
(657, 'PT', '', '', 'Portugal', 'Portugal', 'Portugal', '/portugal.png', '', 351, 0, 0),
(658, 'PR', '', '', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico', '/puerto_rico.png', '', 1787, 0, 0),
(659, 'QA', '', '', 'Qatar', 'Qatar', 'Qatar', '/qatar.jpg', 'QAR', 974, 0, 0),
(660, 'SS', '', '', 'Republic of South Sudan', 'Republic of South Sudan', 'Republic of South Sudan', '/south_sudan.png', '', 211, 0, 0),
(661, 'RE', '', '', 'Reunion', 'Reunion', 'Reunion', '/reunion.png', '', 262, 0, 0),
(662, 'RO', '', '', 'Romania', 'Romania', 'Romania', '/romania.png', '', 40, 0, 0),
(663, 'RU', '', '', 'Russian Federation', 'Russian Federation', 'Russian Federation', '/russia.png', '', 70, 0, 0),
(664, 'RW', '', '', 'Rwanda', 'Rwanda', 'Rwanda', '/rwanda.png', '', 250, 0, 0),
(665, 'KN', '', '', 'Saint Kitts and Nevis', 'Saint Kitts and Nevis', 'Saint Kitts and Nevis', '/saint_kitts_and_nevis.png', '', 1869, 0, 0),
(666, 'LC', '', '', 'Saint Lucia', 'Saint Lucia', 'Saint Lucia', '/saint_lucia.png', '', 1758, 0, 0),
(667, 'VC', '', '', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', '/saint_vincent_and_the_grenadines.png', '', 1784, 0, 0),
(668, 'WS', '', '', 'Samoa', 'Samoa', 'Samoa', '/samoa.png', '', 684, 0, 0),
(669, 'SM', '', '', 'San Marino', 'San Marino', 'San Marino', '/san_marino.png', '', 378, 0, 0),
(670, 'ST', '', '', 'Sao Tome and Principe', 'Sao Tome and Principe', 'Sao Tome and Principe', '/sao_tome_and_principe.png', '', 239, 0, 0),
(671, 'SA', '', '', 'Saudi Arabia', 'Saudi Arabia', 'Saudi Arabia', '/saudi-arabia.jpg', 'SAR', 966, 0, 0),
(672, 'SN', '', '', 'Senegal', 'Senegal', 'Senegal', '/senegal.png', '', 221, 0, 0),
(673, 'RS', '', '', 'Serbia', 'Serbia', 'Serbia', '/serbia.png', '', 381, 0, 0),
(674, 'SC', '', '', 'Seychelles', 'Seychelles', 'Seychelles', '/seychelles.png', '', 248, 0, 0),
(675, 'SL', '', '', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', '/sierra_leone.png', '', 232, 0, 0),
(676, 'SG', '', '', 'Singapore', 'Singapore', 'Singapore', '/singapore.jpg', 'SGD', 65, 1, 0),
(677, 'SK', '', '', 'Slovakia', 'Slovakia', 'Slovakia', '/slovakia.png', '', 421, 0, 0),
(678, 'SI', '', '', 'Slovenia', 'Slovenia', 'Slovenia', '/slovenia.png', '', 386, 0, 0),
(679, 'SB', '', '', 'Solomon Islands', 'Solomon Islands', 'Solomon Islands', '/solomon-islands.jpg', 'SBD', 677, 0, 0),
(680, 'SO', '', '', 'Somalia', 'Somalia', 'Somalia', '/somalia.png', '', 252, 0, 0),
(681, 'ZA', '', '', 'South Africa', 'South Africa', 'South Africa', '/south_africa.png', '', 27, 0, 0),
(682, 'GS', '', '', 'South Georgia South Sandwich Islands', 'South Georgia South Sandwich Islands', 'South Georgia South Sandwich Islands', '/south_georgia_south_sandwich_slands.png', '', 500, 0, 0),
(683, 'ES', '', '', 'Spain', 'Spain', 'Spain', '/spain.png', '', 34, 0, 0),
(684, 'LK', '', '', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', '/sri_lanka.png', '', 94, 0, 0),
(685, 'SH', '', '', 'St. Helena', 'St. Helena', 'St. Helena', '/saint_helena.png', '', 290, 0, 0),
(686, 'PM', '', '', 'St. Pierre and Miquelon', 'St. Pierre and Miquelon', 'St. Pierre and Miquelon', '/st_pierre_miquelon.png', '', 508, 0, 0),
(687, 'SD', '', '', 'Sudan', 'Sudan', 'Sudan', '/sudan.png', '', 249, 0, 0),
(688, 'SR', '', '', 'Suriname', 'Suriname', 'Suriname', '/suriname.png', '', 597, 0, 0),
(689, 'SJ', '', '', 'Svalbarn and Jan Mayen Islands', 'Svalbarn and Jan Mayen Islands', 'Svalbarn and Jan Mayen Islands', '/svalbarn_and_jan_mayen_islands.png', '', 47, 0, 0),
(690, 'SZ', '', '', 'Swaziland', 'Swaziland', 'Swaziland', '/swaziland.png', '', 268, 0, 0),
(691, 'SE', '', '', 'Sweden', 'Sweden', 'Sweden', '/sweden.png', '', 46, 0, 0),
(692, 'CH', '', '', 'Switzerland', 'Switzerland', 'Switzerland', '/switzerland.png', '', 41, 0, 0),
(693, 'SY', '', '', 'Syrian Arab Republic', 'Syrian Arab Republic', 'Syrian Arab Republic', '/syria.png', '', 963, 0, 0),
(694, 'TW', '', '', 'Taiwan', 'Taiwan', 'Taiwan', '/taiwan.jpg', 'TWD', 886, 0, 0),
(695, 'TJ', '', '', 'Tajikistan', 'Tajikistan', 'Tajikistan', '/tajikistan.png', '', 992, 0, 0),
(696, 'TZ', '', '', 'Tanzania, United Republic of', 'Tanzania, United Republic of', 'Tanzania, United Republic of', '/tanzania.png', '', 255, 0, 0),
(697, 'TH', '', '', 'Thailand', 'Thailand', 'Thailand', '/thailand.png', '', 66, 1, 0),
(698, 'TG', '', '', 'Togo', 'Togo', 'Togo', '/togo.png', '', 228, 0, 0),
(699, 'TK', '', '', 'Tokelau', 'Tokelau', 'Tokelau', '/tokelau.png', '', 690, 0, 0),
(700, 'TO', '', '', 'Tonga', 'Tonga', 'Tonga', '/tonga.png', '', 676, 0, 0),
(701, 'TT', '', '', 'Trinidad and Tobago', 'Trinidad and Tobago', 'Trinidad and Tobago', '/trinidad_and_tobago.png', '', 1868, 0, 0),
(702, 'TN', '', '', 'Tunisia', 'Tunisia', 'Tunisia', '/tunisia.png', '', 216, 0, 0),
(703, 'TR', '', '', 'Turkey', 'Turkey', 'Turkey', '/turkey.jpg', 'TRY', 90, 0, 0),
(704, 'TM', '', '', 'Turkmenistan', 'Turkmenistan', 'Turkmenistan', '/turkmenistan.png', '', 7370, 0, 0),
(705, 'TC', '', '', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'Turks and Caicos Islands', '/turks_and_caicos_islands.png', '', 1649, 0, 0),
(706, 'TV', '', '', 'Tuvalu', 'Tuvalu', 'Tuvalu', '/tuvalu.png', '', 688, 0, 0),
(707, 'UG', '', '', 'Uganda', 'Uganda', 'Uganda', '/uganda.png', '', 256, 0, 0),
(708, 'UA', '', '', 'Ukraine', 'Ukraine', 'Ukraine', '/ukraine.png', '', 380, 0, 0),
(709, 'AE', '', '', 'United Arab Emirates', 'United Arab Emirates', 'United Arab Emirates', '/united-arab-emirates.jpg', 'AED', 971, 1, 0),
(710, 'GB', 'en', 'en', 'United Kingdom', 'United Kingdom', 'United Kingdom', '/united_kingdom.png', '', 44, 1, 0),
(711, 'UM', '', '', 'United States minor outlying islands', 'United States minor outlying islands', 'United States minor outlying islands', '/united-states-of-america.jpg', '', 1, 0, 0),
(712, 'UY', '', '', 'Uruguay', 'Uruguay', 'Uruguay', '/uruguay.png', '', 598, 0, 0),
(713, 'UZ', '', '', 'Uzbekistan', 'Uzbekistan', 'Uzbekistan', '/uzbekistan.png', '', 998, 0, 0),
(714, 'VU', '', '', 'Vanuatu', 'Vanuatu', 'Vanuatu', '/vanuatu.png', '', 678, 0, 0),
(715, 'VA', '', '', 'Vatican City State', 'Vatican City State', 'Vatican City State', '/vatican_city.png', '', 39, 0, 0),
(716, 'VE', '', '', 'Venezuela', 'Venezuela', 'Venezuela', '/venezuela.png', '', 58, 0, 0),
(717, 'VN', '', '', 'Vietnam', 'Vietnam', 'Vietnam', '/vietnam.png', '', 84, 0, 0),
(718, 'VG', '', '', 'Virgin Islands (British)', 'Virgin Islands (British)', 'Virgin Islands (British)', '/british_virgin_islands.png', '', 1284, 0, 0),
(719, 'VI', '', '', 'Virgin Islands (U.S.)', 'Virgin Islands (U.S.)', 'Virgin Islands (U.S.)', '/united_states_virgin_islands.png', '', 1340, 0, 0),
(720, 'WF', '', '', 'Wallis and Futuna Islands', 'Wallis and Futuna Islands', 'Wallis and Futuna Islands', '/walis_futana.png', '', 681, 0, 0),
(721, 'EH', '', '', 'Western Sahara', 'Western Sahara', 'Western Sahara', '/western_sahara.png', '', 212, 0, 0),
(722, 'YE', '', '', 'Yemen', 'Yemen', 'Yemen', '/yemen.png', '', 967, 0, 0),
(723, 'YU', '', '', 'Yugoslavia', 'Yugoslavia', 'Yugoslavia', '/yugoslavia.png', '', 38, 0, 0),
(724, 'ZR', '', '', 'Zaire', 'Zaire', 'Zaire', '/zaire.png', '', 243, 0, 0),
(725, 'ZM', '', '', 'Zambia', 'Zambia', 'Zambia', '/zambia.jpg', 'ZMK', 260, 0, 0),
(726, 'ZW', '', '', 'Zimbabwe', 'Zimbabwe', 'Zimbabwe', '/zimbabwe.png', '', 263, 0, 0),
(727, 'CH', '', '', 'China', 'China', 'China', '/china.jpg', 'CNY', 41, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries_data`
--
ALTER TABLE `countries_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries_data`
--
ALTER TABLE `countries_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=728;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
