<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicineSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // check if table medicine is empty
        if (DB::table('medicine')->count() == 0) {
            DB::table('medicine')->insert(array(
                0 =>
                array(
                    'name' => 'Azithromycin',
                    'drug_type_id' => 1,
                    'quantity' => 500,
                    'unit' => 'MG',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                1 =>
                array(
                    'name' => 'Paracetamol',
                    'drug_type_id' => 1,
                    'quantity' => 500,
                    'unit' => 'MG',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                2 =>
                array(
                    'name' => 'Aspirin',
                    'drug_type_id' => 1,
                    'quantity' => 75,
                    'unit' => 'MG',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                3 =>
                array(
                    'name' => 'Becosules',
                    'drug_type_id' => 3,
                    'quantity' => NULL,
                    'unit' => 'MG',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                4 =>
                array(
                    'name' => 'Benadryl',
                    'drug_type_id' => 6,
                    'quantity' => 150,
                    'unit' => 'ML',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                5 =>
                array(
                    'name' => 'Betnovate-C',
                    'drug_type_id' => 7,
                    'quantity' => 30,
                    'unit' => 'GM',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
               6 =>
                array(
                    'name' => 'Duonase Nasal Spray',
                    'drug_type_id' => 6,
                    'quantity' => 9.8,
                    'unit' => 'ML',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                7 =>
                array(
                    'name' => 'Betadine 10%',
                    'drug_type_id' => 4,
                    'quantity' => 20,
                    'unit' => 'GM',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                8 =>
                array(
                    'name' => 'Neosporin',
                    'drug_type_id' => 8,
                    'quantity' => 10,
                    'unit' => 'GM',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                )
            ));
        }
    }

}
