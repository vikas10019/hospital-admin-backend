<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        $permissions = [
            'patient_list',
            'patient_create',
            'patient_edit',
            'patient_delete',
            'doctor_list',
            'doctor_create',
            'doctor_edit',
            'doctor_delete',
            'document_list',
            'document_create',
            'document_edit',
            'document_delete',
            'appointment_list',
            'appointment_create',
            'appointment_edit',
            'appointment_delete',
            'staff_list',
            'staff_create',
            'staff_edit',
            'staff_delete',
            'schedule_list',
            'schedule_create',
            'scheule_edit',
            'schedule_delete',
            'speciality_list',
            'speciality_create',
            'speciality_edit',
            'speciality_delete',
            'role_list',
            'role_create',
            'role_edit',
            'role_delete',
            'permission_assign',
            'calendar_list',
            
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }

}
