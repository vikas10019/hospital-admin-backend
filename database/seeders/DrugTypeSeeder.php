<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DrugTypeSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // check if table drug type is empty
        if (DB::table('drug_type')->count() == 0) {
            $drug = [['name' => 'Tablet', 'created_at' => NULL, 'updated_at' => NULL],
                ['name' => 'Drops', 'created_at' => NULL, 'updated_at' => NULL],
                ['name' => 'Capsules', 'created_at' => NULL, 'updated_at' => NULL],
                ['name' => 'Oitments', 'created_at' => NULL, 'updated_at' => NULL],
                ['name' => 'Spray', 'created_at' => NULL, 'updated_at' => NULL],
                ['name' => 'Syrup', 'created_at' => NULL, 'updated_at' => NULL],
                ['name' => 'Tube', 'created_at' => NULL, 'updated_at' => NULL,],
                ['name' => 'Powder', 'created_at' => NULL, 'updated_at' => NULL,]];

            DB::table('drug_type')->insert($drug);
        }
    }

}
