<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

      $role = Role::create(['name' => 'Admin']);
      $permissions = Permission::all();
      $role->givePermissionTo(Permission::all());
      //$user->assignRole([$role->id]);

      // or may be done by chaining
      $role = Role::create(['name' => 'Doctor']);
      $permissions = Permission::all();
        $role->givePermissionTo(Permission::all());

      $role = Role::create(['name' => 'Receptionist']);
      $permissions = Permission::all();
      $role->givePermissionTo(Permission::all());

      $role = Role::create(['name' => 'Patient']);
      $permissions = Permission::all();
      $role->givePermissionTo(Permission::all());
      $role = Role::create(['name' => 'Staff']);
      $permissions = Permission::all();
      $role->givePermissionTo(Permission::all());

    }
}
