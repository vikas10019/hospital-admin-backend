<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path =  __DIR__ . '/sql/states.sql';
        DB::unprepared(file_get_contents($path));
    }
}
