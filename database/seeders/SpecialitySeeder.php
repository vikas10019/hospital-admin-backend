<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SpecialitySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $specialities = [
            ['name' => 'Anaesthesia', 'status' => 1],
            ['name' => 'Bariatric Surgery', 'status' => 1],
            ['name' => 'Centre For Spinal Diseases', 'status' => 1],
            ['name' => 'Maxillofacial Surgery', 'status' => 1],
            ['name' => 'Neurology', 'status' => 1],
            ['name' => 'Psychiatry', 'status' => 1],
            ['name' => 'Plastic Surgery', 'status' => 1],
            ['name' => 'Rheumatology', 'status' => 1],
            ['name' => 'Urology', 'status' => 1],
            ['name' => 'Surgical Gastroenterology', 'status' => 1],
        ];
        DB::table('specialities')->insert($specialities);
    }

}
