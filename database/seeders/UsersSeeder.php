<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin_role = Role::where('name', 'Admin')->first();
        $permissions = Permission::pluck('id','id')->all();
        /*
        * Add Super Admin User
        *
        */
        if (\App\Models\User::where('email', '=', 'admin@admin.com')->first() === null) {

            $user = \App\Models\User::create([
                'id' => 1,
                'name' => 'Super Admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('password'),
            ]);

        }else{
            $user = \App\Models\User::where('email', '=', 'admin@admin.com')->first();
        }

        // Attach admin role
        $permissions = Permission::pluck('id','id')->all();
        $superadmin_role->syncPermissions($permissions);
        $user->assignRole([$superadmin_role->id]);
       
    }
}
